package fr.eseo.gpi.beanartist.main;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;


public class Main {

	//Tests Manuels
	public static void main(String[] args) {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		fen.setVisible(true);
		
		fen.getPanneauDessin().setOutilCourant(new OutilSelection(fen.getPanneauDessin()));
	}
}

package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilCercleTest {

	//TESTS MANUELS
	public static void main(String[] args) {
		OutilCercleTest test = new OutilCercleTest();
		
		test.testOutilCercle();
	}
	
	public void testOutilCercle() {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		new OutilCercle(fen.getPanneauDessin());
		
		fen.afficher();
	}

}

package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.vue.formes.VueCarre;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilCarreTest {

	//TESTS MANUELS
	public static void main(String[] args) {
		OutilCarreTest test = new OutilCarreTest();
		
		test.testOutilCarre();
	}
	
	public void testOutilCarre() {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		new OutilCarre(fen.getPanneauDessin());
		
		fen.afficher();
		fen.getPanneauDessin().ajouterVueForme(new VueCarre(new Carre(50,50,40)));
		fen.getPanneauDessin().ajouterVueForme(new VueCarre(new Carre(50,50,50)));
		fen.getPanneauDessin().ajouterVueForme(new VueCarre(new Carre(10,10,40)));
		fen.getPanneauDessin().ajouterVueForme(new VueCarre(new Carre(10,50,50)));
	}

}

package fr.eseo.gpi.beanartist.controleur.outils;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Texte;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilTexteTest {

	private PanneauDessin pd;
	public OutilTexteTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testRemplir() {
		pd.getVueFormes().clear();
		new OutilTexte(pd);
		TestUtil.startTest("Ajouter le texte 'Bonjour le monde'");

		assertEquals(pd.getVueFormes().size(), 1);
		assertEquals(((Texte)pd.getVueFormes().get(0).getForme()).getTexte(), "Bonjour le monde");
	}
	
}
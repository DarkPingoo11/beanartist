package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilRectangleTest {

	//TESTS MANUELS
	public static void main(String[] args) {
		OutilRectangleTest test = new OutilRectangleTest();
		
		test.testOutilRectangle();
	}
	
	public void testOutilRectangle() {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		new OutilRectangle(fen.getPanneauDessin());
		
		fen.afficher();
	}

}

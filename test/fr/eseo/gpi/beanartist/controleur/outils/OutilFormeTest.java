package fr.eseo.gpi.beanartist.controleur.outils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilFormeTest {

	private PanneauDessin pd;
	public OutilFormeTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}
	
	@Test
	public void testCreation() {
		pd.getVueFormes().clear();
		new OutilRectangle(pd);
		TestUtil.startTest("Créez une figure en effectuant un glisser-déposer");

		assertEquals(pd.getVueFormes().size(), 1);
		assertEquals(pd.getVueTemporaire(), null);
		assertTrue(UIUtil.askWarningDialog("Question", "Avez vous vu la forme se dessiner au fur et à mesure de la création ?"));
	}
	
	@Test
	public void testCreationAnnulation() {
		pd.getVueFormes().clear();
		new OutilRectangle(pd);
		TestUtil.startTest("Créez une figure en effectuant un glisser-déposer, mais <br> ne relachez pas, et appuyez sur ECHAP");

		assertEquals(pd.getVueFormes().size(), 0);
		assertEquals(pd.getVueTemporaire(), null);
		assertTrue(UIUtil.askWarningDialog("Question", "Avez vous vu la forme se dessiner au fur et à mesure de la création ?"));
	}
	
}

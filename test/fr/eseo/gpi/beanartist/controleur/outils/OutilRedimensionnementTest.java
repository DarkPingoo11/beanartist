package fr.eseo.gpi.beanartist.controleur.outils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilRedimensionnementTest {

	private PanneauDessin pd;
	public OutilRedimensionnementTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testDeplacer() {
		pd.getVueFormes().clear();
		Rectangle r = new Rectangle();
		pd.ajouterVueForme(new VueRectangle(r));
		new OutilRedimension(pd).selectionnerForme(r);
		TestUtil.startTest("Deplacez la figure");

		assertEquals(pd.getVueFormes().size(), 1);
		if(r.getPosition().getX() == 0 && r.getPosition().getY() == 0) {
			fail("Les position doivent changer");
		}
		
		assertEquals((int)r.getLargeur(), (int)Forme.LARGEUR_PAR_DEFAUT);
		assertEquals((int)r.getHauteur(), (int)Forme.HAUTEUR_PAR_DEFAUT);
	}
	
	@Test
	public void testRedimensionnerLargeur() {
		pd.getVueFormes().clear();
		Rectangle r = new Rectangle();
		pd.ajouterVueForme(new VueRectangle(r));
		new OutilRedimension(pd).selectionnerForme(r);
		TestUtil.startTest("Changer la largeur la figure");

		assertEquals(pd.getVueFormes().size(), 1);
		if(r.getLargeur() == 100) {
			fail("Les dimensions doivent changer");
		}
		
		assertEquals((int)r.getHauteur(), 150);
	}
	
	@Test
	public void testRedimensionnerHauteur() {
		pd.getVueFormes().clear();
		Rectangle r = new Rectangle();
		pd.ajouterVueForme(new VueRectangle(r));
		new OutilRedimension(pd).selectionnerForme(r);
		TestUtil.startTest("Changer la hauteur la figure");

		assertEquals(pd.getVueFormes().size(), 1);
		if(r.getHauteur() == 150) {
			fail("Les dimensions doivent changer");
		}
		

		assertEquals((int)r.getLargeur(), 100);
	}
	
	@Test
	public void testRedimensionner() {
		pd.getVueFormes().clear();
		Rectangle r = new Rectangle();
		pd.ajouterVueForme(new VueRectangle(r));
		new OutilRedimension(pd).selectionnerForme(r);
		TestUtil.startTest("Changer la hauteur et largeur la figure");

		assertEquals(pd.getVueFormes().size(), 1);
		if(r.getHauteur() == 150 || r.getLargeur() == 100) {
			fail("Les dimensions doivent changer");
		}
	}
	
	
	
}
package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilLigneTest {

	//TESTS MANUELS
	public static void main(String[] args) {
		OutilLigneTest test = new OutilLigneTest();
		
		test.testOutilLigne();
	}
	
	public void testOutilLigne() {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		new OutilLigne(fen.getPanneauDessin());
		
		fen.afficher();
	}

//		public void testMe(){
//		PanneauDessin pd = new PanneauDessin(200, 200, Color.WHITE);
//		OutilLigne l = new OutilLigne(pd);
//		System.out.println(pd.getVueFormes().size());
//		pd.dispatchEvent(new MouseEvent(pd, MouseEvent.MOUSE_PRESSED, 100, MouseEvent.BUTTON1, 10, 10, 1, false));
//		pd.dispatchEvent(new MouseEvent(pd, MouseEvent.MOUSE_RELEASED, 110, MouseEvent.BUTTON1, 40, 40, 1, false));
//		System.out.println(pd.getVueFormes().size());
//		
//	}
}

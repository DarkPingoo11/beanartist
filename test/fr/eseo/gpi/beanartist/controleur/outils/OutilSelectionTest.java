package fr.eseo.gpi.beanartist.controleur.outils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilSelectionTest {

	private PanneauDessin pd;
	public OutilSelectionTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testCollageVisuel() {
		Rectangle r = new Rectangle();
		pd.getVueFormes().clear();
		new OutilSelection(pd).setCopy(r);
		
		TestUtil.startTest("Ouvrez le menu clic droit (clic-droit)<br />"
				+ "sélectionnez Coller<br />"
				+ "bouger la forme qui apparait jusqu'à une nouvelle position<br />"
				+ "cliquez pour coller");
		
		assertEquals(pd.getVueFormes().size(), 1);
		assertTrue(UIUtil.askWarningDialog("Question", "La forme est-elle apparue, et bougeait-elle suivant la souris  ?"));
	}
	
}
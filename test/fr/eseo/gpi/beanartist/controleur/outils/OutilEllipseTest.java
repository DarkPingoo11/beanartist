package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class OutilEllipseTest {

	//TESTS MANUELS
	public static void main(String[] args) {
		OutilEllipseTest test = new OutilEllipseTest();
		
		test.testOutilEllipse();
	}
	
	public void testOutilEllipse() {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		new OutilEllipse(fen.getPanneauDessin());
		
		fen.afficher();
	}

}

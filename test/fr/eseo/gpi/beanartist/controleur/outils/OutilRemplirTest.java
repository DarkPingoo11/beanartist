package fr.eseo.gpi.beanartist.controleur.outils;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilRemplirTest {

	private PanneauDessin pd;
	public OutilRemplirTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testRemplir() {
		pd.getVueFormes().clear();
		Rectangle r = new Rectangle();
		pd.ajouterVueForme(new VueRectangle(r));
		pd.setCouleurRemplir(Color.BLUE);
		new OutilRemplir(pd);
		TestUtil.startTest("Remplir la figure");

		assertEquals(pd.getVueFormes().size(), 1);
		assertEquals(r.getCouleurFond(), Color.BLUE);
	}
	
}
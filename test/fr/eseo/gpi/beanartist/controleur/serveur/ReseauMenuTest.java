package fr.eseo.gpi.beanartist.controleur.serveur;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ReseauMenuTest {

	@Test
	public void testMenuFull() {
		FenetreBeAnArtist.getInstance();

		TestUtil.startTest("Cliquez sur le menu Serveur<br>"
				+ "Puis sur <b>Démarrer un serveur</b><br />"
				+ "Puis sur <b>Valider</b><br>"
				+ "Puis revenez sur le menu Serveur");
		
		assertTrue(UIUtil.askWarningDialog("Question", "<html>Le menu initiale proposait-il uniquement les options<br>"
				+ " Démarrer un serveur<br>"
				+ " Se Connecter<br >"
				+ " Afficher Console Client"));
		
		assertTrue(UIUtil.askWarningDialog("Question", "<html>Le nouveau menu propose-t-il uniquement les options<br>"
				+ " Stopper le serveur<br>"
				+ " Afficher console serveur<br>"
				+ " Se Connecter<br >"
				+ " Afficher Console Client"));
		
		TestUtil.startTest("Cliquez sur le menu Serveur<br>"
				+ "Puis sur <b>Se connecter</b><br />"
				+ "Puis sur <b>Valider</b><br>"
				+ "Puis revenez sur le menu Serveur");
		
		assertTrue(UIUtil.askWarningDialog("Question", "<html>Le menu propose-t-il uniquement les options<br>"
				+ " Stopper le serveur<br>"
				+ " Afficher console serveur<br>"
				+ " Se déconnecter<br >"
				+ " Afficher Console Client"));
		
		assertTrue(UIUtil.askWarningDialog("Question", "Le menu s'est-il actualisé a chaque étape ?"));
	}
}
package fr.eseo.gpi.beanartist.controleur.serveur;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.ClientBeAnArtist;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ReseauComTest {

	ServeurBeAnArtist serveur;
	ClientBeAnArtist client1;

	public ReseauComTest() {
		try {
			FenetreBeAnArtist.getInstance();
			
			this.serveur = new ServeurBeAnArtist(2345);
			this.serveur.startServeur();
			this.serveur.getConsole().setVisible(true);
			
			Thread.sleep(400);
			
			NetworkManager nm = NetworkManager.getInstance();
			this.client1 = nm.getClient();
			this.client1.setName("Client 1");
			this.client1.getConsole().setVisible(true);
			this.client1.connectToServeur(InetAddress.getByName("127.0.0.1"), 2345);
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@Test
	public void testCommunications() {
		this.client1.getSocketClient().sendPacket(PacketType.MESSAGE, "Bonjour a toi serveur");
		this.serveur.getSocket().getCommunicationService().broadcastMessage("Bonjour clients");
		try {
			Thread.sleep(3000);
		}catch(Exception ex) {
			fail();
		}
		
		assertTrue(UIUtil.askWarningDialog("Question", "Le message est-il reçu par le client ?"));
		assertTrue(UIUtil.askWarningDialog("Question", "Le message est-il reçu par le serveur"));
	}

}

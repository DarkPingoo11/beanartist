package fr.eseo.gpi.beanartist.controleur.serveur;

import static org.junit.Assert.assertTrue;

import java.net.InetAddress;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.ClientBeAnArtist;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ReseauUiTest {

	ServeurBeAnArtist serveur;
	ClientBeAnArtist client1;

	private static boolean first = true;

	public ReseauUiTest() {
		try {
			if(first) {
				first = false;

				FenetreBeAnArtist.getInstance().setVisible(true);

				NetworkManager.getInstance().createServeur(2345);;
				this.serveur = NetworkManager.getInstance().getHostedServeur();
				this.serveur.startServeur();
				this.serveur.getConsole().setVisible(true);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@Test
	public void testConnection() {
		ClientBeAnArtist c = NetworkManager.getInstance().getClient();
		if(c.isConnected()) {
			c.resetConnect();
		}
		TestUtil.startTest("Cliquez sur le menu Serveur<br>"
				+ "Puis sur <b>Se connecter</b><br />"
				+ "Puis sur <b>Connecter</b>");
		assertTrue(UIUtil.askWarningDialog("Question", "L'interface graphique s'est-il affiché ?"));
		assertTrue(UIUtil.askWarningDialog("Question", "La fenêtre Connexion en cours s'est elle affichée"));
		assertTrue("Client connecté", NetworkManager.getInstance().getClient().isConnected());
	}

	@Test
	public void testConsole() {
		NetworkManager.getInstance().getClient().getConsole().setVisible(true);
		NetworkManager.getInstance().getClient().getConsole().log(UIUtil.showSimpleInputDialog("Un message", "Entrez un message"));

		TestUtil.startTest("Vérifiez que le message apparait sur la console client<br />");

		assertTrue(UIUtil.askWarningDialog("Question", "La console s'est-elle affichée ?"));
		assertTrue(UIUtil.askWarningDialog("Question", "Le message s'est-il affiché"));
	}

	@Test
	public void testNotification() {
		NetworkManager nm = NetworkManager.getInstance();
		nm.getClient().getConsole().setVisible(false);
		nm.getHostedServeur().getConsole().setVisible(false);
		if(!nm.getClient().isConnected()) {
			try {
				nm.getClient().connectToServeur(InetAddress.getByName("127.0.0.1"), 2345);
			} catch(Exception ex){}
		}

		nm.getHostedServeur().getSocket().getCommunicationService().broadcastMessage("Un message");

		TestUtil.startTest("Vérifiez que la notification jaune apparait sur la barre Menu<br>"
				+ "Puis cliquez dessus<br />"
				+ "Puis revenez sur la fenêtre");

		assertTrue(UIUtil.askWarningDialog("Question", "La notification est-elle apparue ?"));
		assertTrue(UIUtil.askWarningDialog("Question", "La console s'est-elle affichée lors de l'appuie sur la notification?"));
		assertTrue(UIUtil.askWarningDialog("Question", "La notification a-t-elle disparue ensuite ?"));
	}

	@Test
	public void testConnectionMenu() {
		ClientBeAnArtist c = NetworkManager.getInstance().getClient();
		if(c.isConnected()) {
			c.resetConnect();
		}

		TestUtil.startTest("Cliquez sur le menu Serveur<br>"
				+ "Puis sur <b>Se connecter</b><br />"
				+ "Puis sur <b>Connecter</b>"
				+ "Puis vérifiez si le menu change de couleur");
		assertTrue(UIUtil.askWarningDialog("Question", "le menu est-il passé du gris au vert ?"));
		assertTrue(UIUtil.askWarningDialog("Question", "La fenêtre Connexion en cours s'est elle affichée"));
		assertTrue("Client connecté", NetworkManager.getInstance().getClient().isConnected());
	}



}

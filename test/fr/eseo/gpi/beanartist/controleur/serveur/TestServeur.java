package fr.eseo.gpi.beanartist.controleur.serveur;

import java.net.SocketException;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;

public class TestServeur{


	public static void main(String[] args) {
		try {
			ServeurBeAnArtist serveur = new ServeurBeAnArtist(2345);
			serveur.startServeur();
			serveur.getConsole().setVisible(true);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

}

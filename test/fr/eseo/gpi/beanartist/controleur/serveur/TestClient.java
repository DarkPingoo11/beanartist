package fr.eseo.gpi.beanartist.controleur.serveur;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Test;

import fr.eseo.gpi.beanartist.controleur.client.ClientBeAnArtist;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class TestClient{


	@Test
	public void testClient(String[] args) {
		try {
			FenetreBeAnArtist.getInstance();
			NetworkManager nm = NetworkManager.getInstance();
			ClientBeAnArtist client = nm.getClient();
			client.getConsole().setVisible(true);
			client.connectToServeur(InetAddress.getByName("127.0.0.1"), 2345);
			
			assertTrue("Le cllient est connecté au serveur", client.isConnected());
		} catch (UnknownHostException e) {
			e.printStackTrace();
			fail();
		}
		
	}

}

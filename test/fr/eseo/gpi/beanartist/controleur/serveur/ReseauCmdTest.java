package fr.eseo.gpi.beanartist.controleur.serveur;

import static org.junit.Assert.assertTrue;

import java.net.InetAddress;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.ClientBeAnArtist;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ReseauCmdTest {

	ServeurBeAnArtist serveur;
	ClientBeAnArtist client1;

	private static boolean first = true;

	public ReseauCmdTest() {
		try {
			if(first) {
				first = false;
				FenetreBeAnArtist.getInstance();
				NetworkManager nm = NetworkManager.getInstance();
				nm.createServeur(2345);

				this.serveur = nm.getHostedServeur();
				this.serveur.startServeur();
				this.serveur.getConsole().setVisible(true);

				this.serveur.addForme(new Rectangle());
				this.serveur.addForme(new Cercle(new Point(200,200)));
				this.serveur.addForme(new Ellipse(new Point(50, 250)));

				Thread.sleep(400);


				this.client1 = nm.getClient();
				this.client1.setName("Client 1");
				this.client1.connectToServeur(InetAddress.getByName("127.0.0.1"), 2345);
			}
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@Test
	public void testList() {
		TestUtil.startTest("Entrez la commande /list");
		assertTrue(UIUtil.askWarningDialog("Question", "La liste des clients (-> User-XXX) s'est elle affichée ?"));
	}

	@Test
	public void testTime() {
		TestUtil.startTest("Entrez la commande /time");
		assertTrue(UIUtil.askWarningDialog("Question", "La date actuelle s'est-elle affichée ?"));
	}

	@Test
	public void testClearDessin() {

		TestUtil.startTest("Entrez la commande /clearDessin");
		assertTrue("Plus aucune forme", NetworkManager.getInstance().getHostedServeur().getFormes().isEmpty());
		assertTrue(UIUtil.askWarningDialog("Question", "Les formes ont-elle été effacées ?"));
	}

}

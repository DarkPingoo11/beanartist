package fr.eseo.gpi.beanartist.controleur.actions;

import fr.eseo.gpi.beanartist.controleur.outils.OutilLigne;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionEffacerTest {

	//TESTS MANUELS
	public static void main(String[] args) {
		ActionEffacerTest test = new ActionEffacerTest();
		
		test.testBarreOutilEffacer();
	}
	
	public void testBarreOutilEffacer() {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		new OutilLigne(fen.getPanneauDessin());
		fen.afficher();
	}
	
}

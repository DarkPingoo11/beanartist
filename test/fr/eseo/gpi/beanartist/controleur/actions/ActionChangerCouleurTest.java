package fr.eseo.gpi.beanartist.controleur.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JButton;

import org.junit.Test;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionChangerCouleurTest {

	private PanneauDessin pd;
	public ActionChangerCouleurTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testCouleur() {
		pd.getVueFormes().clear();
		Rectangle r = new Rectangle();
		pd.ajouterVueForme(new VueRectangle(r));
		new OutilSelection(pd).selectionnerForme(r);
		TestUtil.startTest("Choisir la couleur Bleu (0, 0, 255)");

		assertEquals(r.getCouleurLigne(), Color.BLUE);
	}
	
	@Test
	public void testCouleurBouton() {
		pd.getVueFormes().clear();
		TestUtil.startTest("Choisir la couleur Bleu (0, 0, 255)");

		JButton bouton = null;
		for(Component c : FenetreBeAnArtist.getInstance().getPanneauBarreOutils().getComponents()) {
			if(c instanceof JButton) {
				JButton b = (JButton) c;
				if(b.getActionCommand().equalsIgnoreCase("Choisir la couleur")) {
					bouton = b;
				}
			}
		}
		
		assertTrue(bouton != null);
		assertEquals(bouton.getBackground(), Color.BLUE);
	}
	
}
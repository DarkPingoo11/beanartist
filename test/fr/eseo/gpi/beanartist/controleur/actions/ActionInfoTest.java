package fr.eseo.gpi.beanartist.controleur.actions;

import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionInfo;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionInfoTest {

	//TESTS MANUELS
	public static void main(String[] args) {
		
		FenetreBeAnArtist f = FenetreBeAnArtist.getInstance();
		f.afficher();
		OutilSelection o = new OutilSelection(f.getPanneauDessin());
		Rectangle r = new Rectangle(10, 10, 60, 100);
		f.getPanneauDessin().ajouterVueForme(new VueRectangle(r));
		o.selectionnerForme(r);
		new ActionInfo().fire();
	}
	
}

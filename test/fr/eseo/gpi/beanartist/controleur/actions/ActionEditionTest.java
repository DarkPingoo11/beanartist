package fr.eseo.gpi.beanartist.controleur.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionEditionTest {

	private PanneauDessin pd;
	public ActionEditionTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testEdition() {
		pd.getVueFormes().clear();
		Rectangle r = new Rectangle();
		pd.ajouterVueForme(new VueRectangle(r));
		new OutilSelection(pd).selectionnerForme(r);;
		TestUtil.startTest("Editez (clic droit > Edition) <br />"
				+ "Puis changer la position et les dimensions");

		assertEquals(pd.getVueFormes().size(), 1);
		if(r.getPosition().getX() == 0 ||
			r.getPosition().getY() == 0 ||
			r.getLargeur() == 100 ||
			r.getHauteur() == 150) {
			
			fail("Les données ont mal été modifiées");
		}
	}
	
}
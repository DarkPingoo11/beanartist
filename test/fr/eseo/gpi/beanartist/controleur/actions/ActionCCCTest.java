package fr.eseo.gpi.beanartist.controleur.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionCCCTest {

	private PanneauDessin pd;
	public ActionCCCTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testBase() {
		TestUtil.startTest("Cliquer sur le bouton TEST");

		assertTrue(true);
	}
	
	@Test
	public void testCopier() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilSelection(pd).selectionnerForme(pd.getVueFormes().get(0).getForme());
		TestUtil.startTest("Copiez la figure (clic-droit > Copier)");

		assertEquals(pd.getVueFormes().size(), 1);
		if( ((OutilSelection)pd.getOutilCourant()).getFormeCopy() == null) {
			fail("Forme nulle");
		}
	}
	
	@Test
	public void testCouper() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilSelection(pd).selectionnerForme(pd.getVueFormes().get(0).getForme());
		
		TestUtil.startTest("Coupez la figure (clic-droit > Couper)");
		
		if( ((OutilSelection)pd.getOutilCourant()).getFormeCopy() == null) {
			fail("Forme nulle");
		}
		assertEquals(pd.getVueFormes().size(), 0);
		assertTrue(true);
	}
	
	@Test
	public void testColler() {
		
		pd.getVueFormes().clear();
		OutilSelection o = new OutilSelection(pd);
		o.setCopy(new Rectangle());
		
		TestUtil.startTest("Collez la figure (clic-droit > Coller > Clic)");
		
		assertEquals(pd.getVueFormes().size(), 1);
	}
	

	@Test
	public void testCopier2() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilSelection(pd).selectionnerForme(pd.getVueFormes().get(0).getForme());
		TestUtil.startTest("Copiez la figure (CTRL+C)");

		assertEquals(pd.getVueFormes().size(), 1);
		if( ((OutilSelection)pd.getOutilCourant()).getFormeCopy() == null) {
			fail("Forme nulle");
		}
	}
	
	@Test
	public void testCouper2() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilSelection(pd).selectionnerForme(pd.getVueFormes().get(0).getForme());
		
		TestUtil.startTest("Coupez la figure (CTRL+X)");
		
		if( ((OutilSelection)pd.getOutilCourant()).getFormeCopy() == null) {
			fail("Forme nulle");
		}
		assertEquals(pd.getVueFormes().size(), 0);
		assertTrue(true);
	}
	
	@Test
	public void testColler2() {
		
		pd.getVueFormes().clear();
		OutilSelection o = new OutilSelection(pd);
		o.setCopy(new Rectangle());
		
		TestUtil.startTest("Collez la figure (CTRL + V) et clic");
		
		assertEquals(pd.getVueFormes().size(), 1);
	}
	
	@Test
	public void testEffacer2() {
		
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilSelection(pd).selectionnerForme(pd.getVueFormes().get(0).getForme());
		
		TestUtil.startTest("Effacez la figure (SUPPR)");
		assertEquals(pd.getVueFormes().size(), 0);
	}
}

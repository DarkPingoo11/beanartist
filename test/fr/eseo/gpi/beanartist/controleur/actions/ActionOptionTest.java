package fr.eseo.gpi.beanartist.controleur.actions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.options.OptionTypeEnum;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauBarreOutils;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionOptionTest {

	private PanneauDessin pd;
	private PanneauBarreOutils po;
	public ActionOptionTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
		po = FenetreBeAnArtist.getInstance().getPanneauBarreOutils();
	}

	@Test
	public void testOptionAliash() {
		pd.getVueFormes().clear();
		Ellipse r = new Ellipse();
		pd.ajouterVueForme(new VueEllipse(r));
		TestUtil.startTest("Activer l'option anti-aliashing");
		
		assertTrue(UIUtil.askWarningDialog("Question", "Le cercle etait-il propre ?"));
		assertEquals(po.getOptionValue(OptionTypeEnum.ANTI_ALIASHING), true);
	}
	
	@Test
	public void testOptionGrille() {
		pd.getVueFormes().clear();
		Ellipse r = new Ellipse();
		pd.ajouterVueForme(new VueEllipse(r));
		TestUtil.startTest("Activer l'option grille");
		
		assertTrue(UIUtil.askWarningDialog("Question", "La grille était-elle visible ?"));
		assertEquals(po.getOptionValue(OptionTypeEnum.AFFICHER_GRILLE), true);
	}
	
	@Test
	public void testOptionRE() {
		pd.getVueFormes().clear();
		Ellipse r = new Ellipse();
		pd.ajouterVueForme(new VueEllipse(r));
		TestUtil.startTest("Activer l'option R.Englobant");
		
		assertTrue(UIUtil.askWarningDialog("Question", "Le rectangle englobant était-il visible ?"));
		assertEquals(po.getOptionValue(OptionTypeEnum.AFFICHER_RENGLOBANT), true);
	}
	

	@Test
	public void testOptionRP() {
		pd.getVueFormes().clear();
		Ellipse r = new Ellipse();
		pd.ajouterVueForme(new VueEllipse(r));
		new OutilSelection(pd).selectionnerForme(r);
		TestUtil.startTest("Activer l'option R.Precision ");
		
		assertTrue(UIUtil.askWarningDialog("Question", "Le rectangle bleu etait-il visible ?"));
		assertEquals(po.getOptionValue(OptionTypeEnum.AFFICHER_PRECISION), true);
	}
	
	@Test
	public void testOptionConfirm() {
		pd.getVueFormes().clear();
		Ellipse r = new Ellipse();
		pd.ajouterVueForme(new VueEllipse(r));
		new OutilSelection(pd).selectionnerForme(r);
		TestUtil.startTest("Désactiver Ignorer confirmation et cliquer sur Effacer tout<br>puis valider");
		
		assertTrue(UIUtil.askWarningDialog("Question", "La confirmation est-elle apparue ?"));
		assertEquals(po.getOptionValue(OptionTypeEnum.IGNORER_CONFIRMATIONS), false);
	}
	
}
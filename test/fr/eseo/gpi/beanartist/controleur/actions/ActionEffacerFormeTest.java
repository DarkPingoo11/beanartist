package fr.eseo.gpi.beanartist.controleur.actions;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import fr.eseo.gpi.beanartist.controleur.outils.OutilEffacer;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionEffacerFormeTest {

	private PanneauDessin pd;
	public ActionEffacerFormeTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testEffacer() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		pd.ajouterVueForme(new VueRectangle(new Rectangle(new Point(300, 300))));
		new OutilEffacer(pd);
		TestUtil.startTest("Effacer la figure (clic)");

		assertEquals(pd.getVueFormes().size(), 1);
	}
	
}
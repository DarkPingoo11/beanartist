package fr.eseo.gpi.beanartist.vue.ui;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;

public class IconesTest {

	public IconesTest() {
	}

	@Test
	public void testIconesVisibles() {
		TestUtil.startTest("Observez rapidement les boutons à droite");
		
		assertTrue(UIUtil.askWarningDialog("Question", "Les icones étaient-ils visibles"));
	}
}
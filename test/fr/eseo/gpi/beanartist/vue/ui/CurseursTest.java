package fr.eseo.gpi.beanartist.vue.ui;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilEffacer;
import fr.eseo.gpi.beanartist.controleur.outils.OutilRedimension;
import fr.eseo.gpi.beanartist.controleur.outils.OutilRemplir;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;

public class CurseursTest {

	private PanneauDessin pd;
	public CurseursTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testCurseurDeplacement() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilSelection(pd).selectionnerForme(pd.getVueFormes().get(0).getForme());;
		TestUtil.startTest("Mettez la souris au centre du rectangle");

		assertTrue(UIUtil.askWarningDialog("Question", "Le curseur a-t-il changé, sous la forme d'une main ?"));
	}
	
	@Test
	public void testCurseurRedimensionnement() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilRedimension(pd).selectionnerForme(pd.getVueFormes().get(0).getForme());;
		TestUtil.startTest("Mettez la souris sur un bord du rectangle");

		assertTrue(UIUtil.askWarningDialog("Question", "Le curseur a-t-il changé, sous la forme d'une double flèche ?"));
	}
	
	@Test
	public void testCurseurEffacer() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilEffacer(pd);
		TestUtil.startTest("Mettez la souris sur le rectangle");

		assertTrue(UIUtil.askWarningDialog("Question", "Le curseur a-t-il changé, sous la forme d'une gomme ?"));
	}
	
	@Test
	public void testCurseurRemplir() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilRemplir(pd);
		TestUtil.startTest("Mettez la souris sur le rectangle");

		assertTrue(UIUtil.askWarningDialog("Question", "Le curseur a-t-il changé, sous la forme d'une goute ?"));
	}
	
}
package fr.eseo.gpi.beanartist.vue.ui;

import lib.jiconfont.icons.FontAwesome;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.vue.ui.dialog.ProprieteDatas;
import fr.eseo.gpi.beanartist.vue.ui.dialog.ProprieteDialog;

public class ProprieteDialogTest {

	public static void main(String[] args) {
		IconFontSwing.register(GoogleMaterialDesignIcons.getIconFont());
		IconFontSwing.register(FontAwesome.getIconFont());
		
		Rectangle r = new Rectangle(10, 20, 100, 300);
		System.out.println("Mettre : 50, 60, 200, 400");
		ProprieteDatas pd = ProprieteDialog.showProprieteDialog("Titre", r);
		if(pd != null) {
			r.update(pd);
		}
		System.out.println(r.toString());
		System.out.println("Couleur Ligne : " + r.getCouleurLigne());
		System.out.println("Couleur Fond : " + r.getCouleurFond());
		
	}
}

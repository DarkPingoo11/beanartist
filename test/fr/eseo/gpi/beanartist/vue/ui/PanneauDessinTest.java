package fr.eseo.gpi.beanartist.vue.ui;

import static org.junit.Assert.assertEquals;

import java.awt.Color;

import org.junit.Test;

import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;


public class PanneauDessinTest {

	@Test
	public void testPanneauDessin() {
		PanneauDessin pd = new PanneauDessin();
		assertEquals("Taille L", 250, pd.getWidth());
		assertEquals("Taille H", 500, pd.getHeight());
		assertEquals("Color", Color.GRAY, pd.getBackground());
	}
	
	@Test
	public void testGetFormes() {
		PanneauDessin pd = new PanneauDessin();
		pd.ajouterVueForme(new VueRectangle(new Rectangle(0,0,20,10)));
		pd.ajouterVueForme(new VueRectangle(new Rectangle(50,20,100,50)));
		assertEquals("Nombre de formes = 2", 2, pd.getVueFormes().size());
	}
	
}

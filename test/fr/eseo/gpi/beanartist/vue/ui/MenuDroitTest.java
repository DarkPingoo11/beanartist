package fr.eseo.gpi.beanartist.vue.ui;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;

public class MenuDroitTest {

	public MenuDroitTest() {
	}

	@Test
	public void testMenuDroit1() {
		PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
		new OutilSelection(pd);
		TestUtil.startTest("Cliquez droit sur le dessin et observez le menu");
		
		assertTrue(UIUtil.askWarningDialog("Question", "Le menu COLLER etait-il visible ?"));
	}
	
	@Test
	public void testMenuDroit2() {
		PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
		Rectangle r = new Rectangle();
		pd.ajouterVueForme(new VueRectangle(r));
		new OutilSelection(pd).selectionnerForme(r);
		TestUtil.startTest("Cliquez droit sur la figure et observez les menus");
		
		assertTrue(UIUtil.askWarningDialog("Question", "Les menus COPIER, COUPER, COLLER, EFFACER"
				+ "PROPRIETE, PARAMETRE etaient-ils visibles ?"));
	}
	
}
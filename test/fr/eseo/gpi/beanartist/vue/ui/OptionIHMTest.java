package fr.eseo.gpi.beanartist.vue.ui;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.options.OptionTypeEnum;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;

public class OptionIHMTest {

	private PanneauDessin pd;
	public OptionIHMTest() {
		pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
	}

	@Test
	public void testGrilleVisible() {
		pd.getVueFormes().clear();
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_GRILLE, true);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_RENGLOBANT, false);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_PRECISION, false);
		TestUtil.startTest("Observez la fenêtre");

		assertTrue(UIUtil.askWarningDialog("Question", "La grille etait-elle visible ?"));
	}
	
	@Test
	public void testREnglobantVisible() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueEllipse(new Ellipse()));
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_GRILLE, false);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_RENGLOBANT, true);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_PRECISION, false);
		TestUtil.startTest("Observez la fenêtre");

		assertTrue(UIUtil.askWarningDialog("Question", "Le rectangle englobant est-il visible ?"));
	}
	
	@Test
	public void testRPrecisionVisible() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueRectangle(new Rectangle()));
		new OutilSelection(pd).selectionnerForme(pd.getVueFormes().get(0).getForme());
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_GRILLE, false);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_RENGLOBANT, false);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_PRECISION, true);
		TestUtil.startTest("Observez la fenêtre");

		assertTrue(UIUtil.askWarningDialog("Question", "Le rectangle de précision est-il visible (Rectangle bleu autour de la forme) ?"));
	}
	
	@Test
	public void testAntiAliash() {
		pd.getVueFormes().clear();
		pd.ajouterVueForme(new VueEllipse(new Ellipse()));
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_GRILLE, false);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_RENGLOBANT, true);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.AFFICHER_PRECISION, false);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(OptionTypeEnum.ANTI_ALIASHING, true);
		TestUtil.startTest("Observez la fenêtre");

		assertTrue(UIUtil.askWarningDialog("Question", "L'ellipse est elle propre (sans creneaux)?"));
	}
}
package fr.eseo.gpi.beanartist.vue.ui;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.testUtil.TestUtil;

public class MenuTest {

	public MenuTest() {
	}

	@Test
	public void testMenu() {
		TestUtil.startTest("Cliquez sur les menus Fichier, Serveur, et Validez");
		
		assertTrue(UIUtil.askWarningDialog("Question", "Les menu etait-ils visibles ?"));
	}
	
}
package fr.eseo.gpi.beanartist.vue.ui;

import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueCarre;
import fr.eseo.gpi.beanartist.vue.formes.VueCercle;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueTrace;


public class FenetreBeAnArtistTest {

	//Tests Manuels
	public static void main(String[] args) {
		FenetreBeAnArtistTest test = new FenetreBeAnArtistTest();
		
		test.testAffichageFormes();
	}
	
	public void testAffichageFormes() {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		Rectangle r1 = new Rectangle(10,10,150,60);
		Ellipse r2 = new Ellipse(30,150,80,60);
		Cercle r3 = new Cercle(10,300,100);
		Carre r4 = new Carre(200,10,40);
		Ligne l = new Ligne(new Point(10,400), new Point(320,450));
		Point p1 = new Point(20,20);
		Point p2 = new Point(50,50);
		Point p3 = new Point(10,100);
		Point p4 = new Point(200,200);
		Point p5 = new Point(66,220);
		
		Trace t = new Trace(p1, p2);
		t.ajouterPoint(p3);
		t.ajouterPoint(p4);
		t.ajouterPoint(p5);

		fen.getPanneauDessin().ajouterVueForme(new VueRectangle(r1));
		fen.getPanneauDessin().ajouterVueForme(new VueEllipse(r2));
		fen.getPanneauDessin().ajouterVueForme(new VueCercle(r3));
		fen.getPanneauDessin().ajouterVueForme(new VueCarre(r4));
		fen.getPanneauDessin().ajouterVueForme(new VueLigne(l));
		fen.getPanneauDessin().ajouterVueForme(new VueTrace(t));
		
		fen.afficher();
	}

}

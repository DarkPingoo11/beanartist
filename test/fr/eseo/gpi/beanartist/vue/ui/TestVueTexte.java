package fr.eseo.gpi.beanartist.vue.ui;

import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Texte;
import fr.eseo.gpi.beanartist.vue.formes.VueTexte;


public class TestVueTexte {

	//Tests Manuels
	public static void main(String[] args) {
		TestVueTexte test = new TestVueTexte();
		
		test.testAffichageFormes();
	}
	
	public void testAffichageFormes() {
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		Texte r4 = new Texte(new Point(10, 10), 100, 100, "Bonjour ceci est un message de bienvenue anticonstitutionellementalement"
				+ "très très long, de ce fait ceci ne devrait pas s'afficher en entier");
		
		fen.getPanneauDessin().ajouterVueForme(new VueTexte(r4));
		
		fen.afficher();
	}

}

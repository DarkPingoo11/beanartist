package fr.eseo.gpi.beanartist.testUtil;

import java.awt.Color;

import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.border.LineBorder;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;


public class TestUtil {

	private static boolean done = false;
	private static boolean first = true;
	
	public static void startTest(String objectif) {
		done = false;
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		JMenuBar menu = fen.getMenu();

		if(first) {
			JMenuItem item = new JMenuItem(new TestAction());
			item.setBackground(Color.GREEN);
			item.setBorder(new LineBorder(Color.BLUE, 2));
			menu.add(item, 3);
			first = false;
		}
		
		fen.setVisible(true);
		UIUtil.showSuccessDialog("Valider le test", "<html>" + objectif + "<br>Puis, cliquez sur Valider le test</html>");
		
		while(!done) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
			System.out.println("---Test en cours---");
		}
	}
	
	public static void testDone() {
		done = true;
	}

}

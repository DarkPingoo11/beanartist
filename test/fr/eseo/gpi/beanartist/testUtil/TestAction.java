package fr.eseo.gpi.beanartist.testUtil;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;


public class TestAction extends AbstractAction{

	private static final long serialVersionUID = 1L;

	public TestAction() {
		super("Finir le Test");
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		FenetreBeAnArtist.getInstance().setVisible(false);
		TestUtil.testDone();
	}

}

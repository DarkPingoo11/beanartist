package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

public class CercleTest {

	@Test
	public void testToString() {
		Cercle r = new Cercle(new Point(10, 10.25), 25);
		assertEquals("Test toString()", "[Cercle] pos (10 , 10,25) dim 25 x 25 périmètre : 78,54 aire : 490,87", r.toString());
	}
	
	@Test
	public void testException() {
		try {
			new Cercle(new Point(1,1), -20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
	}
	
	@Test
	public void testException2() {
		Cercle r = new Cercle(new Point(1,1), 20);
		try {
			r.setHauteur(-20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
		
		try {
			r.setLargeur(-20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
	}
	
	@Test
	public void testSetHauteur() {
		Cercle r = new Cercle(10, 10, 20);
		r.setHauteur(32.5);
		
		assertEquals("Test setHauteur()", ""+r.getHauteur(), ""+32.5);
		assertEquals("Test setHauteur()", ""+r.getLargeur(), ""+32.5);
	}

	@Test
	public void testSetLargeur() {
		Cercle r = new Cercle(10, 10, 20);
		r.setLargeur(36.5);
		
		assertEquals("Test setLargeur()", ""+r.getHauteur(), ""+36.5);
		assertEquals("Test setLargeur()", ""+r.getLargeur(), ""+36.5);
	}
	
	@Test
	public void testContient() {
		Cercle r = new Cercle(100, 100, 10);
		Point p1 = new Point(10, 10); //OUT
		Point p2 = new Point(100, 100); //OUT
		Point p3 = new Point(110, 100); //OUT
		Point p4 = new Point(100, 110); //OUT
		Point p5 = new Point(110, 110); //OUT
		
		Point p6 = new Point(110, 105);
		Point p7 = new Point(105, 100);
		Point p8 = new Point(100, 105);
		Point p9 = new Point(105, 110);
		Point p10 = new Point(105, 108);
		
		assertFalse("Point à l'exterieur", r.contient(p1));
		assertFalse("Point à l'exterieur", r.contient(p2));
		assertFalse("Point à l'exterieur", r.contient(p3));
		assertFalse("Point à l'exterieur", r.contient(p4));
		assertFalse("Point à l'exterieur", r.contient(p5));
		assertTrue("Point à l'intérieur", r.contient(p6));
		assertTrue("Point à l'intérieur", r.contient(p7));
		assertTrue("Point à l'intérieur", r.contient(p8));
		assertTrue("Point à l'intérieur", r.contient(p9));
		assertTrue("Point à l'intérieur", r.contient(p10));
	}

}

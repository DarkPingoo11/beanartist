package fr.eseo.gpi.beanartist.modele.formes;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CarreTest.class, CercleTest.class, EllipseTest.class,
		LigneTest.class, PointTest.class, RectangleTest.class, /*TraceTest.class*/ })
public class AllFormesTests {

}

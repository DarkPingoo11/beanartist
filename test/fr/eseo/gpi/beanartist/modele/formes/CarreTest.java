package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

public class CarreTest {

	@Test
	public void testToString() {
		Carre r = new Carre(new Point(10, 10.25), 25);
		assertEquals("Test toString()", "[Carre] pos (10 , 10,25) dim 25 x 25 périmètre : 100 aire : 625", r.toString());
	}
	
	@Test
	public void testException() {
		try {
			new Carre(new Point(1,1), -20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
	}
	
	@Test
	public void testException2() {
		Carre r = new Carre(new Point(1,1), 20);
		try {
			r.setHauteur(-20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
		
		try {
			r.setLargeur(-20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
	}
	
	@Test
	public void testSetHauteur() {
		Carre r = new Carre(10, 10, 20);
		r.setHauteur(32.5);
		
		assertEquals("Test setHauteur()", ""+r.getHauteur(), ""+32.5);
		assertEquals("Test setHauteur()", ""+r.getLargeur(), ""+32.5);
	}

	@Test
	public void testSetLargeur() {
		Carre r = new Carre(10, 10, 20);
		r.setLargeur(36.5);
		
		assertEquals("Test setLargeur()", ""+r.getHauteur(), ""+36.5);
		assertEquals("Test setLargeur()", ""+r.getLargeur(), ""+36.5);
	}
	
	@Test
	public void testContient() {
		Carre r = new Carre(100, 100, 20);
		Point p1 = new Point(10, 10); //OUT
		Point p2 = new Point(99, 110); //OUT
		Point p3 = new Point(110, 99); //OUT
		Point p4 = new Point(110, 110);
		Point p5 = new Point(119, 119);
		Point p6 = new Point(100, 120);
		
		assertFalse("Point à l'exterieur", r.contient(p1));
		assertFalse("Point à l'exterieur", r.contient(p2));
		assertFalse("Point à l'exterieur", r.contient(p3));
		assertTrue("Point à l'intérieur", r.contient(p4));
		assertTrue("Point à l'intérieur", r.contient(p5));
		assertTrue("Point à l'intérieur", r.contient(p6));
	}

}

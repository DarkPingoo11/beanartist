package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TraceTest {

	
	@Test
	public void testToString() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		assertEquals("Methode toString()", "[Trace] pos : (10 , 10) dim : 20 x 10 longueur : 22,36 nbSegments : 1", t1.toString());
	}
	
	
	@Test
	public void testgetPoints() {
		Trace t1 = new Trace(new Point(18, 10), new Point(30, 20));
		Point p1 = new Point(10, 15);
		Point p2 = new Point(6, 8);
		Point p3 = new Point(7, 22);
		Point p4 = new Point(9, 11);
		
		t1.ajouterPoint(p1);
		t1.ajouterPoint(p2);
		t1.ajouterPoint(p3);
		t1.ajouterPoint(p4);

		assertEquals("Methode getPoints().size()", 6, t1.getPoints().size());
		assertEquals("Methode getPoints()", new Point(18, 10).toString(), t1.getPoints().get(0).toString());
		assertEquals("Methode getPoints()", new Point(30, 20).toString(), t1.getPoints().get(1).toString());
		assertEquals("Methode getPoints()", p1, t1.getPoints().get(2));
		assertEquals("Methode getPoints()", p2, t1.getPoints().get(3));
		assertEquals("Methode getPoints()", p3, t1.getPoints().get(4));
		assertEquals("Methode getPoints()", p4, t1.getPoints().get(5));
	}
	
	
	@Test
	public void testSetPosition() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.setPosition(new Point(0, 2));
		assertEquals("Methode getPos(X)", ""+0d, ""+t1.getPosition().getX());
		assertEquals("Methode getPos(Y)", ""+2d, ""+t1.getPosition().getY());
		assertEquals("Methode getLargeur(X)", ""+20d, ""+t1.getLargeur());
		assertEquals("Methode getHauteur(Y)", ""+10d, ""+t1.getHauteur());
	}
	
	
	@Test
	public void testDeplacerVers() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.deplacerVers(0, 2);
		assertEquals("Methode getPos(X)", ""+0d, ""+t1.getPosition().getX());
		assertEquals("Methode getPos(Y)", ""+2d, ""+t1.getPosition().getY());
		assertEquals("Methode getLargeur(X)", ""+20d, ""+t1.getLargeur());
		assertEquals("Methode getHauteur(Y)", ""+10d, ""+t1.getHauteur());
	}
	
	
	@Test
	public void testDeplacerDe() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.deplacerDe(5, 2);
		assertEquals("Methode getPos(X)", ""+15d, ""+t1.getPosition().getX());
		assertEquals("Methode getPos(Y)", ""+12d, ""+t1.getPosition().getY());
		assertEquals("Methode getLargeur(X)", ""+20d, ""+t1.getLargeur());
		assertEquals("Methode getHauteur(Y)", ""+10d, ""+t1.getHauteur());
	}
	
	@Test
	public void testSetX() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.setX(5);
		assertEquals("Methode getPos(X)", ""+5d, ""+t1.getX());
		assertEquals("Methode getPos(Y)", ""+10d, ""+t1.getY());
		assertEquals("Methode getLargeur(X)", ""+20d, ""+t1.getLargeur());
		assertEquals("Methode getHauteur(Y)", ""+10d, ""+t1.getHauteur());
	}
	
	
	@Test
	public void testSetY() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.setY(5);
		assertEquals("Methode getPos(X)", ""+10d, ""+t1.getPosition().getX());
		assertEquals("Methode getPos(Y)", ""+5d, ""+t1.getPosition().getY());
		assertEquals("Methode getLargeur(X)", ""+20d, ""+t1.getLargeur());
		assertEquals("Methode getHauteur(Y)", ""+10d, ""+t1.getHauteur());
	}
	
	
	@Test
	public void testSetHauteur() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.setHauteur(30);
		assertEquals("Methode getLongueur", ""+20d, ""+t1.getLargeur());
		assertEquals("Methode getHauteur", ""+30d, ""+t1.getHauteur());
	}
	
	
	@Test
	public void testSetHauteur2() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.setHauteur(30);
		assertEquals("Methode getX(P1)", ""+10d, ""+t1.getPoints().get(0).getX());
		assertEquals("Methode getY(P1)", ""+10d, ""+t1.getPoints().get(0).getY());
		assertEquals("Methode getX(P2)", ""+30d, ""+t1.getPoints().get(1).getX());
		assertEquals("Methode getY(P2)", ""+40d, ""+t1.getPoints().get(1).getY());
	}
	
	@Test
	public void testSetHauteur3() {
		Trace t1 = new Trace(new Point(10, 10), new Point(20, 20));
		t1.ajouterPoint(new Point(15,15));
		t1.setHauteur(20);
		assertEquals("Methode getX(P1)", ""+10d, ""+t1.getPoints().get(0).getX());
		assertEquals("Methode getY(P1)", ""+10d, ""+t1.getPoints().get(0).getY());
		assertEquals("Methode getX(P2)", ""+20d, ""+t1.getPoints().get(1).getX());
		assertEquals("Methode getY(P2)", ""+30d, ""+t1.getPoints().get(1).getY());
		assertEquals("Methode getX(P3)", ""+15d, ""+t1.getPoints().get(2).getX());
		assertEquals("Methode getY(P3)", ""+20d, ""+t1.getPoints().get(2).getY());
	}
	
	
	@Test
	public void testSetLargeur() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.setLargeur(30);
		assertEquals("Methode getLongueur", ""+30d, ""+t1.getLargeur());
		assertEquals("Methode getHauteur", ""+10d, ""+t1.getHauteur());
	}
	
	
	@Test
	public void testSetLargeur2() {
		Trace t1 = new Trace(new Point(10, 10), new Point(30, 20));
		t1.setLargeur(30);
		assertEquals("Methode getX(P2)", ""+10d, ""+t1.getPoints().get(0).getX());
		assertEquals("Methode getY(P2)", ""+10d, ""+t1.getPoints().get(0).getY());
		assertEquals("Methode getX(P2)", ""+(10d+30d), ""+t1.getPoints().get(1).getX());
		assertEquals("Methode getY(P2)", ""+20d, ""+t1.getPoints().get(1).getY());
	}
	
	@Test
	public void testSetLargeur3() {
		Trace t1 = new Trace(new Point(10, 10), new Point(20, 20));
		t1.ajouterPoint(new Point(15,15));
		t1.setLargeur(20);
		assertEquals("Methode getX(P1)", ""+10d, ""+t1.getPoints().get(0).getX());
		assertEquals("Methode getY(P1)", ""+10d, ""+t1.getPoints().get(0).getY());
		assertEquals("Methode getX(P2)", ""+30d, ""+t1.getPoints().get(1).getX());
		assertEquals("Methode getY(P2)", ""+20d, ""+t1.getPoints().get(1).getY());
		assertEquals("Methode getX(P3)", ""+20d, ""+t1.getPoints().get(2).getX());
		assertEquals("Methode getY(P3)", ""+15d, ""+t1.getPoints().get(2).getY());
	}
	
	
	
	@Test
	public void testContient() {
		Trace t = new Trace(new Point(100, 100), new Point(200, 200));
		Point p1 = new Point(10, 10); //OUT
		Point p2 = new Point(99, 110); //OUT
		Point p3 = new Point(110, 99); //OUT
		Point p4 = new Point(110, 110); 
		Point p5 = new Point(133, 133);
		Point p6 = new Point(179, 179);
		
		assertFalse("Point à l'exterieur", t.contient(p1));
		assertFalse("Point à l'exterieur", t.contient(p2));
		assertFalse("Point à l'exterieur", t.contient(p3));
		assertTrue("Point à l'intérieur", t.contient(p4));
		assertTrue("Point à l'intérieur", t.contient(p5));
		assertTrue("Point à l'intérieur", t.contient(p6));
	}

}

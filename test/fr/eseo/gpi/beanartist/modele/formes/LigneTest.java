package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

public class LigneTest {

	@Test
	public void testToString() {
		Ligne l1 = new Ligne(10, 10, -5, 20);
		assertEquals("Methode toString()", "[Ligne] p1 : (10 , 10) p2 : (5 , 30) longueur : 20,62", l1.toString());
	}
	
	@Test
	public void testPoints() {
		Ligne l1 = new Ligne(10, 15, 10, 20);
		assertEquals("Methode getP1(X)", ""+10d, ""+l1.getP1().getX());
		assertEquals("Methode getP1(Y)", ""+15d, ""+l1.getP1().getY());
		assertEquals("Methode getP2(X)", ""+20d, ""+l1.getP2().getX());
		assertEquals("Methode getP2(Y)", ""+35d, ""+l1.getP2().getY());
	}
	
	@Test
	public void testSetPosition() {
		Ligne l1 = new Ligne(10, 15, 10, 20);
		l1.setPosition(new Point(0, 5));
		assertEquals("Methode getPos(X)", ""+0d, ""+l1.getPosition().getX());
		assertEquals("Methode getPos(Y)", ""+5d, ""+l1.getPosition().getY());
		assertEquals("Methode getP1(X)", ""+0d, ""+l1.getP1().getX());
		assertEquals("Methode getP1(Y)", ""+5d, ""+l1.getP1().getY());
		assertEquals("Methode getP2(X)", ""+10d, ""+l1.getP2().getX());
		assertEquals("Methode getP2(Y)", ""+25d, ""+l1.getP2().getY());
	}
	
	@Test
	public void testSetP1() {
		Ligne l1 = new Ligne(10, 15, 10, 20);
		l1.setP1(new Point(0, 0));
		assertEquals("Methode getP1(X)", ""+0d, ""+l1.getP1().getX());
		assertEquals("Methode getP1(Y)", ""+0d, ""+l1.getP1().getY());
		assertEquals("Methode getLongueur", ""+20d, ""+l1.getLargeur());
		assertEquals("Methode getHauteur", ""+35d, ""+l1.getHauteur());
		assertEquals("Methode getP2(X)", ""+20d, ""+l1.getP2().getX());
		assertEquals("Methode getP2(Y)", ""+35d, ""+l1.getP2().getY());
	}
	
	@Test
	public void testSetP2() {
		Ligne l1 = new Ligne(10, 15, 10, 20);
		l1.setP2(new Point(0, 0));
		assertEquals("Methode getP2(X)", ""+0d, ""+l1.getP2().getX());
		assertEquals("Methode getP2(Y)", ""+0d, ""+l1.getP2().getY());
		assertEquals("Methode getLongueur", ""+-10d, ""+l1.getLargeur());
		assertEquals("Methode getHauteur", ""+-15d, ""+l1.getHauteur());
		assertEquals("Methode getP1(X)", ""+10d, ""+l1.getP1().getX());
		assertEquals("Methode getP1(Y)", ""+15d, ""+l1.getP1().getY());
	}

}

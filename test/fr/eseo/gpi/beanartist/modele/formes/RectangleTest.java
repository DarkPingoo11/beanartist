package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class RectangleTest {

	@Test
	public void testToString() {
		Rectangle r = new Rectangle(new Point(10, 10.25), 25, 15);
		assertEquals("Test toString()", "[Rectangle] pos (10 , 10,25) dim 25 x 15 périmètre : 80 aire : 375", r.toString());
	}
	
	@Test
	public void testException() {
		try {
			new Rectangle(new Point(1,1), 20, -5);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
		
		try {
			new Rectangle(new Point(1,1), -20, 5);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
		
		try {
			new Rectangle(new Point(1,1), -20, -5);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
	}
	
	@Test
	public void testException2() {
		Rectangle r = new Rectangle(new Point(1,1), 20, 20);
		try {
			r.setHauteur(-20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
		
		try {
			r.setLargeur(-20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
	}
	
	@Test
	public void testContient() {
		Rectangle r = new Rectangle(100, 100, 20, 10);
		Point p1 = new Point(10, 10); //OUT
		Point p2 = new Point(99, 110); //OUT
		Point p3 = new Point(110, 99); //OUT
		Point p4 = new Point(110, 110);
		Point p5 = new Point(101, 109);
		Point p6 = new Point(119, 100);
		
		assertFalse("Point à l'exterieur", r.contient(p1));
		assertFalse("Point à l'exterieur", r.contient(p2));
		assertFalse("Point à l'exterieur", r.contient(p3));
		assertTrue("Point à l'intérieur", r.contient(p4));
		assertTrue("Point à l'intérieur", r.contient(p5));
		assertTrue("Point à l'intérieur", r.contient(p6));
	}

}

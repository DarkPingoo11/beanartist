package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

public class EllipseTest {

	@Test
	public void testToString() {
		Ellipse e = new Ellipse(new Point(10, 10.25), 25, 15);
		assertEquals("Test toString()", "[Ellipse] pos (10 , 10,25) dim 25 x 15 périmètre : 63,82 aire : 294,52", e.toString());
	}
	
	@Test
	public void testException() {
		try {
			new Ellipse(new Point(1,1), 20, -5);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
		
		try {
			new Ellipse(new Point(1,1), -20, 5);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
		
		try {
			new Ellipse(new Point(1,1), -20, -5);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
	}
	
	@Test
	public void testException2() {
		Ellipse r = new Ellipse(new Point(1,1), 20, 20);
		try {
			r.setHauteur(-20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
		
		try {
			r.setLargeur(-20);
			fail("Aucune exception de levée");
		} catch(IllegalArgumentException ex) {
			assertTrue("Exception capturée", true);
		}
	}
	
	
	@Test
	public void testChangeWidth() {
		Ellipse e = new Ellipse(new Point(10, 10.25), 25, 15);
		e.setLargeur(4.56);
		assertEquals("Test toString()", ""+4.56, ""+e.getLargeur());
	}
	
	@Test
	public void testContient() {
		Ellipse r = new Ellipse(100, 100, 20, 10);
		Point p1 = new Point(10, 10); //OUT
		Point p2 = new Point(100, 100); //OUT
		Point p3 = new Point(120, 100); //OUT
		Point p4 = new Point(100, 110); //OUT
		Point p5 = new Point(120, 110); //OUT
		Point p6 = new Point(110, 105);
		Point p7 = new Point(105, 105);
		Point p8 = new Point(118, 105);
		Point p9 = new Point(110, 110);
		
		assertFalse("Point à l'exterieur", r.contient(p1));
		assertFalse("Point à l'exterieur", r.contient(p2));
		assertFalse("Point à l'exterieur", r.contient(p3));
		assertFalse("Point à l'exterieur", r.contient(p4));
		assertFalse("Point à l'exterieur", r.contient(p5));
		assertTrue("Point à l'intérieur", r.contient(p6));
		assertTrue("Point à l'intérieur", r.contient(p7));
		assertTrue("Point à l'intérieur", r.contient(p8));
		assertTrue("Point à l'intérieur", r.contient(p9));
	}

}

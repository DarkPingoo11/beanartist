package fr.eseo.gpi.beanartist.modele.formes;

import static org.junit.Assert.*;

import org.junit.Test;

public class PointTest {

	@Test
	public void testToString() {
		Point p1 = new Point(15.5d, 17.35d);
		Point p2 = new Point(7d, 18.4521d);
		assertEquals("Methode toString()", "(15,5 , 17,35)", p1.toString());
		assertEquals("Methode toString()", "(7 , 18,45)", p2.toString());
	}
	
	@Test
	public void testDeplacerVers() {
		Point p1 = new Point(15.5d, 17.35d);
		p1.deplacerVers(12.3d, 7.88d);
		assertEquals("Methode deplacerVers (X)", ""+12.3d, ""+p1.getX());
		assertEquals("Methode deplacerVers (Y)", ""+7.88d, ""+p1.getY());
	}
	
	@Test
	public void testDeplacerDe() {
		Point p1 = new Point(15.5d, 17.35d);
		p1.deplacerDe(2.3d, 1.00d);
		assertEquals("Methode deplacerDe (X)", ""+17.8d, ""+p1.getX());
		assertEquals("Methode deplacerDe (Y)", ""+18.35d, ""+p1.getY());
	}

}

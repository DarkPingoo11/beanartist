package fr.eseo.gpi.beanartist.modele;

import java.awt.Font;

public class TexteDatas {

	private String texte;
	private Font font;
	
	public TexteDatas(String txt, String fontFamily, int taille) {
		this.texte = txt;
		this.font = new Font(fontFamily, Font.PLAIN, taille);
	}
	
	public String getTexte() {
		return this.texte;
	}
	
	public Font getFont() {
		return this.font;
	}

}

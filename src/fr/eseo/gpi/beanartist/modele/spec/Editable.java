package fr.eseo.gpi.beanartist.modele.spec;

public interface Editable {

	public void edit();
	
}

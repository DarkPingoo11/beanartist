package fr.eseo.gpi.beanartist.modele.formes;

import java.io.Serializable;

import fr.eseo.gpi.beanartist.Utils.MathUtil;

public class Point implements Serializable, Cloneable{
    
	private static final long serialVersionUID = 1L;
	private double x;
    private double y;
    
    //Constructeur général
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public Point() {
        this(0.0d, 0.0d);
    }
    
    
    public double getX() {
        return this.x;
    }
    
    public double getY() {
        return this.y;
    }
    
    public void setX(double x) {
    	this.x = x;
    }
    
    public void setY(double y) {
    	this.y = y;
    }
    
    public void deplacerVers(double x, double y) {
        this.x = x;
        this.y = y;
    }
    
    public void deplacerDe(double deltaX, double deltaY) {
        this.x += deltaX;
        this.y += deltaY;
    }
    
    public String toString() {
    	return "(" + MathUtil.formatDouble(this.getX()) + " , " + MathUtil.formatDouble(this.getY()) + ")";
    }
    
    @Override
    public Object clone() {
    	Point p = null;
    	try {
			p = (Point) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
    	return p;
    }
    
    
    
}
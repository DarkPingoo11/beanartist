package fr.eseo.gpi.beanartist.modele.formes;

public class Carre extends Rectangle{
	
	private static final long serialVersionUID = 1L;

	//Constructeur général
	public Carre(Point p, double  cote) {
    	super(p, cote, cote);
    	if(cote < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		}
    }
	
	public Carre() {
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT);
    }
    
    public Carre(Point p) {
    	this(p, Forme.LARGEUR_PAR_DEFAUT);
    }
    
    public Carre(double cote) {
    	this(new Point(), cote);
    }
    
    public Carre(double x, double y, double d) {
    	this(new Point(x, y), d);
    }
    
    @Override
    public void setHauteur(double h) {
    	super.setHauteur(h);
    	super.setLargeur(h);
    }
    
    @Override
    public void setLargeur(double l) {
    	this.setHauteur(l);
    }
}    
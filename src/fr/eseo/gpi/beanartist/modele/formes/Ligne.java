package fr.eseo.gpi.beanartist.modele.formes;

import java.awt.Color;

import fr.eseo.gpi.beanartist.Utils.MathUtil;

public class Ligne extends Forme {

	
	private static final long serialVersionUID = 1L;
	public static final double EPSILON = 0.98;
	//Constructeur général
	public Ligne(Point position, double largeur, double hauteur) {
		super(position, largeur, hauteur);
	}
	
	public Ligne() {
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	
	public Ligne(Point p) {
		this(p, Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	
	public Ligne(double x, double y, double largeur, double hauteur) {
		this(new Point(x, y), largeur, hauteur);
	}
	
	public Ligne(double largeur, double hauteur) {
		this(new Point(), largeur, hauteur);
	}
	
	public Ligne(Point p1, Point p2) {
		this(p1, p2.getX()-p1.getX(), p2.getY()-p1.getY());
	}

	public Point getP1() {
		return this.getPosition();
	}
	
	public void setP1(Point p) {
		Point p2 = this.getP2();
		this.setLargeur(p2.getX() - p.getX());
		this.setHauteur(p2.getY() - p.getY());
		this.setPosition(p);
	}
	
	public Point getP2() {
		return new Point(this.getX()+this.getLargeur(), this.getY()+this.getHauteur());
	}
	
	public void setP2(Point p) {
		this.setLargeur(p.getX() - this.getX());
		this.setHauteur(p.getY() - this.getY());
	}
	
	@Override
	public double getCadreMinX() {
		return Math.min(this.getP2().getX(),this.getP1().getX());
	}
	
	@Override
	public double getCadreMinY() {
		return Math.min(this.getP2().getY(), this.getP1().getY());//this.getP2().getY() - this.getP1().getY();
	}
	
	@Override
	public double getCadreMaxX() {
		return Math.max(this.getP2().getX(),this.getP1().getX());
	}
	
	@Override
	public double getCadreMaxY() {
		return Math.max(this.getP2().getY(), this.getP1().getY());
	}
	
	@Override
	public double aire() {
		return 0;
	}

	@Override
	public double perimetre() {
		return MathUtil.distance(this.getP1(), this.getP2());
	}
	
	public String toString() {
    	return "[" + this.getClass().getSimpleName() + "] "
    			+ "p1 : (" + MathUtil.formatDouble(this.getX()) + " , " + MathUtil.formatDouble(this.getY()) + ") "
    			+ "p2 : (" + MathUtil.formatDouble(this.getP2().getX()) + " , " + MathUtil.formatDouble(this.getP2().getY()) + ") "
    			+ "longueur : " + MathUtil.formatDouble(this.perimetre());
    }

	@Override
	public boolean contient(Point p) {
		double sommeDist = MathUtil.distance(this.getP1(), p) + MathUtil.distance(this.getP2(), p);
		return sommeDist - MathUtil.distance(this.getP1(), this.getP2()) <= EPSILON;
	}

	@Override
	public boolean contient(double x, double y) {
		return this.contient(new Point(x, y));
	}
	
	@Override
	public void remplir(Color c) {
		super.remplir(null);
	}
	
	

}

package fr.eseo.gpi.beanartist.modele.formes;

import fr.eseo.gpi.beanartist.Utils.MathUtil;

public class Cercle extends Ellipse{
	
	private static final long serialVersionUID = 1L;

	//Constructeur général
	public Cercle(Point p, double diametre) {
    	super(p, diametre, diametre);
    	
    	if(diametre < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		}
    }
	
	public Cercle() {
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT);
    }
    
    public Cercle(Point p) {
    	this(p, Forme.LARGEUR_PAR_DEFAUT);
    }
    
    public Cercle(Point p1, Point p2) {
		this(p1, p2.getX() - p1.getX());
	}
    
    public Cercle(double diametre) {
    	this(new Point(), diametre);
    }
    
    public Cercle(double x, double y, double d) {
    	this(new Point(x, y), d);
    }

    @Override
    public void setHauteur(double h) {
    	super.setHauteur(h);
    	super.setLargeur(h);
    }
    
    @Override
    public void setLargeur(double l) {
    	this.setHauteur(l);
    }
    
    @Override
    public double perimetre() {
    	return 2 * Math.PI * this.getRayon();
    }
    
    @Override
    public boolean contient(Point p) {
    	Point centre = new Point(this.getX() + this.getRayon(), this.getY() + this.getRayon());
    	return (MathUtil.distance(p, centre) <= this.getRayon());
    }
    
    @Override
    public boolean contient(double x, double y) {
    	return contient(new Point(x, y));
    }
    
    public double getRayon() {
    	return this.getHauteur() / 2;
    }
}

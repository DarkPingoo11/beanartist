package fr.eseo.gpi.beanartist.modele.formes;

public class Ellipse extends Forme{
    

	private static final long serialVersionUID = 1L;

	//Constructeur général
	public Ellipse(Point p, double largeur, double hauteur) {
		super(p, largeur, hauteur);
		
		if(largeur < 0 || hauteur < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		}
	}
	
    public Ellipse() {
        this(new Point(), Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
    }
    
    public Ellipse(Point p) {
    	this(p, Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
    }
    
    public Ellipse(Point p1, Point p2) {
		this(p1, p2.getX() - p1.getX(), p2.getY() - p1.getY());
	}
    
    public Ellipse(double largeur, double hauteur) {
    	this(new Point(), largeur, hauteur);
    }
    
    public Ellipse(double x, double y, double largeur, double hauteur) {
    	this(new Point(x, y), largeur, hauteur);
    }
    
    @Override
	public void setHauteur(double h) {
		if(h < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		} else {
			super.setHauteur(h);
		}
	}
	
	@Override
	public void setLargeur(double l) {
		if(l < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		} else {
			super.setLargeur(l);
		}
	}
	
	@Override
    public double aire() {
        return (Math.PI / 4 ) * this.getLargeur() * this.getHauteur();
    }
    
	@Override
    public double perimetre() {
    	double a = this.getHauteur() / 2;
    	double b = this.getLargeur() / 2;
    	double h = Math.pow((a-b)/(a+b), 2);
        return (Math.PI * (a + b) * (1 + (3*h)/(10+Math.sqrt(4-3*h))));
    }

	@Override
	public boolean contient(Point p) {
		double a = this.getX() + this.getLargeur() / 2;
		double b = this.getY() + this.getHauteur() / 2;
		double r = Math.pow((p.getX() - a) / (this.getLargeur() / 2), 2) + Math.pow((p.getY() - b) / (this.getHauteur() / 2), 2);
		return r <= 1;
	}

	@Override
	public boolean contient(double x, double y) {
		return this.contient(new Point(x, y));
	}
}
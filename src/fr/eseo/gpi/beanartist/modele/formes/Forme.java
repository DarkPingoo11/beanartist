package fr.eseo.gpi.beanartist.modele.formes;

import java.awt.Color;
import java.io.Serializable;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.vue.ui.dialog.ProprieteDatas;

public abstract class Forme implements Serializable, Cloneable{
	
	private static final long serialVersionUID = 1L;
	private static long globalUUID = 0;
	public static final double LARGEUR_PAR_DEFAUT = 100;
	public static final double HAUTEUR_PAR_DEFAUT = 150;
	public static final Color COULEUR_LIGNE_PAR_DEFAUT = Color.BLACK;
	public static final Color COULEUR_FOND_PAR_DEFAUT = null;
	
	private double largeur, hauteur, epaisseurLigne;
	private Point position;
	private Color couleurLigne, couleurFond;
	private long uuid;
	
	//Constructeur général
	public Forme(Point position, double largeur, double hauteur) {
		this.position = position;
		this.largeur = largeur;
		this.hauteur = hauteur;
		this.couleurLigne = COULEUR_LIGNE_PAR_DEFAUT;
		this.couleurFond = COULEUR_FOND_PAR_DEFAUT;
		this.uuid = ++globalUUID;
		this.epaisseurLigne = 1;
	}
	
	public Forme() {
		this(new Point(), LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT);
	}

	public Forme(Point position) {
		this(position, LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT);
	}
	
	public Forme(double x, double y, double largeur, double hauteur) {
		this(new Point(x, y), largeur, hauteur);
	}
	
	public Forme(double largeur, double hauteur) {
		this(new Point(), largeur, hauteur);
	}
	
	/**
	 * @return the largeur
	 */
	public double getLargeur() {
		return largeur;
	}

	/**
	 * @param largeur the largeur to set
	 */
	public void setLargeur(double largeur) {
		this.largeur = largeur;
	}

	/**
	 * @return the hauteur
	 */
	public double getHauteur() {
		return hauteur;
	}

	/**
	 * @param hauteur the hauteur to set
	 */
	public void setHauteur(double hauteur) {
		this.hauteur = hauteur;
	}
	
	/**
	 * @return the position
	 */
	public Point getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(Point position) {
		this.position = position;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return this.position.getX();
	}

	/**
	 * @param x the x to set
	 */
	public void setX(double x) {
		this.position.setX(x);
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return this.position.getY();
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.position.setY(y);
	}
	
	/**
	 * @return the epaisseurLigne
	 */
	public double getEpaisseurLigne() {
		return epaisseurLigne;
	}

	/**
	 * @param epaisseurLigne the epaisseurLigne to set
	 */
	public void setEpaisseurLigne(double epaisseurLigne) {
		if(epaisseurLigne < 0) {
			throw new IllegalArgumentException("L'épaisseur doit être positive");
		}
		
		this.epaisseurLigne = epaisseurLigne;
	}
	
	public void setCouleurLigne(Color c) {
		this.couleurLigne = (c == null ? COULEUR_LIGNE_PAR_DEFAUT : c);
	}
	
	public Color getCouleurLigne() {
		return this.couleurLigne;
	}
	
	public Color getCouleurFond() {
		return this.couleurFond;
	}
	
	public double getCadreMinX() {
		return this.getX();
	}
	
	public double getCadreMinY() {
		return this.getY();
	}
	
	public double getCadreMaxX() {
		return this.getX() + this.getLargeur();
	}
	
	public double getCadreMaxY() {
		return this.getY() + this.getHauteur();
	}
	
	
	public abstract double aire();
	public abstract double perimetre();
	public abstract boolean contient(Point p);
	public abstract boolean contient(double x, double y);
	
	public void remplir(Color c) {
		this.couleurFond = c;
	}
	
	public void deplacerVers(double x, double y) {
		this.position.deplacerVers(x, y);
	}
	
	public void deplacerDe(double deltaX, double deltaY) {
		this.position.deplacerDe(deltaX, deltaY);
	}

	public void update(ProprieteDatas pd) {
		this.setX(pd.getX());
		this.setY(pd.getY());
		this.setLargeur(pd.getLargeur());
		this.setHauteur(pd.getHauteur());
		this.setEpaisseurLigne(pd.getEpaisseur());
		this.setCouleurLigne(pd.getLigne());
		this.remplir(pd.getFond());
	}
	public String toString() {
    	return "[" + this.getClass().getSimpleName() + "] pos (" + MathUtil.formatDouble(this.getX()) + " , " + MathUtil.formatDouble(this.getY()) 
    		   + ") dim " + MathUtil.formatDouble(this.getLargeur()) + " x " + MathUtil.formatDouble(this.getHauteur()) +
    		   " périmètre : " + MathUtil.formatDouble(this.perimetre()) + " aire : " + MathUtil.formatDouble(this.aire());
    }
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Forme) {
			Forme f = ((Forme)obj);
			return f.getUuid() == this.getUuid();
		}
		return false;
	}
	
	
	@Override
	public Object clone() {
		Forme f = null;
		try {
			 f = (Forme) super.clone();
			 f.uuid = ++globalUUID;
			 f.setPosition(this.getPosition());
			 f.setHauteur(this.getHauteur());
			 f.setLargeur(this.getLargeur());
			 f.setCouleurLigne(this.getCouleurLigne());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return f;
	}

	public long getUuid() {
		return uuid;
	}

}

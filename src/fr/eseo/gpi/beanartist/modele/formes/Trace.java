package fr.eseo.gpi.beanartist.modele.formes;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import fr.eseo.gpi.beanartist.Utils.MathUtil;

public class Trace extends Forme {

	private static final long serialVersionUID = 1L;
	private List<Point> points;
	
	//Constructeur général
	public Trace(Point p1) {
		super(p1, 0, 0);
		this.points = new ArrayList<Point>();
		this.ajouterPoint(p1);
	}
	
	public Trace(Point p1, Point p2) {
		this(p1);
		this.ajouterPoint(p2);
	}
	
	public List<Point> getPoints() {
		return this.points;
	}
	
	public void ajouterPoint(Point p) {
		this.points.add(p);
		//On récupère le Xmin, Xmax, Ymin, Ymax
		double minX = this.getMinX().getX();
		double minY = this.getMinY().getY();
		double maxX = this.getMaxX().getX();
		double maxY = this.getMaxY().getY();

		super.setPosition(new Point(minX, minY));
		super.setLargeur(maxX - minX);
		super.setHauteur(maxY - minY);
	}
	
	@Override
	public void setX(double x) {
		this.setPosition(new Point(x, this.getY()));
	}
	
	@Override
	public void setY(double y) {
		this.setPosition(new Point(this.getX(), y));
	}
	
	@Override
	public void setPosition(Point p)
	{
		double dx = p.getX() - this.getX();
		double dy = p.getY() - this.getY();
		
		for(Point point : this.getPoints())
			point.deplacerDe(dx, dy);
		super.setPosition(p);
	}
	
	@Override
	public void deplacerDe(double dx, double dy) {
		this.setPosition(new Point(this.getX() + dx, this.getY() + dy));
	}
	
	@Override
	public void deplacerVers(double x, double y) {
		this.setPosition(new Point(x, y));
	}
	
	public void setLargeur(double l) {
		double xMin = this.getMinX().getX();
		double r = l / this.getLargeur();
		for(Point p : this.getPoints())
			p.deplacerVers(xMin+(p.getX()-xMin)*r, p.getY());
		super.setLargeur(l);
	}
	
	public void setHauteur(double h) {
		double yMin = this.getMinY().getY();
		double r = h / this.getHauteur();
		for(Point p : this.getPoints())
			p.deplacerVers(p.getX(), yMin+(p.getY()-yMin)*r);
		super.setHauteur(h);
	}
	
	@Override
	public String toString() {
    	return "[" + this.getClass().getSimpleName() + "] "
    			+ "pos : (" + MathUtil.formatDouble(this.getX()) + " , " + MathUtil.formatDouble(this.getY()) + ") "
    			+ "dim : " + MathUtil.formatDouble(this.getLargeur()) + " x " + MathUtil.formatDouble(this.getHauteur()) + " "
    			+ "longueur : " + MathUtil.formatDouble(this.perimetre()) + " "
    			+ "nbSegments : " + (this.getPoints().size() - 1);
    }
	
	@Override
	public double aire() {
		return 0;
	}

	@Override
	public double perimetre() {
		double lTot = 0;
		for(int i = 0; i < this.getPoints().size()-1; i++)
		{
			Ligne l = new Ligne(this.getPoints().get(i));
			l.setP2(this.getPoints().get(i+1));
			lTot += l.perimetre();
		}
		return lTot;
	}
	
	private Point getMinX() {
		Point pmin = this.points.get(0);
		for(Point p : this.points)
			if(p.getX() < pmin.getX())
				pmin = p;
		return pmin;
	}
	
	private Point getMaxX() {
		Point pmax = this.points.get(0);
		for(Point p : this.points)
			if(p.getX() > pmax.getX())
				pmax = p;
		return pmax;
	}
	
	private Point getMinY() {
		Point pmin = this.points.get(0);
		for(Point p : this.points)
			if(p.getY() < pmin.getY())
				pmin = p;
		return pmin;
	}
	
	private Point getMaxY() {
		Point pmax = this.points.get(0);
		for(Point p : this.points)
			if(p.getY() > pmax.getY())
				pmax = p;
		return pmax;
	}


	@Override
	public boolean contient(Point p) {
		for(int i = 0; i < this.getPoints().size()-1; i++) {
			Ligne l = new Ligne(this.getPoints().get(i));
			l.setP2(this.getPoints().get(i+1));
			if(l.contient(p)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean contient(double x, double y) {
		return this.contient(new Point(x, y));
	}

	@Override
	public Object clone() {
		Trace t = new Trace((Point) this.points.get(0).clone());
		for(int i = 1; i < this.getPoints().size(); i++) {
			t.ajouterPoint((Point)this.getPoints().get(i).clone());
		}
		return t;
	}
	
	@Override
	public void remplir(Color c) {
		super.remplir(null);
	}
}

package fr.eseo.gpi.beanartist.modele.formes;



public class Rectangle extends Forme{

	private static final long serialVersionUID = 1L;

	//Constructeur général
	public Rectangle(Point p, double largeur, double hauteur) {
		super(p, largeur, hauteur);
		
		if(largeur < 0 || hauteur < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		}
	}
	
	public Rectangle() {
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}

	public Rectangle(Point p) {
		this(p, Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
	}
	
	public Rectangle(Point p1, Point p2) {
		this(p1, p2.getX() - p1.getX(), p2.getY() - p1.getY());
	}

	public Rectangle(double largeur, double hauteur) {
		this(new Point(), largeur, hauteur);
	}
	
	public Rectangle(double x, double y, double largeur, double hauteur) {
		this(new Point(x, y), largeur, hauteur);
	}
	
	@Override
	public void setHauteur(double h) {
		if(h < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		} else {
			super.setHauteur(h);
		}
	}
	
	@Override
	public void setLargeur(double l) {
		if(l < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		} else {
			super.setLargeur(l);
		}
	}

	@Override
	public double aire() {
		return this.getLargeur() * this.getHauteur();
	}

	@Override
	public double perimetre() {
		return (this.getLargeur() + this.getHauteur())*2;
	}

	@Override
	public boolean contient(Point p) {
		if(p.getX() >= this.getCadreMinX() && p.getX() <= this.getCadreMaxX()) {
			if(p.getY() >= this.getCadreMinY() && p.getY() <= this.getCadreMaxY()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean contient(double x, double y) {
		return this.contient(new Point(x, y));
	}
}
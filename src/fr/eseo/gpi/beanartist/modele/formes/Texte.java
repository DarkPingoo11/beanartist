package fr.eseo.gpi.beanartist.modele.formes;

import java.awt.Font;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.modele.TexteDatas;
import fr.eseo.gpi.beanartist.modele.spec.Editable;

public class Texte extends Forme implements Editable{


	private static final long serialVersionUID = 1L;
	private static final String TEXTE_PAR_DEFAUT = "";
	private String texte;
	private Font font;

	//Constructeur général
	public Texte(Point p, double largeur, double hauteur, String texte, int taille, String family) {
		super(p, largeur, hauteur);
		this.texte = texte;
		this.font = new Font(family, Font.PLAIN, taille);
		if(largeur < 0 || hauteur < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		}
	}

	public Texte(Point p, double largeur, double hauteur, String texte) {
		this(p, largeur, hauteur, texte, 12, "Dialog");
	}

	public Texte() {
		this(new Point(), Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT, TEXTE_PAR_DEFAUT);
	}

	public Texte(Point p) {
		this(p, Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT, TEXTE_PAR_DEFAUT);
	}

	public Texte(Point p1, Point p2) {
		this(p1, p2.getX() - p1.getX(), p2.getY() - p1.getY(), TEXTE_PAR_DEFAUT);
	}

	public Texte(double largeur, double hauteur) {
		this(new Point(), largeur, hauteur, TEXTE_PAR_DEFAUT);
	}

	public Texte(double x, double y, double largeur, double hauteur) {
		this(new Point(x, y), largeur, hauteur, TEXTE_PAR_DEFAUT);
	}

	@Override
	public void setHauteur(double h) {
		if(h < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		} else {
			super.setHauteur(h);
		}
	}

	@Override
	public void setLargeur(double l) {
		if(l < 0) {
			throw new IllegalArgumentException("Les dimensions ne peuvent être négatives !");
		} else {
			super.setLargeur(l);
		}
	}

	@Override
	public double aire() {
		return 0;
	}

	@Override
	public double perimetre() {
		return 0;
	}

	@Override
	public boolean contient(Point p) {
		if(p.getX() >= this.getCadreMinX() && p.getX() <= this.getCadreMaxX()) {
			if(p.getY() >= this.getCadreMinY() && p.getY() <= this.getCadreMaxY()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean contient(double x, double y) {
		return this.contient(new Point(x, y));
	}

	public void setTexte(String texte) {
		this.texte = texte;
	}

	public String getTexte() {
		return this.texte;
	}
	
	public Font getFont() {
		return font;
	}

	public void setFont(Font font) {
		this.font = font;
	}

	@Override
	public String toString() {
		return "[" + this.getClass().getSimpleName() + "] "
				+ "pos : (" + MathUtil.formatDouble(this.getX()) + " , " + MathUtil.formatDouble(this.getY()) + ") "
				+ "texte : \"" + this.getTexte() + "\" "
				+ "police : " + this.getFont().getFamily() + " "
				+ "taille : " + this.getFont().getSize();
	}

	@Override
	public Object clone() {
		return new Texte((Point)this.getPosition().clone(), this.getLargeur(), this.getHauteur(), this.getTexte()+"");
	}

	@Override
	public void edit() {
		TexteDatas newData = UIUtil.showTexteInputDialog("Modifier le texte", 
				this.getTexte(), this.getFont().getSize(), this.getFont().getFamily());
		if(newData != null) {
			this.setTexte(newData.getTexte());
			this.setFont(newData.getFont());
		}
	}

}
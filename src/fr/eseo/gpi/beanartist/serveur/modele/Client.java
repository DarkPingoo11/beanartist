package fr.eseo.gpi.beanartist.serveur.modele;

import java.net.InetAddress;
import java.util.Date;

public class Client {

	private String name;
	private InetAddress ip;
	private int port;
	private long lastResp;
	
	public Client(String name, InetAddress ip, int port) {
		this.name = name;
		this.ip = ip;
		this.port = port;
		this.lastResp = new Date().getTime();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public InetAddress getIp() {
		return ip;
	}
	
	public String toString() {
		return this.getName() + "(" + this.getIp() + ":" + this.getPort() + ")";
	}
	public int getPort() {
		return port;
	}
	
	public long getTimeout() {
		return (new Date().getTime() - this.lastResp);
	}
	
	public void refreshTimeout() {
		this.lastResp = new Date().getTime();
	}
}

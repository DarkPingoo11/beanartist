package fr.eseo.gpi.beanartist.serveur.modele;

import java.io.Serializable;
import java.util.Date;

public class Ping implements Serializable{

	private static final long serialVersionUID = 1L;
	private long start;
	private long end;

	public Ping() {
		this.start = new Date().getTime();
		this.end = -1;
	}

	public long getStart() {
		return this.start;
	}

	public long getDuration() {
		return this.end - this.start;
	}
	
	public boolean isDone() {
		return this.end != -1;
	}
	
	/**
	 * Valide un ping, définissant la date de fin
	 */
	public void valid() {
		if(!this.isDone()) {
			this.end = new Date().getTime();
		}
	}



}

package fr.eseo.gpi.beanartist.serveur.modele;

import java.awt.Color;

public enum ColorStyle {

	BLACK("§0", Color.BLACK),
	DARK_BLUE("§1", new Color(0, 0, 170)),
	DARK_GREEN("§2", new Color(0, 170, 0)),
	DARK_AQUA("§3", new Color(0, 170, 170)),
	DARK_RED("§4", new Color(170, 0, 0)),
	DARK_PURPLE("§5", new Color(170, 0, 170)),
	GOLD("§6", new Color(255, 170, 0)),
	GRAY("§7", new Color(170, 170, 170)),
	DARK_GRAY("§8", new Color(85, 85, 85)),
	BLUE("§9", new Color(85, 85, 255)),
	GREEN("§a", new Color(85, 255, 85)),
	AQUA("§b", new Color(85, 255, 255)),
	RED("§c", new Color(255, 85, 85)),
	LIGHT_PURPLE("§d", new Color(85, 85, 255)),
	YELLOW("§e", new Color(255, 255, 85)),
	WHITE("§f", new Color(255, 255, 255)),

	RESET("", Color.BLACK);
	
	private String code;
	private Color c;
	ColorStyle(String code, Color c) {
		this.code = code;
		this.c = c;
	}
	
	public Color getColor() {
		return this.c;
	}
	public String getCode() {
		return this.code;
	}
	
	public String toString() {
		return this.code;
	}
	
	public static ColorStyle getStyle(String code) {
		for(ColorStyle cs : values()) {
			if(code.equalsIgnoreCase(cs.getCode())) {
				return cs;
			}
		}
		return RESET;
	}
	
	
}

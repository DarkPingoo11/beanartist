package fr.eseo.gpi.beanartist.serveur.modele;

public enum PacketType {

		MESSAGE,
		PING,
		START_CONNECT,
		END_CONNECT,
		CHECK_CONNECT,
		ADD_FORME,
		REMOVE_FORME,
		EDIT_FORME,
		UPDATE_FORMES;
}

package fr.eseo.gpi.beanartist.serveur.modele;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class DatasPacket implements Serializable{

	private static final long serialVersionUID = 1L;

	private Serializable message;
	private PacketType type;
	private String from;
	
	public DatasPacket(PacketType type, String from, Serializable message) {
		this.type = type;
		this.message = message;
		this.from = from;
	}
	
	/**
	 * @return the message
	 */
	public Serializable getMessage() {
		return message;
	}
	
	public String getFrom() {
		return this.from;
	}

	public byte[] getBytes() {
		byte[] serializedMessage = null;
		try {
			ByteArrayOutputStream bStream = new ByteArrayOutputStream();
			ObjectOutput oo = new ObjectOutputStream(bStream); 
			oo.writeObject(this);
			oo.close();

			serializedMessage = bStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return serializedMessage;
	}

	/**
	 * @return the type
	 */
	public PacketType getType() {
		return type;
	}

}

package fr.eseo.gpi.beanartist.serveur.vue.ui;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JTextField;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.controleur.action.ServeurConsoleSendCommand;

public class ServeurConsole extends JFrame{

	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT = 600;
	public static final int HAUTEUR_PAR_DEFAUT = 300;

	private Console console;
	private ServeurBeAnArtist serveur;

	public ServeurConsole(ServeurBeAnArtist serveur) {
		super(">_Console serveur (" + serveur.getPort() + ")");
		this.serveur = serveur;
		this.console = new Console(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT, new ServeurConsoleSendCommand(this.getServeur()));
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT));
		this.setResizable(false);
		this.setContentPane(this.console);
		this.pack();
	}

	private Console getConsolePan() {
		return this.console;
	}

	
	public JTextField getTextField() {
		return this.getConsolePan().getTextField();
	}

	public ServeurBeAnArtist getServeur() {
		return this.serveur;
	}

	public void log(String msg) {
		this.getConsolePan().log(msg);
	}
	
	public void clear() {
		this.getConsolePan().clear();
	}
}


package fr.eseo.gpi.beanartist.serveur.vue.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.Calendar;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultCaret;

public class Console extends JPanel{

	private static final long serialVersionUID = 1L;

	private JTextArea texteArea;
	private JTextField input;

	public Console(int largeur, int hauteur, AbstractAction action) {
		super();
		this.setLayout(new BorderLayout());
		Dimension dim = new Dimension(largeur, hauteur);
		this.setPreferredSize(dim);
		this.setSize(dim);
		this.initComponents();
		this.initBouton(action);
	}

	private void initComponents() {
		this.texteArea = new JTextArea();
		this.texteArea.setEditable(false);
		this.texteArea.setAutoscrolls(true);
		DefaultCaret caret = (DefaultCaret) this.texteArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		JScrollPane scroll = new JScrollPane(this.texteArea);
		scroll.setAutoscrolls(true);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		this.add(scroll);
	}
	
	private void initBouton(AbstractAction action) {
		JPanel commandBar = new JPanel();
		commandBar.setPreferredSize(new Dimension(this.getWidth(), 35));

		this.input = new JTextField();
		this.input.setPreferredSize(new Dimension(this.getWidth() - 150, 25));
		this.input.addActionListener(action);
		JButton sendButton = new JButton(action);

		commandBar.add(this.input);
		commandBar.add(sendButton);
		
		this.add(commandBar, BorderLayout.SOUTH);
	}


	public JTextField getTextField() {
		return this.input;
	}

	public void log(String msg) {
		Calendar date = Calendar.getInstance();
		int heure = date.get(Calendar.HOUR_OF_DAY);
		int minute = date.get(Calendar.MINUTE);
		int seconde = date.get(Calendar.SECOND);
		
		String dateFormat = (heure < 10 ? "0" : "") + heure  + ":" + (minute < 10 ? "0" : "") + minute + ":" + (seconde < 10 ? "0" : "") + seconde;

		msg = "(" + dateFormat + ") " + msg + "\n";

		this.texteArea.append(msg);
		this.validate();
	}
	
	public void clear() {
		this.texteArea.setText("");
	}
}


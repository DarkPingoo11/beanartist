package fr.eseo.gpi.beanartist.serveur.controleur.action.commandes;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.modele.Client;

public class KickCommande extends ServeurCommande{


	public KickCommande(ServeurBeAnArtist serveur) {
		super("/kick", serveur);
	}

	public String getHelp() {
		return "Kick le client du serveur";
	}
	
	public String getSyntaxe() {
		return "/kick <client_name> (raison)";
	}

	public boolean run(String[] args) {
		if(args.length >= 1) {
			Client c = this.getServeur().getClientByName(args[0]);
			String raison = "";
			if(args.length > 1) {
				for(int i = 1; i < args.length; i++) {
					raison += args[i] + " ";
				}
			} else {
				raison = "Vous avez été kick !";
			}
			
			if(c != null) {
				this.getServeur().deconnectClient(c, raison);
				this.getConsole().log("[Kick] Le client à été kick : " + c.toString());
			} else {
				this.getConsole().log("[Kick] Client introuvable !");
			}
		} else {
			this.getConsole().log("[Kick] La commande correcte est /kick <client_name> (raison)");
		}

		return true;
	}

}

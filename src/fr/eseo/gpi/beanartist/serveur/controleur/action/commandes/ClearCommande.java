package fr.eseo.gpi.beanartist.serveur.controleur.action.commandes;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;

public class ClearCommande extends ServeurCommande{

	
	public ClearCommande(ServeurBeAnArtist serveur) {
		super("/clear", serveur);
	}
	
	public String getHelp() {
		return "Efface la console serveur";
	}
	
	public boolean run(String[] args) {
		this.getConsole().clear();
		return true;
	}

}

package fr.eseo.gpi.beanartist.serveur.controleur.action.commandes;

import java.util.Calendar;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;

public class TimeCommande extends ServeurCommande{

	
	public TimeCommande(ServeurBeAnArtist serveur) {
		super("/time", serveur);
	}
	
	public String getHelp() {
		return "Affiche la date actuelle";
	}
	
	public boolean run(String[] args) {
		Calendar t = Calendar.getInstance();
		this.getConsole().log("[Time] Nous sommes le "
				+ t.get(Calendar.DAY_OF_MONTH) + "/" 
				+ t.get(Calendar.MONTH) + "/"
				+ t.get(Calendar.YEAR) + " et il est "
				+ t.get(Calendar.HOUR_OF_DAY) + ":" 
				+ t.get(Calendar.MINUTE) + ":" 
				+ t.get(Calendar.SECOND));
		
		return true;
	}

}

package fr.eseo.gpi.beanartist.serveur.controleur.action.commandes;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.vue.ui.ServeurConsole;

public abstract class ServeurCommande {

	private String commande;
	private ServeurBeAnArtist serveur;
	
	public ServeurCommande(String commande, ServeurBeAnArtist serveur) {
		this.commande = commande;
		this.serveur = serveur;
	}
	
	public String getCommande() {
		return this.commande;
	}
	
	public String getSyntaxe() {
		return this.commande;
	}
	
	public ServeurBeAnArtist getServeur() {
		return this.serveur;
	}
	
	public ServeurConsole getConsole() {
		return this.getServeur().getConsole();
	}
	
	public abstract String getHelp();
	public abstract boolean run(String[] args);
	
}

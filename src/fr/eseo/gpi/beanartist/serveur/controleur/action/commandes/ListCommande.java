package fr.eseo.gpi.beanartist.serveur.controleur.action.commandes;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.modele.Client;

public class ListCommande extends ServeurCommande{

	
	public ListCommande(ServeurBeAnArtist serveur) {
		super("/list", serveur);
	}
	
	public String getHelp() {
		return "Affiche la liste des utilisateurs connectés";
	}
	
	public boolean run(String[] args) {
		this.getConsole().log("Actuellement connectés : " + this.getServeur().getClients().size());
		for(Client c : this.getServeur().getClients()) {
			this.getConsole().log("-->  " + c.toString());
		}
		
		return true;
	}

}

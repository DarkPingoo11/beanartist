package fr.eseo.gpi.beanartist.serveur.controleur.action.commandes;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;

public class StopCommande extends ServeurCommande{

	
	public StopCommande(ServeurBeAnArtist serveur) {
		super("/stop", serveur);
	}
	
	public String getHelp() {
		return "Eteint le serveur et déconnecte tous les clients";
	}
	
	public boolean run(String[] args) {
		this.getConsole().log("[Shutdown] Arrêt du serveur en cours...");
		this.getServeur().stopServeur();
		this.getConsole().log("[Shutdown] Serveur éteint !");
		return true;
	}

}

package fr.eseo.gpi.beanartist.serveur.controleur.action.commandes;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.modele.Client;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.serveur.modele.Ping;

public class PingCommande extends ServeurCommande{


	public PingCommande(ServeurBeAnArtist serveur) {
		super("/ping", serveur);
	}

	public String getHelp() {
		return "Envoi un paquet au client, et attends une reponse";
	}
	
	@Override
	public String getSyntaxe() {
		return "/ping <client_name>";
	}

	public boolean run(String[] args) {
		if(args.length == 1) {
			Client c = this.getServeur().getClientByName(args[1]);
			if(c != null) {
				this.getServeur().getSocket().sendPacket(PacketType.PING, new Ping(), c);
				this.getConsole().log("[Ping] Envoie d'une requête ping à " + c.toString());
			} else {
				this.getConsole().log("[Ping] Client introuvable !");
			}
		} else {
			this.getConsole().log("[Ping] La commande correcte est /ping <client_name>");
		}

		return true;
	}

}

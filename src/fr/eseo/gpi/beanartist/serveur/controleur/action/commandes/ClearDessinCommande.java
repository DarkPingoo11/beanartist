package fr.eseo.gpi.beanartist.serveur.controleur.action.commandes;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;

public class ClearDessinCommande extends ServeurCommande{

	
	public ClearDessinCommande(ServeurBeAnArtist serveur) {
		super("/clearDessin", serveur);
	}
	
	public String getHelp() {
		return "Efface toutes les formes du serveur";
	}
	
	public boolean run(String[] args) {
		this.getServeur().getFormes().clear();
		this.getConsole().log("[Clear] Le dessin a bien été effacé !");
		return true;
	}

}

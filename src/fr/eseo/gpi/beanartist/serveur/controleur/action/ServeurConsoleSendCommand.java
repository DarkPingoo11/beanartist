package fr.eseo.gpi.beanartist.serveur.controleur.action;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JTextField;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.controleur.action.commandes.ClearCommande;
import fr.eseo.gpi.beanartist.serveur.controleur.action.commandes.ClearDessinCommande;
import fr.eseo.gpi.beanartist.serveur.controleur.action.commandes.KickCommande;
import fr.eseo.gpi.beanartist.serveur.controleur.action.commandes.ListCommande;
import fr.eseo.gpi.beanartist.serveur.controleur.action.commandes.PingCommande;
import fr.eseo.gpi.beanartist.serveur.controleur.action.commandes.ServeurCommande;
import fr.eseo.gpi.beanartist.serveur.controleur.action.commandes.StopCommande;
import fr.eseo.gpi.beanartist.serveur.controleur.action.commandes.TimeCommande;
import fr.eseo.gpi.beanartist.serveur.vue.ui.ServeurConsole;

public class ServeurConsoleSendCommand extends AbstractAction{

	private static final long serialVersionUID = 1L;
	private ServeurBeAnArtist serveur;
	private List<ServeurCommande> commandes;

	public ServeurConsoleSendCommand(ServeurBeAnArtist serveur) {
		super(">_Envoyer");
		this.serveur = serveur;
		this.commandes = new ArrayList<ServeurCommande>();
		this.initCommandes();
	}

	private void initCommandes() {
		this.getCommandes().add(new ClearCommande(this.getServeur()));
		this.getCommandes().add(new ClearDessinCommande(this.getServeur()));
		this.getCommandes().add(new ListCommande(this.getServeur()));
		this.getCommandes().add(new PingCommande(this.getServeur()));
		this.getCommandes().add(new StopCommande(this.getServeur()));
		this.getCommandes().add(new TimeCommande(this.getServeur()));
		this.getCommandes().add(new KickCommande(this.getServeur()));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String message = this.getInputField().getText();
		String[] msg = message.split(" ");
		String commande = msg[0];
		ServeurConsole console = this.getServeur().getConsole();
		this.getInputField().setText("");

		//Construction des args
		String[] args = new String[msg.length - 1];
		for(int i = 0; i < args.length; i++) {
			args[i] = msg[i+1];
		}

		if(!commande.startsWith("/")) {
			this.getServeur().getSocket().getCommunicationService().broadcastMessage(message);
		} else if(commande.equals("/help")) {
			//Commande HELP
			if(args.length == 0) {
				console.log("[Help] Voici la liste des commandes disponibles :");
				String cmds = "";
				for(ServeurCommande cmd : this.commandes) {
					cmds += ", " + cmd.getSyntaxe();
				}
				console.log("[Help]" + cmds.substring(1));
				console.log("[Help] Pour obtenir plus d'aide, /help <commande>");
			} else if(args.length == 1) {
				boolean success = false;
				for(ServeurCommande cmd : this.commandes) {
					if(cmd.getCommande().substring(1).equalsIgnoreCase(args[0])) {
						console.log("[Help] (" + cmd.getCommande() + ") " + cmd.getHelp());
						success = true;
						break;
					}
				}

				if(!success) {
					console.log("[Help] La commande " + args[0] + " n'existe pas !");
				}
			}
		} else {
			//Autres commandes
			boolean success = false;
			for(ServeurCommande cmd : this.commandes) {
				if(cmd.getCommande().equalsIgnoreCase(commande)) {
					success = cmd.run(args);
					if(success) {
						break;
					}
				}
			}

			if(!success) {
				console.log("[Erreur] Commande inconnue : "+commande);
			}
		}
	}

	public List<ServeurCommande> getCommandes() {
		return this.commandes;
	}

	public JTextField getInputField() {
		return this.getServeur().getConsole().getTextField();
	}

	public ServeurBeAnArtist getServeur() {
		return this.serveur;
	}
}

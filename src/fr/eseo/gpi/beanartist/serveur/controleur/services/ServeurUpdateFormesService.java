package fr.eseo.gpi.beanartist.serveur.controleur.services;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.modele.Client;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class ServeurUpdateFormesService extends ServeurService implements Runnable{

	public static final int CHECK_TIME_DEF = 20;

	private boolean running;

	public ServeurUpdateFormesService(SocketServeur serveur) {
		super(serveur);
		this.running = false;
	}		


	public void startService() {
		this.getConsole().log("[UFService] Démarrage du service updateFormes...");
		this.running = true;
		Thread t = new Thread(this);
		t.start();
	}

	public void stopService() {
		this.getConsole().log("[UFService] Arrêt du service updateFormes..");
		this.running = false;
	}

	//ACCESSEURS & MUTATEURS
	public boolean isRunning() {
		return this.running;
	}


	@Override
	public void run() {
		this.getConsole().log("[UFService] Service UpdateFormes demarré.");

		//Le client doit envoyer un packet au serveur pour maintenir la connexion
		while(this.isRunning()) {
			
			if(true) {
				this.sendAllFormes();
			}
			
			try {
				Thread.sleep(CHECK_TIME_DEF);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.getConsole().log("[UFService] Service  UpdateFormes stoppé.");
	}		
	
	public void sendAllFormes() {
		ServeurBeAnArtist serveur = this.getSocketServeur().getServeur();
		for(Client c : this.getSocketServeur().getServeur().getClients()) {
			serveur.getSocket().sendPacket(PacketType.UPDATE_FORMES, serveur.getFormes() , c);
		}
	}
}

package fr.eseo.gpi.beanartist.serveur.controleur.services;

import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.vue.ui.ServeurConsole;

public abstract class ServeurService{

	private SocketServeur socketServeur;
	
	public ServeurService(SocketServeur socket) {
		this.socketServeur = socket;
	}

	public SocketServeur getSocketServeur() {
		return this.socketServeur;
	}
	
	public ServeurConsole getConsole() {
		return this.getSocketServeur().getServeur().getConsole();
	}
}
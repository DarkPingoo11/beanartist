package fr.eseo.gpi.beanartist.serveur.controleur.services;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.modele.Client;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class ServeurCommunicationService extends ServeurService{

	public ServeurCommunicationService(SocketServeur socket) {
		super(socket);
	}

	public void sendPacket(DatasPacket dataPacket, InetAddress address, int port) {
		try {
			byte[] buffer = dataPacket.getBytes();
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, port);
			this.getSocketServeur().getSocket().send(packet);
			packet.setLength(buffer.length);
		} catch (IOException e) {
			this.getConsole().log("[ERREUR@CommunicationService] Impossible d'envoyer le packet !");
			e.getStackTrace();
		}
	}

	public void sendPacket(DatasPacket dataPacket, String address, int port) throws IOException {
		this.sendPacket(dataPacket, InetAddress.getByName(address), port);
	}

	public void sendAllMessage(Client from, String message) {
		if(from != null) {
			for(Client c : this.getSocketServeur().getServeur().getClients()) {
				if(!c.equals(from)) {
					this.getSocketServeur().sendPacket(PacketType.MESSAGE, from.getName() + " : " +  message, c);
				}
			}
			this.getConsole().log(from.getName() + " : " +  message);

		} else {
			this.getConsole().log("Le client n'est pas connecté");
		}
	}

	public void broadcastMessage(String message) {
		for(Client c : this.getSocketServeur().getServeur().getClients()) {
			this.getSocketServeur().sendPacket(PacketType.MESSAGE, "[Serveur] : " +  message, c);
		}
		this.getConsole().log("[Serveur] : " +  message);
	}
}
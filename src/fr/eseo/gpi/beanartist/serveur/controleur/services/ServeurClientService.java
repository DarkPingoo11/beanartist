package fr.eseo.gpi.beanartist.serveur.controleur.services;

import java.util.ArrayList;
import java.util.List;

import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.modele.Client;

public class ServeurClientService extends ServeurService implements Runnable{

	public static final int MAX_TIMEOUT_TIME_DEF = 5000;
	public static final int CHECK_TIME_DEF = 1000;

	private boolean running;

	public ServeurClientService(SocketServeur serveur) {
		super(serveur);
		this.running = false;
	}		


	public void startService() {
		this.getConsole().log("[SCService] Démarrage du service clientCheck...");
		this.running = true;
		Thread t = new Thread(this);
		t.start();
	}

	public void stopService() {
		this.getConsole().log("[SCService] Arrêt du service ClientCheck...");
		this.running = false;
	}

	//ACCESSEURS & MUTATEURS
	public boolean isRunning() {
		return this.running;
	}


	@Override
	public void run() {
		this.getConsole().log("[SCService] Service ClientCheck demarré.");

		//Le client doit envoyer un packet au serveur pour maintenir la connexion
		while(this.isRunning()) {
			List<Client> toKick = new ArrayList<Client>();
			for(Client client : this.getSocketServeur().getServeur().getClients()) {
				if(client.getTimeout() > MAX_TIMEOUT_TIME_DEF) {
					toKick.add(client);
				}
			}

			for(Client kick : toKick) {
				this.getSocketServeur().getServeur().deconnectClient(kick, "La connexion a été interrompue");
			}

			try {
				Thread.sleep(CHECK_TIME_DEF);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		this.getConsole().log("[SCService] Service  ClientCheck stoppé.");
	}		
}

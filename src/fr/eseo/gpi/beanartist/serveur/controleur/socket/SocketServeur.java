package fr.eseo.gpi.beanartist.serveur.controleur.socket;

import java.io.Serializable;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.serveur.NetServeurClientConnectListener;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.serveur.NetServeurPacketListener;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.serveur.NetServeurPing;
import fr.eseo.gpi.beanartist.serveur.controleur.services.ServeurClientService;
import fr.eseo.gpi.beanartist.serveur.controleur.services.ServeurCommunicationService;
import fr.eseo.gpi.beanartist.serveur.controleur.services.ServeurListenService;
import fr.eseo.gpi.beanartist.serveur.controleur.services.ServeurUpdateFormesService;
import fr.eseo.gpi.beanartist.serveur.modele.Client;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class SocketServeur{

	private ServeurBeAnArtist serveur;
	private DatagramSocket socket;
	
	private ServeurClientService clientCheckService; //Verification de la persistance des connexions
	private ServeurListenService listenService;
	private ServeurCommunicationService comService;
	private ServeurUpdateFormesService ufService;
	
	
	//Création de la partie socket, qui envoie et recois les connexions
	public SocketServeur(ServeurBeAnArtist serveur, int port) throws SocketException {
		this.serveur = serveur;
		this.socket = new DatagramSocket(port);
		this.clientCheckService = new ServeurClientService(this);
		this.listenService = new ServeurListenService(this);
		this.comService = new ServeurCommunicationService(this);
		this.ufService = new ServeurUpdateFormesService(this);
		
		this.initListeners();
	}

	private void initListeners() {
		this.getListenService().addNetListener(new NetServeurClientConnectListener(this));
		this.getListenService().addNetListener(new NetServeurPacketListener(this));
		this.getListenService().addNetListener(new NetServeurPing(this));
	}

	private void startServices() {
		this.getListenService().startService();
		this.getClientCheckService().startService();
		this.getUpdateFormesService().startService();
	}
	
	private void stopServices() {
		this.getListenService().stopService();
		this.getClientCheckService().stopService();
	}

	public void startSocketServer() {
		this.startServices();
	}
	
	public void stopSocketServer() {
		this.stopServices();
	}
	

	//ACCESSEURS & MUTATEURS
	public ServeurBeAnArtist getServeur() {
		return this.serveur;
	}
	
	public DatagramSocket getSocket() {
		return this.socket;
	}
	
	public ServeurListenService getListenService() {
		return this.listenService;
	}

	public ServeurCommunicationService getCommunicationService() {
		return this.comService;
	}

	public ServeurClientService getClientCheckService() {
		return clientCheckService;
	}

	public ServeurUpdateFormesService getUpdateFormesService() {
		return ufService;
	}

	public void sendPacket(DatasPacket packet, InetAddress host, int port) {
		this.getCommunicationService().sendPacket(packet, host, port);
	}
	
	public void sendPacket(DatasPacket packet, Client client) {
		this.sendPacket(packet, client.getIp(), client.getPort());
	}
	
	public void sendPacket(PacketType type, Serializable message, Client client) {
		this.sendPacket(new DatasPacket(type, "Serveur", message), client);
	}
	
}

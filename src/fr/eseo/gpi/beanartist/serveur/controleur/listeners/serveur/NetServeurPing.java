package fr.eseo.gpi.beanartist.serveur.controleur.listeners.serveur;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.serveur.modele.Ping;

public class NetServeurPing extends NetServeurAction{
	
	public NetServeurPing(SocketServeur Serveur) {
		super(Serveur);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		DatasPacket dp = e.getPacket();
		
		if(dp.getType() == PacketType.PING && !e.isRefused()) {
			Object obj = dp.getMessage();

			if(obj instanceof Ping) {
				Ping ping = (Ping) obj;

				if(ping.isDone()) {
					this.getConsole().log("[Ping] Requête reçu de " + e.getFromDestination().toString() + " en " + ping.getDuration() + "ms !");
				} else {
					ping.valid();
					this.getSocketServeur().sendPacket(new DatasPacket(PacketType.PING, "Serveur", ping), e.getFrom(), e.getPort());
				}
			} else {
				DebugUtil.error(this.getClass(), "L'objet reçu n'est pas un Ping!");
			}
		}
	}

}

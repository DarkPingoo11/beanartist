package fr.eseo.gpi.beanartist.serveur.controleur.listeners.serveur;

import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetListener;
import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.serveur.vue.ui.ServeurConsole;

public abstract class NetServeurAction implements NetListener{

	private SocketServeur serveur;
	public NetServeurAction(SocketServeur serveur) {
		this.serveur = serveur;
	}
	
	public SocketServeur getSocketServeur() {
		return this.serveur;
	}
	
	public ServeurConsole getConsole() {
		return this.getSocketServeur().getServeur().getConsole();
	}
	
	public void packetCaptured(NetEvent event) {
		//On refuse les demande entrantes si le client n'existe pas
		DatasPacket packet = event.getPacket();
		if(packet != null) {
			if(packet.getType() != PacketType.START_CONNECT && packet.getType() != PacketType.PING) {
				if(this.getSocketServeur().getServeur().getClientByIpAndPort(event.getFrom(), event.getPort()) == null) {
					event.refuse();
					this.getConsole().log("Packet entrant refusé ! (" + event.getFrom() + ":" + event.getPort() + ")");
				}
			}
		}
	}
}

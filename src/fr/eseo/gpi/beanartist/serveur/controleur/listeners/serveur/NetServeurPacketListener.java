package fr.eseo.gpi.beanartist.serveur.controleur.listeners.serveur;

import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.modele.Client;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class NetServeurPacketListener extends NetServeurAction{

	public NetServeurPacketListener(SocketServeur serveur) {
		super(serveur);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		super.packetCaptured(e);

		if(!e.isRefused()) {
			DatasPacket dp = e.getPacket();
			ServeurBeAnArtist serveur = this.getSocketServeur().getServeur();
			Client client = serveur.getClientByIpAndPort(e.getFrom(), e.getPort());
			
			if(dp.getType().equals(PacketType.MESSAGE)) {
				this.getSocketServeur().getCommunicationService().sendAllMessage(client, dp.getMessage().toString());
			} else if(dp.getType().equals(PacketType.ADD_FORME) && dp.getMessage() instanceof Forme) {
				serveur.addForme((Forme)dp.getMessage());
				this.getConsole().log(client.getName() + " à ajouté une forme " + dp.getMessage().getClass().getSimpleName());
			} else if(dp.getType().equals(PacketType.REMOVE_FORME) && dp.getMessage() instanceof Forme) {
				serveur.removeForme((Forme)dp.getMessage());
				this.getConsole().log(client.getName() + " à supprimé une forme " + dp.getMessage().getClass().getSimpleName());
			} else if(dp.getType().equals(PacketType.EDIT_FORME) && dp.getMessage() instanceof Forme) {
				Forme newForme = ((Forme)dp.getMessage());
				for(Forme f : serveur.getFormes()) {
					if(f.equals(newForme)) {
						f.setHauteur(newForme.getHauteur());
						f.setLargeur(newForme.getLargeur());
						f.setPosition(newForme.getPosition());
						f.setCouleurLigne(newForme.getCouleurLigne());
						f.remplir(newForme.getCouleurFond());
					}
				}
				//this.getConsole().log(client.getName() + " à modifié une forme " + dp.getMessage().getClass().getSimpleName());
			}
		}
	}


	
	
	
}

package fr.eseo.gpi.beanartist.serveur.controleur.listeners.serveur;

import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.modele.Client;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class NetServeurClientConnectListener extends NetServeurAction{

	public NetServeurClientConnectListener(SocketServeur socketServer) {
		super(socketServer);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		super.packetCaptured(e);

		if(!e.isRefused()) {
			DatasPacket dp = e.getPacket();
			Client client;

			if(dp.getType() == PacketType.CHECK_CONNECT) {
				client = this.getSocketServeur().getServeur().getClientByIpAndPort(e.getFrom(), e.getPort());
				//Si le client existe, on confirme et on reset le timeout
				if(client != null) {
					this.getSocketServeur().sendPacket(PacketType.CHECK_CONNECT, Boolean.TRUE, client);
					client.refreshTimeout();
				}

			} else if (dp.getType() == PacketType.START_CONNECT) {
				if(this.getSocketServeur().getServeur().getClientByIpAndPort(e.getFrom(), e.getPort()) == null) {
					String name = (String)dp.getMessage();
					client = new Client(name, e.getFrom(), e.getPort());
					this.getSocketServeur().getServeur().connectClient(client);

					this.getConsole().log("Actuellement connectés : " + this.getSocketServeur().getServeur().getClients().size());
					for(Client c : this.getSocketServeur().getServeur().getClients()) {
						this.getConsole().log("-->  " + c.toString());
					}

					this.getSocketServeur().sendPacket(PacketType.START_CONNECT, Boolean.TRUE, client);
				} else {
					this.getConsole().log("Client déjà connecté !");
				}
			}
		}
	}
}
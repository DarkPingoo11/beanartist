package fr.eseo.gpi.beanartist.serveur.controleur.listeners;

import java.net.InetAddress;

import fr.eseo.gpi.beanartist.controleur.client.NetDestination;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;

public class NetEvent{

	private NetDestination fromDest;
	private DatasPacket packet;
	private boolean refused;

	public NetEvent(DatasPacket packet, InetAddress address, int port) {
		this.packet = packet;
		this.fromDest = new NetDestination(address, port);
		this.refused = false;
	}

	public InetAddress getFrom() {
		return this.getFromDestination().getIp();
	}

	public int getPort() {
		return this.getFromDestination().getPort();
	}
	
	public NetDestination getFromDestination() {
		return this.fromDest;
	}

	public DatasPacket getPacket() {
		return this.packet;
	}
	
	public boolean isRefused() {
		return this.refused;
	}
	
	/**
	 * Refuse le packet définitivement
	 */
	public void refuse() {
		this.refused = true;
	}

}

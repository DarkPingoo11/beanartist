package fr.eseo.gpi.beanartist.serveur.controleur.listeners;

import java.util.EventListener;


public interface NetListener extends EventListener{
	
	public void packetCaptured(NetEvent event);
}

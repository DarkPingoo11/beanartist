package fr.eseo.gpi.beanartist.serveur.controleur;

import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;

import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.serveur.controleur.socket.SocketServeur;
import fr.eseo.gpi.beanartist.serveur.modele.Client;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.serveur.vue.ui.ServeurConsole;

public class ServeurBeAnArtist {

	private ServeurConsole console;
	private SocketServeur socket;
	private ArrayList<Client> clients;
	private ArrayList<Forme> formes;
	private int port;
	
	public ServeurBeAnArtist(int port) throws SocketException{
		this.port = port;
		this.socket = new SocketServeur(this, port); //initialisation su socket de communication
		this.clients = new ArrayList<Client>(); //Initialisation clients (liste vide)
		this.console = new ServeurConsole(this); //Initialisation Console
		this.formes = new ArrayList<Forme>();
		
		this.getConsole().log("Initialisation du serveur !");
	}

	public void startServeur() {
		this.getSocket().startSocketServer();
	}
	
	@SuppressWarnings("unchecked")
	public void stopServeur() {
		for(Client c : (ArrayList<Client>)this.getClients().clone()) {
			this.deconnectClient(c, "Exctinction du serveur !");
		}
		
		this.getSocket().stopSocketServer();
		this.getConsole().log("=============== SERVEUR STOPPE ===============");
	}
	
	
	public void connectClient(Client client) {
		this.clients.add(client);
		this.getSocket().sendPacket(PacketType.CHECK_CONNECT, Boolean.TRUE, client);
		client.refreshTimeout(); //On reset le timeout
		this.getConsole().log("Connexion de l'utilisateur " + client.getName() + "(" + client.getIp() + ":" + client.getPort()+ ")");
	}
	
	public void deconnectClient(Client client, String raison) {
		this.clients.remove(client);
		this.getSocket().sendPacket(PacketType.END_CONNECT, raison, client);
		this.getConsole().log("Deconnexion de l'utilisateur " + client.getName() + "(" + client.getIp() + ":" + client.getPort()+ ")");
	}
	
	public Client getClientByIpAndPort(InetAddress ip, int port) {
		for(Client c : this.getClients()) {
			if(c.getIp().equals(ip) && c.getPort() == port) {
				return c;
			}
		}
		return null;
	}
	
	public Client getClientByName(String name) {
		for(Client c : this.getClients()) {
			if(c.getName().equalsIgnoreCase(name)) {
				return c;
			}
		}
		return null;
	}

	//ACCESSEURS & MUTATEURS
	public SocketServeur getSocket() {
		return this.socket;
	}
	
	public ServeurConsole getConsole() {
		return this.console;
	}
	
	public int getPort() {
		return this.port;
	}
	
	public synchronized ArrayList<Client> getClients() {
		return this.clients;
	}
	
	public synchronized ArrayList<Forme> getFormes() {
		return this.formes;
	}
	
	public void addForme(Forme f) {
		this.getFormes().add(f);
	}
	
	public void removeForme(Forme f) {
		this.getFormes().remove(f);
	}
	
}

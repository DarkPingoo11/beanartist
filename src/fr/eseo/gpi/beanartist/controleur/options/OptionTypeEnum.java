package fr.eseo.gpi.beanartist.controleur.options;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;

public enum OptionTypeEnum {

	AFFICHER_GRILLE("Afficher grille" , GoogleMaterialDesignIcons.GRID_ON, GoogleMaterialDesignIcons.GRID_OFF),
	AFFICHER_PRECISION("Afficher R.Précision", GoogleMaterialDesignIcons.VISIBILITY, GoogleMaterialDesignIcons.VISIBILITY_OFF),
	AFFICHER_RENGLOBANT("Afficher R.Englobant", GoogleMaterialDesignIcons.BORDER_ALL, GoogleMaterialDesignIcons.BORDER_CLEAR),
	ANTI_ALIASHING("Activer Anti-Aliasing", GoogleMaterialDesignIcons.BLUR_ON, GoogleMaterialDesignIcons.BLUR_OFF),
	IGNORER_CONFIRMATIONS("Ignorer Confirmations", GoogleMaterialDesignIcons.NOTIFICATIONS_OFF, GoogleMaterialDesignIcons.NOTIFICATIONS_ACTIVE);
//GoogleMaterialDesignIcons.
	private String nom;
	private IconCode iconeE;
	private IconCode iconeD;
	OptionTypeEnum(String nom) {
		this(nom, null, null);
	}
	
	OptionTypeEnum(String nom, IconCode icone) {
		this(nom, icone, icone);
	}
	
	OptionTypeEnum(String nom, IconCode icone1, IconCode icone2) {
		this.nom = nom;
		this.iconeE = icone1;
		this.iconeD = icone2;
	}

	public String getNom() {
		return this.nom;
	}
	
	public IconCode getIconeEnabled() {
		return this.iconeE;
	}
	
	public IconCode getIconeDisabled() {
		return this.iconeD;
	}

	public static OptionTypeEnum getOptionType(String nom) {
		OptionTypeEnum o = null;
		if(nom != null) {
			for(OptionTypeEnum option : OptionTypeEnum.values()) {
				if(option.getNom().equalsIgnoreCase(nom)) {
					o = option;
					break;
				}
			}
		}
		return o;
	}
}

package fr.eseo.gpi.beanartist.controleur.options;

import java.awt.Component;

import lib.jiconfont.IconCode;

public abstract class Option {

	private Component component;
	private OptionTypeEnum type;

	public Option(OptionTypeEnum type, Component c) {
		this.component = c;
		this.type = type;
	}
	
	public Component getComposantAssocie() {
		return this.component;
	}

	public OptionTypeEnum getOptionType() {
		return type;
	}
	
	public IconCode getIconeEnabled() {
		return this.getOptionType().getIconeEnabled();
	}
	
	public IconCode getIconeDisabled() {
		return this.getOptionType().getIconeDisabled();
	}

	

}

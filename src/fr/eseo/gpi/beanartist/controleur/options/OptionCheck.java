package fr.eseo.gpi.beanartist.controleur.options;

import java.awt.Color;

import javax.swing.AbstractButton;
import javax.swing.Icon;
import javax.swing.JCheckBox;

import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.controleur.actions.ActionOption;

public class OptionCheck extends Option{

	public static final Color COULEUR_ACTIF = Color.GREEN;
	public static final Color COULEUR_INACTIF = Color.DARK_GRAY;
	
	private boolean status;
	
	public OptionCheck(OptionTypeEnum option, boolean status) {
		super(option, new JCheckBox(new ActionOption(option)));
		this.status = status;
	}
	public OptionCheck(OptionTypeEnum option) {
		this(option, false);
	}
	
	public boolean getStatus() {
		return this.status;
	}
	
	public void setStatus(boolean b) {
		this.status = b;
		((JCheckBox)this.getComposantAssocie()).setSelected(this.getStatus());
		this.getComposantAssocie().setForeground(b ? COULEUR_ACTIF : COULEUR_INACTIF);
		Icon icon = IconFontSwing.buildIcon(b ? this.getOptionType().getIconeEnabled() : this.getOptionType().getIconeDisabled(), 15);
		((AbstractButton) this.getComposantAssocie()).setIcon(icon);
	}
	
}

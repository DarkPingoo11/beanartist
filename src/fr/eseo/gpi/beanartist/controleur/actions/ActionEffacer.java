package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.controleur.options.OptionTypeEnum;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionEffacer extends AbstractAction{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Effacer Tout";

	public ActionEffacer() {
		super(NOM_ACTION);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		boolean doIt = true;
		NetworkManager nm = NetworkManager.getInstance();
		if(nm.isClientConnected()) {
			UIUtil.showErrorDialog("Action impossible", "Cette action est interdite en serveur !");
			doIt = false;
		} else if(!FenetreBeAnArtist.getInstance().getPanneauBarreOutils().getOptionValue(OptionTypeEnum.IGNORER_CONFIRMATIONS)) {
			doIt = UIUtil.askWarningDialog("Tout effacer ?", "Voulez vous vraiment effacer tous les dessins ?");
		}
		
		if(doIt) {
			FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes().clear();
			FenetreBeAnArtist.getInstance().getPanneauDessin().setVueTemporaire(null);
			FenetreBeAnArtist.getInstance().getPanneauDessin().repaint();
		}
	}

}

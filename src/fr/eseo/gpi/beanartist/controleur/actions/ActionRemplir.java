package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilRemplir;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionRemplir extends AbstractAction{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Remplir";
	
	public ActionRemplir() {
		super(NOM_ACTION);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
		if(pd.getOutilCourant() instanceof OutilRemplir) {
			Color c = FenetreBeAnArtist.getInstance().getPanneauDessin().getCouleurRemplir(); 
			c = UIUtil.pickColor("Choisir une couleur de remplissage", c);
			FenetreBeAnArtist.getInstance().getPanneauDessin().setCouleurRemplir(c);
			((OutilRemplir)pd.getOutilCourant()).updateColor();
		} else {
			new OutilRemplir(pd);
		}
	}

}

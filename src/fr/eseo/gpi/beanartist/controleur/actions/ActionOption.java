package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JCheckBox;

import fr.eseo.gpi.beanartist.controleur.options.OptionTypeEnum;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionOption extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	public ActionOption(OptionTypeEnum option) {
		super(option.getNom());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		//Récupération de l'option
		OptionTypeEnum option = OptionTypeEnum.getOptionType(e.getActionCommand());
		if(option != null) {
			boolean value = false;
			//SI CheckBox
			if(e.getSource() instanceof JCheckBox) {
				value = ((JCheckBox)e.getSource()).isSelected();
			}
			
			FenetreBeAnArtist.getInstance().getPanneauBarreOutils().setOptionValue(option, value);
			FenetreBeAnArtist.getInstance().getPanneauDessin().repaint();
		}
	}
}

package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.Utils.IOUtil;
import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionEnregistrer extends AbstractAction{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Enregistrer";
	
	public ActionEnregistrer() {
		super(NOM_ACTION);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		//DEMANDE DU NOM
		String path = UIUtil.chooseFileSavePath();
		if(path != null) {
			List<Forme> formes = new ArrayList<Forme>();
			for(VueForme v : FenetreBeAnArtist.getInstance().getPanneauDessin().getVueFormes()) {
				formes.add(v.getForme());
			}
			
			IOUtil.serializeAndSave(path, formes);
		}
	}

}

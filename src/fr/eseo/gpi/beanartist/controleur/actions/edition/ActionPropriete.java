package fr.eseo.gpi.beanartist.controleur.actions.edition;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.vue.ui.dialog.ProprieteDatas;
import fr.eseo.gpi.beanartist.vue.ui.dialog.ProprieteDialog;

public class ActionPropriete extends ActionEdition{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Propriétés";
	public static final IconCode ICONE = FontAwesome.STEAM;

	public ActionPropriete() {
		super(NOM_ACTION, ICONE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.isOutilSelection()) {
			OutilSelection o = ((OutilSelection)this.getOutilCourant());
			if(o.getFormeSelectionnee() != null) {
				Forme f = o.getFormeSelectionnee();
				ProprieteDatas pd = ProprieteDialog.showProprieteDialog("Propriétés "+f.getClass().getSimpleName(), f);
				if(pd != null) {
					f.update(pd);
					o.notifyFormeModified(f);
					this.getPanneauDessin().repaint();
				}
			}
		}
	}
}

package fr.eseo.gpi.beanartist.controleur.actions.edition;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.Utils.ColorUtil;
import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Texte;

public class ActionInfo extends ActionEdition{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Informations";
	public static final IconCode ICONE = FontAwesome.INFO;

	public ActionInfo() {
		super(NOM_ACTION, ICONE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.isOutilSelection()) {
			OutilSelection o = ((OutilSelection)this.getOutilCourant());
			if(o.getFormeSelectionnee() != null) {
				Forme f = o.getFormeSelectionnee();
				String message;
				message = "<html>"
						+ "¤ Position : &nbsp;" + f.getPosition().toString() + "<br />"
						+ "¤ Dimensions : &nbsp;" + MathUtil.arrondis(f.getLargeur()) + " x " + MathUtil.arrondis(f.getHauteur()) + "<br />";
				
				if(f instanceof Texte) {
					message += "¤ Texte: &nbsp;\"" + ((Texte)f).getTexte() + "\"<br />"
							+  "¤ Police : \"" + ((Texte)f).getFont().getFamily() + "\" (" + ((Texte)f).getFont().getSize() + "px)<br />";
				} else {
					message += "¤ Périmètre : &nbsp;" + MathUtil.arrondis(f.perimetre()) + "<br />"
							+  "¤ Aire : &nbsp;" + MathUtil.arrondis(f.aire()) + "<br />";
				}
				
				message += "¤ Couleur de ligne : &nbsp;" + ColorUtil.htmlColorize("#" + Integer.toHexString(f.getCouleurLigne().getRGB()), f.getCouleurLigne()) + "<br />"
						+ "¤ Couleur de fond : &nbsp;" 
						+ ((f.getCouleurFond() == null) ? "Aucune" :
							ColorUtil.htmlColorize("#" + Integer.toHexString(f.getCouleurFond().getRGB()), f.getCouleurFond()))
						+ "<br />"
						+ "</html>";
				
				UIUtil.showSuccessDialog("Informations : "+f.getClass().getSimpleName(), message);
			}
		}
	}

}

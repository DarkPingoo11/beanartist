package fr.eseo.gpi.beanartist.controleur.actions.edition;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import lib.jiconfont.IconCode;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.controleur.outils.Outil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public abstract class ActionEdition extends AbstractAction{

	private static final long serialVersionUID = 1L;
	private Icon icone;

	public ActionEdition(String nom, IconCode icon) {
		super(nom);
		this.icone = IconFontSwing.buildIcon(icon, 13);;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
	}
	
	public boolean isOutilSelection() {
		return (this.getOutilCourant() instanceof OutilSelection);
	}
	
	public Outil getOutilCourant() {
		return this.getPanneauDessin().getOutilCourant();
	}
	
	public PanneauDessin getPanneauDessin() {
		return FenetreBeAnArtist.getInstance().getPanneauDessin();
	}
	
	public Icon getIcone() {
		return this.icone;
	}
	
	public void fire() {
		this.actionPerformed(null);
	}

}

package fr.eseo.gpi.beanartist.controleur.actions.edition;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;

public class ActionColler extends ActionEdition{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Coller";
	public static final IconCode ICONE = FontAwesome.STICKY_NOTE;
	
	public ActionColler() {
		super(NOM_ACTION, ICONE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.isOutilSelection()) {
			OutilSelection o = ((OutilSelection)this.getOutilCourant());
			if(o.getFormeCopy() != null) {
				o.setPasting(true);
				DebugUtil.message(this.getClass(), "PASTE figure");
			}
		}
	}

}

package fr.eseo.gpi.beanartist.controleur.actions.edition;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;

public class ActionDelete extends ActionEdition{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Effacer";
	public static final IconCode ICONE = FontAwesome.TRASH;

	public ActionDelete() {
		super(NOM_ACTION, ICONE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.isOutilSelection()) {
			OutilSelection o = ((OutilSelection)this.getOutilCourant());
			if(o.getFormeSelectionnee() != null) {
				this.getPanneauDessin().retirerForme(o.getFormeSelectionnee());
				o.notifyFormeDeleted(o.getFormeSelectionnee());
				DebugUtil.message(this.getClass(), "DELETE figure");
			}
		}
	}

}

package fr.eseo.gpi.beanartist.controleur.actions.edition;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.spec.Editable;

public class ActionEdit extends ActionEdition{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Edition";
	public static final IconCode ICONE = FontAwesome.ADN;

	public ActionEdit() {
		super(NOM_ACTION, ICONE);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(this.isOutilSelection()) {
			OutilSelection o = ((OutilSelection)this.getOutilCourant());
			if(o.getFormeSelectionnee() != null) {
				Forme f = o.getFormeSelectionnee();
				if(f instanceof Editable) {
					((Editable) f).edit();
				} else {
					new ActionPropriete().fire();
				}
			}
		}
	}
}

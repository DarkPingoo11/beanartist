package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.outils.Outil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionChoisirCouleurLigne extends AbstractAction{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Choisir la couleur";
	
	public ActionChoisirCouleurLigne() {
		super(NOM_ACTION);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Color c = FenetreBeAnArtist.getInstance().getPanneauDessin().getCouleurLigneCourante();
		
		Outil outil = FenetreBeAnArtist.getInstance().getPanneauDessin().getOutilCourant();
		if(outil instanceof OutilSelection) {
			Forme f = ((OutilSelection)outil).getFormeSelectionnee();
			if(f != null) {
				c = f.getCouleurLigne();
				c = UIUtil.pickColor("Choisissez la couleur de la ligne", c);
				f.setCouleurLigne(c);
				outil.notifyFormeModified(f);
			} else {
				UIUtil.showWarningDialog("Aucune figure sélectionnée", "Merci de sélectionner une figure pour effectuer cette action");
			}
		} else {
			c = UIUtil.pickColor("Choisissez la couleur de la ligne", c);
		}
		FenetreBeAnArtist.getInstance().getPanneauDessin().setCouleurLigneCourante(c);
	}
}

package fr.eseo.gpi.beanartist.controleur.actions.dialog;

import java.awt.event.ActionEvent;

import fr.eseo.gpi.beanartist.vue.ui.dialog.EditDialog;

public class ActionAnnuler extends ActionDialog{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Annuler";
	
	public ActionAnnuler(EditDialog dialog) {
		super(NOM_ACTION, dialog);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.getDialog().setSendData(false);
		this.getDialog().setVisible(false);
	}
	
}

package fr.eseo.gpi.beanartist.controleur.actions.dialog;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.vue.ui.dialog.EditDialog;

public abstract class ActionDialog extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	private EditDialog dialog;
	
	public ActionDialog(String nom, EditDialog dialog) {
		super(nom);
		this.dialog = dialog;
	}

	protected EditDialog getDialog() {
		return this.dialog;
	}

}

package fr.eseo.gpi.beanartist.controleur.actions.dialog.propriete;

import java.awt.Color;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.vue.ui.dialog.ProprietePanel;

public class ActionChangerCouleur extends AbstractAction{

	public enum ComponentType{
		LIGNE("Ligne"), FOND("Fond");

		private String nom;
		ComponentType(String nom) {
			this.nom = nom;
		}

		public String getNom() {
			return this.nom;
		}
	}

	private static final long serialVersionUID = 1L;

	private ProprietePanel panel;
	private ComponentType ct;

	public ActionChangerCouleur(ProprietePanel panel, ComponentType ct) {
		super(ct.getNom());
		this.panel = panel;
		this.ct = ct;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Color c = null;
		switch(this.ct) {
		case FOND:
			if(e.getModifiers() == 18) {
				this.getPanel().setValueCouleurFond(null);
			} else {
				c = UIUtil.pickColor("Choisissez la couleur de remplissage", this.getPanel().getValueCouleurFond());
				if(c != null) {
					this.getPanel().setValueCouleurFond(c);
				}	
			}
			break;
		case LIGNE:
			c = UIUtil.pickColor("Choisissez la couleur de ligne", this.getPanel().getValueCouleurLigne());
			if(c != null) {
				this.getPanel().setValueCouleurLigne(c);
			}
			break;
		default:
			break;
		}
	}

	private ProprietePanel getPanel() {
		return this.panel;
	}

}

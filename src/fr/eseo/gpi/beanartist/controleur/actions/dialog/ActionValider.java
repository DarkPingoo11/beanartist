package fr.eseo.gpi.beanartist.controleur.actions.dialog;

import java.awt.event.ActionEvent;

import fr.eseo.gpi.beanartist.vue.ui.dialog.EditDialog;

public class ActionValider extends ActionDialog{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Valider";
	
	public ActionValider(EditDialog dialog) {
		super(NOM_ACTION, dialog);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		this.getDialog().setSendData(true);
		this.getDialog().setVisible(false);
	}
	
}

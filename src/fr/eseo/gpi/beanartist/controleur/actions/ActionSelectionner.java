package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionSelectionner extends AbstractAction{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Selectionner";
	
	public ActionSelectionner() {
		super(NOM_ACTION);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
		new OutilSelection(pd);
	}

}

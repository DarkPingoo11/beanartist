package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.JTextField;

import fr.eseo.gpi.beanartist.controleur.actions.reseau.commandes.ClientCommande;
import fr.eseo.gpi.beanartist.controleur.actions.reseau.commandes.LogoutCommande;
import fr.eseo.gpi.beanartist.controleur.client.ClientBeAnArtist;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.vue.ui.ClientConsole;

public class ActionSendCommand extends AbstractAction{

	private static final long serialVersionUID = 1L;
	private List<ClientCommande> commandes;

	public ActionSendCommand() {
		super("Envoyer");
		this.commandes = new ArrayList<ClientCommande>();
		this.initCommandes();
	}

	private void initCommandes() {
		this.getCommandes().add(new LogoutCommande());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String message = this.getInputField().getText();
		String[] msg = message.split(" ");
		String commande = msg[0];
		ClientConsole console = this.getClient().getConsole();
		this.getInputField().setText("");

		//Construction des args
		String[] args = new String[msg.length - 1];
		for(int i = 0; i < args.length; i++) {
			args[i] = msg[i+1];
		}

		//Message
		if(!commande.startsWith("/")) {
			if(NetworkManager.getInstance().isClientConnected()) {
				this.getClient().getSocketClient().sendPacket(PacketType.MESSAGE, message);
			}
		}else if(commande.equals("/help")) {
			//Commande HELP
			if(args.length == 0) {
				console.log("[Help] Voici la liste des commandes disponibles :");
				String cmds = "";
				for(ClientCommande cmd : this.commandes) {
					for(String c : cmd.getSyntaxe()) {
						cmds += ", " + c;
					}
				}
				console.log("[Help]" + cmds.substring(1));
				console.log("[Help] Pour obtenir plus d'aide, /help <commande>");
			} else if(args.length == 1) {
				boolean success = false;
				for(ClientCommande cmd : this.commandes) {
					if(cmd.getCommande().substring(1).equalsIgnoreCase(args[0])) {
						console.log("[Help] (" + cmd.getCommande() + ") " + cmd.getHelp());
						success = true;
						break;
					}
				}

				if(!success) {
					console.log("[Help] La commande " + args[0] + " n'existe pas !");
				}
			}
		} else {
			//Autres commandes
			boolean success = false;
			for(ClientCommande cmd : this.commandes) {
				if(cmd.getCommande().equalsIgnoreCase(commande)) {
					success = cmd.run(args);
					if(success) {
						break;
					}
				}
			}

			if(!success) {
				console.log("[Erreur] Commande inconnue : "+commande);
			}
		}
	}

	public List<ClientCommande> getCommandes() {
		return this.commandes;
	}

	public JTextField getInputField() {
		return this.getClient().getConsole().getTextField();
	}

	public ClientBeAnArtist getClient() {
		return NetworkManager.getInstance().getClient();
	}
}

package fr.eseo.gpi.beanartist.controleur.actions.reseau.commandes;

import fr.eseo.gpi.beanartist.controleur.client.ClientBeAnArtist;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.vue.ui.ClientConsole;

public abstract class ClientCommande {

	private String commande;
	
	public ClientCommande(String commande) {
		this.commande = commande;
	}
	
	public String getCommande() {
		return this.commande;
	}
	
	public String[] getSyntaxe() {
		return new String[]{this.commande};
	}
	
	public ClientBeAnArtist getClient() {
		return NetworkManager.getInstance().getClient();
	}
	
	public ClientConsole getConsole() {
		return this.getClient().getConsole();
	}
	
	public abstract String getHelp();
	public abstract boolean run(String[] args);
	
}

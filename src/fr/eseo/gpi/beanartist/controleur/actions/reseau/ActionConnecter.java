package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import java.awt.event.ActionEvent;
import java.net.InetAddress;
import java.net.UnknownHostException;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.vue.ui.ConnectWindow;
import fr.eseo.gpi.beanartist.vue.ui.dialog.ConnectDatas;
import fr.eseo.gpi.beanartist.vue.ui.dialog.ConnectDialog;

public class ActionConnecter extends ActionReseau{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Se connecter";
	public static final IconCode ICON_DEF = GoogleMaterialDesignIcons.POWER;

	public ActionConnecter() {
		super(NOM_ACTION, ICON_DEF);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		NetworkManager nm = NetworkManager.getInstance();
		if(!nm.isClientConnected()) {
			ConnectDatas cd = null;
			InetAddress ipAdd = null;
			do{
				cd = ConnectDialog.showConnectDialog("Connexion au serveur");
				try {
					if(cd != null) {
						ipAdd = InetAddress.getByName(cd.getIp());
					}
				} catch (UnknownHostException e) {
					UIUtil.showErrorDialog("Erreur d'IP", "L'adresse IP est inconnue !");
				}
			} while(cd != null && ipAdd == null);


			if(cd != null && nm.getClient() != null && ipAdd != null) {
				nm.getClient().setName(cd.getName());

				final ConnectWindow cw = new ConnectWindow();
				final InetAddress ipAddress = ipAdd;
				final int port = cd.getPort();
				
				new Thread(new Runnable(){
					public void run() {
						cw.setVisible(true);
						boolean status = nm.getClient().connectToServeur(ipAddress, port);

						cw.setVisible(false);
						cw.dispose();
						
						if(!status) {
							UIUtil.showErrorDialog("Connexion Impossible", "Le serveur est injoignable !");
						}

					}
				}).start();
			}
			
		}
	}
}
package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import java.awt.event.ActionEvent;
import java.net.SocketException;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;

public class ActionDemarrerServeur extends ActionReseau{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Démarrer un serveur";
	public static final IconCode ICON_DEF = GoogleMaterialDesignIcons.PLAY_ARROW;

	public ActionDemarrerServeur() {
		super(NOM_ACTION, ICON_DEF);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		NetworkManager nm = NetworkManager.getInstance();
		if(nm.getHostedServeur() == null) {
			
			Integer port = UIUtil.showInputIntCancelDialog("Démarrage d'un serveur", "Entrez le port d'écoute", 2345);
			while(port != null && nm.getHostedServeur() == null) {
				//Si il n'y a pas encore de serveur crée
				if(port > 0) {
					try {
						nm.createServeur(port);
						nm.getHostedServeur().startServeur();
					} catch (SocketException e) {
						port = UIUtil.showInputIntCancelDialog("Port déjà utilisé !", "Entrez le port d'écoute", port);
					}
				} else {
					port = UIUtil.showInputIntCancelDialog("Port invalide !", "Entrez le port d'écoute", port);
				}
			}
		}
	}
}
package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;

public class ActionAfficherConsoleClient extends ActionReseau{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Console client";
	public static final IconCode ICON_DEF = FontAwesome.LOW_VISION;

	public ActionAfficherConsoleClient() {
		super(NOM_ACTION, ICON_DEF);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(NetworkManager.getInstance().getClient() != null) {
			NetworkManager.getInstance().getClient().getConsole().setVisible(true);
			NetworkManager.getInstance().getClient().getConsole().setUpToDate(true);
		} else {
			NetworkManager.getInstance().reCreateClient();
		}
	}
}
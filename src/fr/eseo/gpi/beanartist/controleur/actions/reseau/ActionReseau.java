package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import lib.jiconfont.IconCode;
import lib.jiconfont.swing.IconFontSwing;

public abstract class ActionReseau extends AbstractAction{

	private static final long serialVersionUID = 1L;
	
	public ActionReseau(String nom, IconCode icon) {
		super(nom, IconFontSwing.buildIcon(icon, 16));
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {}
}
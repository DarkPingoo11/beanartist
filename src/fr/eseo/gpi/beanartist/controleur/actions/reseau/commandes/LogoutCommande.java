package fr.eseo.gpi.beanartist.controleur.actions.reseau.commandes;

import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionDeconnecter;

public class LogoutCommande extends ClientCommande{

	ActionDeconnecter deco;
	
	public LogoutCommande() {
		super("/logout");
		this.deco = new ActionDeconnecter();
	}
	
	public String getHelp() {
		return "Vous déconnecte du serveur en cours";
	}
	
	public boolean run(String[] args) {
		this.deco.actionPerformed(null);
		return true;
	}

}

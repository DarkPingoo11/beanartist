package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;

public class ActionDeconnecter extends ActionReseau{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Se déconnecter";
	public static final IconCode ICON_DEF = FontAwesome.TIMES;

	public ActionDeconnecter() {
		super(NOM_ACTION, ICON_DEF);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		NetworkManager nm = NetworkManager.getInstance();
		if(nm.isClientConnected()) {
			if(UIUtil.askWarningDialog("Déconnexion", "Voulez vous vraiment vous déconnecter ?")) {
				if(nm.isClientConnected()) {
					nm.getClient().resetConnect();
				}
			}
		}
	}
}
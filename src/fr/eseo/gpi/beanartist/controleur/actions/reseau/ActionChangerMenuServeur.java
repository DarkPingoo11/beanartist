package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ActionChangerMenuServeur implements MenuListener{

	public ActionChangerMenuServeur() {
		super();
	}

	@Override
	public void menuSelected(MenuEvent e) {
		FenetreBeAnArtist.getInstance().getMenu().updateMenu();
	}

	@Override
	public void menuCanceled(MenuEvent e) {
	}

	@Override
	public void menuDeselected(MenuEvent e) {
	}
}
package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;

public class ActionAfficherConsoleServeur extends ActionReseau{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = " Console serveur";
	public static final IconCode ICON_DEF = FontAwesome.LAPTOP;

	public ActionAfficherConsoleServeur() {
		super(NOM_ACTION, ICON_DEF);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if(NetworkManager.getInstance().getHostedServeur() != null) {
			NetworkManager.getInstance().getHostedServeur().getConsole().setVisible(true);
		}
	}
}
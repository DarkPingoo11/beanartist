package fr.eseo.gpi.beanartist.controleur.actions.reseau;

import java.awt.event.ActionEvent;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;

public class ActionStopperServeur extends ActionReseau{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Stopper le serveur";
	public static final IconCode ICON_DEF = GoogleMaterialDesignIcons.STOP;

	public ActionStopperServeur() {
		super(NOM_ACTION, ICON_DEF);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		NetworkManager nm = NetworkManager.getInstance();
		if(nm.getHostedServeur() != null) {
			nm.stopServeur();
		}
	}
}
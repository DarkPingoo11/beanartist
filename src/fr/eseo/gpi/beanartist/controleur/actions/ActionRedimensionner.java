package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import fr.eseo.gpi.beanartist.controleur.outils.OutilRedimension;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionRedimensionner extends ActionSelectionner{

	private static final long serialVersionUID = 1L;
	
	public ActionRedimensionner() {
		super();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
		new OutilRedimension(pd);
	}

}

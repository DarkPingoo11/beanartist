package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.Utils.IOUtil;
import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueCarre;
import fr.eseo.gpi.beanartist.vue.formes.VueCercle;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueTrace;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionOuvrir extends AbstractAction{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION = "Ouvrir";

	public ActionOuvrir() {
		super(NOM_ACTION);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		NetworkManager nm = NetworkManager.getInstance();
		if(nm.isClientConnected()) {
			UIUtil.showErrorDialog("Action impossible", "Cette action est interdite en serveur !");
		} else {
			String path = UIUtil.chooseFileOpenPath();
			if(path != null) {
				List<Forme> formes = IOUtil.openAndDeserialize(path);
				if(formes != null) {
					//On efface
					PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
					pd.getVueFormes().clear();
					for(Forme f : formes) {
						if(f instanceof Rectangle) {
							pd.ajouterVueForme(new VueRectangle((Rectangle)f));
						} else if(f instanceof Ellipse) {
							pd.ajouterVueForme(new VueEllipse((Ellipse)f));
						} else if(f instanceof Carre) {
							pd.ajouterVueForme(new VueCarre((Carre)f));
						} else if(f instanceof Cercle) {
							pd.ajouterVueForme(new VueCercle((Cercle)f));
						} else if(f instanceof Ligne) {
							pd.ajouterVueForme(new VueLigne((Ligne)f));
						} else if(f instanceof Trace) {
							pd.ajouterVueForme(new VueTrace((Trace)f));
						}
					}
					UIUtil.showSuccessDialog("Importation terminée !", "Les " + formes.size() + " formes ont bien été importées !");
				}
			} else {
				UIUtil.showWarningDialog("Fichier Introuvable", "Le fichier n'a pas pu être importé");
			}
		}

	}

}

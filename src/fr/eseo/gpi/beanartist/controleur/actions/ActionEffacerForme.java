package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.controleur.outils.OutilEffacer;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionEffacerForme extends AbstractAction{

	private static final long serialVersionUID = 1L;
	public static final String NOM_ACTION_EFFACERFORME = "Effacer";
	
	public ActionEffacerForme() {
		super(NOM_ACTION_EFFACERFORME);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
		new OutilEffacer(pd);
	}

}

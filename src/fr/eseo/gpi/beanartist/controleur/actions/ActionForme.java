package fr.eseo.gpi.beanartist.controleur.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilCarre;
import fr.eseo.gpi.beanartist.controleur.outils.OutilCercle;
import fr.eseo.gpi.beanartist.controleur.outils.OutilEllipse;
import fr.eseo.gpi.beanartist.controleur.outils.OutilFormeTypeEnum;
import fr.eseo.gpi.beanartist.controleur.outils.OutilLigne;
import fr.eseo.gpi.beanartist.controleur.outils.OutilRectangle;
import fr.eseo.gpi.beanartist.controleur.outils.OutilTexte;
import fr.eseo.gpi.beanartist.controleur.outils.OutilTrace;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class ActionForme extends AbstractAction{

	private static final long serialVersionUID = 1L;
	//Non utilisé en dessous
	public static final String NOM_DEFAUT = "Undefined";
	public static final String NOM_ACTION_RECTANGLE = "Rectangle";
	public static final String NOM_ACTION_ELLIPSE = "Ellipse";
	public static final String NOM_ACTION_CARRE = "Carre";
	public static final String NOM_ACTION_CERCLE = "Cercle";
	public static final String NOM_ACTION_LIGNE = "Ligne";
	//End Non utilisé
	
	public ActionForme() {
		this(null);
	}
	
	public ActionForme(OutilFormeTypeEnum o) {
		super(o.getNom());
		DebugUtil.message(this.getClass(), "Création d'un bouton pour l'outil "+o.getNom());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		OutilFormeTypeEnum outilType = OutilFormeTypeEnum.getOutilType(e.getActionCommand());
		PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
		switch(outilType) {
			case RECTANGLE :
				new OutilRectangle(pd);
				break;
			case CARRE:
				new OutilCarre(pd);
				break;
			case CERCLE:
				new OutilCercle(pd);
				break;
			case ELLIPSE:
				new OutilEllipse(pd);
				break;
			case LIGNE:
				new OutilLigne(pd);
				break;
			case TRACE:
				new OutilTrace(pd);
				break;
			case TEXTE:
				new OutilTexte(pd);
				break;
			default:
				new IllegalArgumentException("Outil inconnu !");
				break;
		}
	}

}

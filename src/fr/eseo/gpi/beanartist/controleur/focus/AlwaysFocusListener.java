package fr.eseo.gpi.beanartist.controleur.focus;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import fr.eseo.gpi.beanartist.vue.ui.PanneauBarreOutils;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class AlwaysFocusListener implements FocusListener{

	private PanneauDessin c;
	public AlwaysFocusListener(PanneauDessin c) {
		this.c = c;
		this.c.setFocusable(true);
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		this.focus();
	}

	@Override
	public void focusLost(FocusEvent e) {
		if(e.getOppositeComponent() != null && e.getOppositeComponent().getParent() instanceof PanneauBarreOutils) {
			this.focus();
		}
	}

	public void focus() {
		this.c.requestFocus();
		this.c.requestFocusInWindow();
	}

}

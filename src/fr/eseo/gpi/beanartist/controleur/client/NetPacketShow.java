package fr.eseo.gpi.beanartist.controleur.client;

import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetListener;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

@Deprecated
public class NetPacketShow implements NetListener{

	private String entity; 
	public NetPacketShow(String entity) {
		this.entity = entity;
	}

	@Override
	public void packetCaptured(NetEvent e) {
		DatasPacket dp = e.getPacket();

		Object message = dp.getMessage();
		PacketType type = dp.getType();
		String from = dp.getFrom();

		System.err.println("[" + this.entity + "] Récéption d'un paquet de " + e.getFrom() + ":" + e.getPort());
		System.err.println("          [Emmetteur] " + from);
		System.err.println("          [Type de packet] " + type);
		System.err.println("          [packet] "+ message);

	}

}

package fr.eseo.gpi.beanartist.controleur.client;

import java.net.InetAddress;
import java.net.SocketException;

import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.vue.ui.ClientConsole;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class ClientBeAnArtist {

	private String name;
	private SocketClient socketClient;
	private NetDestination connectedServer; //Server binded
	private ClientConsole console;
	
	public ClientBeAnArtist(String name) throws SocketException{
		this.name = name;
		this.socketClient = null;
		this.console = new ClientConsole(this);
		this.connectedServer = null;
		
		this.socketClient = new SocketClient(this, this.getName());
		
	}
	
	public ClientBeAnArtist() throws SocketException{
		this("User-"+(int)(Math.random()*1000));
	}
	
	public boolean connectToServeur(InetAddress ip, int port) {
		this.getConsole().log("[Connect] Etablissement de la connexion serveur en cours...");
		this.connectedServer = new NetDestination(ip, port);
		boolean status = this.getSocketClient().getConnectService().connect(5); //5 Tentatives de connexion
		
		if(status) {
			this.getConsole().log("[Connect] La connexion serveur a bien été établie");
		} else {
			this.getConsole().log("[Connect] Echec, le serveur est injoignable !");
			this.getConsole().setVisible(true);
			this.resetConnect();
		}
		
		FenetreBeAnArtist.getInstance().getMenu().updateMenu();
		return status;
	}
	
	public void resetConnect() {
		this.connectedServer = null;
		this.getSocketClient().getConnectService().disconnect();
		FenetreBeAnArtist.getInstance().getMenu().updateMenu();
	}
	
	public boolean isConnected() {
		return this.getConnectedServer() != null && this.getSocketClient().getConnectService().isConnected();
	}
	
	public NetDestination getConnectedServer() {
		return this.connectedServer;
	}
	
	public SocketClient getSocketClient() {
		return this.socketClient;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ClientConsole getConsole() {
		return this.console;
	}
	
	
}

package fr.eseo.gpi.beanartist.controleur.client.socket;

import java.io.Serializable;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import fr.eseo.gpi.beanartist.controleur.client.ClientBeAnArtist;
import fr.eseo.gpi.beanartist.controleur.client.listeners.NetClientConnectionEstablished;
import fr.eseo.gpi.beanartist.controleur.client.listeners.NetClientDisconnected;
import fr.eseo.gpi.beanartist.controleur.client.listeners.NetClientMessageReceived;
import fr.eseo.gpi.beanartist.controleur.client.listeners.NetClientPing;
import fr.eseo.gpi.beanartist.controleur.client.listeners.NetClientUpdateFormes;
import fr.eseo.gpi.beanartist.controleur.client.services.ClientCommunicationService;
import fr.eseo.gpi.beanartist.controleur.client.services.ClientConnectService;
import fr.eseo.gpi.beanartist.controleur.client.services.ClientListenService;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class SocketClient{

	private String name;
	private ClientBeAnArtist client;
	private DatagramSocket socket;
	private ClientConnectService connectService;
	private ClientListenService listenService;
	private ClientCommunicationService comService;


	public SocketClient(ClientBeAnArtist client, String name) throws SocketException {
		this.client = client;
		this.name = name;
		this.socket = new DatagramSocket();
		this.connectService = new ClientConnectService(this);
		this.listenService = new ClientListenService(this);
		this.comService = new ClientCommunicationService(this);

		this.initListeners();
		this.startServices();
	}

	private void initListeners() {
		//this.getListenService().addNetListener(new NetClientAjouterForme(this));
		this.getListenService().addNetListener(new NetClientConnectionEstablished(this));
		this.getListenService().addNetListener(new NetClientPing(this));
		this.getListenService().addNetListener(new NetClientUpdateFormes(this));
		this.getListenService().addNetListener(new NetClientDisconnected(this));
		this.getListenService().addNetListener(new NetClientMessageReceived(this));
	}

	private void startServices() {
		this.getListenService().startService();
	}

	public DatagramSocket getSocket() {
		return this.socket;
	}

	public ClientListenService getListenService() {
		return this.listenService;
	}

	public ClientCommunicationService getCommunicationService() {
		return this.comService;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public ClientBeAnArtist getClient() {
		return this.client;
	}

	public boolean isConnected() {
		return this.getConnectService().isRunning();
	}

	public ClientConnectService getConnectService() {
		return this.connectService;
	}

	public void sendPacket(DatasPacket packet, InetAddress host, int port) {
		this.getCommunicationService().sendPacket(packet, host, port);
	}

	public void sendPacket(PacketType type, Serializable message) {
		this.sendPacket(new DatasPacket(type, this.getName(), message), this.getClient().getConnectedServer().getIp(), this.getClient().getConnectedServer().getPort());
	}

}

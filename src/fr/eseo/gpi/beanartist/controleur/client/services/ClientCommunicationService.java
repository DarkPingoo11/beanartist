package fr.eseo.gpi.beanartist.controleur.client.services;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;

public class ClientCommunicationService extends ClientService{

	public ClientCommunicationService(SocketClient socket) {
		super(socket);
	}

	public void sendPacket(DatasPacket dataPacket, InetAddress address, int port) {
		try {
			byte[] buffer = dataPacket.getBytes();
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length, address, port);
			this.getSocketClient().getSocket().send(packet);
			packet.setLength(buffer.length);
		} catch (IOException e) {
			this.getConsole().log("[ERREUR@CommunicationService] Impossible d'envoyer le packet !");
			e.getStackTrace();
		}
	}
	
	public void sendPacket(DatasPacket dataPacket, String address, int port) throws IOException {
		this.sendPacket(dataPacket, InetAddress.getByName(address), port);
	}
}
package fr.eseo.gpi.beanartist.controleur.client.services;

import javax.swing.event.EventListenerList;

import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetListener;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class ClientConnectService extends ClientService implements Runnable{

	public static final int MAX_ATTEMPTS_DEF = 5;
	public static final int ATTEMPTS_TIME_DEF = 1500;

	private EventListenerList listeners = new EventListenerList();
	private boolean running;
	private boolean connected;
	private boolean checkConnect;

	public ClientConnectService(SocketClient client) {
		super(client);

		this.checkConnect = false;
		this.running = false;
		this.connected = false;
		this.listeners = new EventListenerList();
	}		


	public void startKeepConnectService() {
		if(!this.isRunning()) {
			this.getConsole().log("[KCService] Démarrage du service KeepConnect...");
			this.running = true;
			Thread t = new Thread(this);
			t.start();
		}
	}

	public void stopKeepConnectService() {
		if(this.isRunning()) {
			this.getConsole().log("[KCService] Arrêt du service KeepConnect...");
			this.running = false;
		}
	}

	public void addNetListener(NetListener listener) {
		this.listeners.add(NetListener.class, listener);
	}

	public void remNetListener(NetListener listener) {
		this.listeners.remove(NetListener.class, listener);
	}

	public NetListener[] getNetListeners() {
		return this.listeners.getListeners(NetListener.class);
	}

	public void notifyPacketCaptured(NetEvent event) {
		for(NetListener nl : this.getNetListeners()) {
			nl.packetCaptured(event);
		}
	}



	@Override
	public void run() {
		this.getConsole().log("[KCService] Service KeepConnect demarré.");
		if(this.isConnected()) {
			while(this.isRunning()) {
				//Si le serveur ne réponds pas
				if(!this.checkConnect(1)) {
					this.getConsole().log("[KCService] Connexion perdue, tentative de reconnexion...");
					if(!this.checkConnect(MAX_ATTEMPTS_DEF)) {
						this.getConsole().log("[KCService] Connexion perdue définitivement !");
						this.getSocketClient().getClient().resetConnect();
					}
				} else {
					//this.getConsole().log("[KCService] Connexion maintenue");
				}

				//On reitère deux secondes plus tard
				try {
					Thread.sleep(ATTEMPTS_TIME_DEF);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} else {
			this.running = false;
			this.getConsole().log("[KCService] Vous n'êtes connecté à aucun serveur.");
		}
		this.getConsole().log("[KCService] Service KeepConnect stoppé.");
	}		


	public boolean checkConnect(int attempts) {
		if(this.isConnected()) {
			this.setCheckConnect(false);
			for(int i = 0; i < attempts && !this.isCheckConnected(); i++) {
				this.getSocketClient().sendPacket(PacketType.CHECK_CONNECT, this.getSocketClient().getClient().getName());
				if(i > 1) {
					this.getConsole().log("[KCService] Tentative de reconnexion " + i + " / " + attempts);			
				}

				try {
					Thread.sleep(ATTEMPTS_TIME_DEF);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			return this.isCheckConnected();
		}

		return false;
	}


	public boolean connect(int attempts) {
		this.setConnected(false);
		for(int i = 0; i < attempts && !this.isConnected(); i++) {
			this.getSocketClient().sendPacket(PacketType.START_CONNECT, this.getSocketClient().getClient().getName());
			this.getConsole().log("[KCService] Tentative de connexion " + i + " / " + attempts);			
			try {
				Thread.sleep(ATTEMPTS_TIME_DEF);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		if(this.isConnected()) {
			this.getConsole().log("[KCService] Connexion au serveur reussie !");
			this.startKeepConnectService();
		} else {
			this.getConsole().log("[KCService] Echec de connexion au serveur !");
		}

		return this.isConnected();
	}

	public void disconnect() {
		if(this.isConnected()) {
			this.getConsole().log("[KCService] Connexion au serveur terminée!");
		}

		this.setConnected(false);
		this.setCheckConnect(false);
		this.stopKeepConnectService();
	}


	//ACCESSEURS & MUTATEURS
	public boolean isRunning() {
		return this.running;
	}

	public boolean isConnected() {
		return this.connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

	public boolean isCheckConnected() {
		return this.checkConnect;
	}

	public void setCheckConnect(boolean connected) {
		this.checkConnect = connected;
	}
}

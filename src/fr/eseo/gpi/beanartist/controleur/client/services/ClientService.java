package fr.eseo.gpi.beanartist.controleur.client.services;

import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.vue.ui.ClientConsole;

public abstract class ClientService{

	private SocketClient socketClient;
	
	public ClientService(SocketClient socket) {
		this.socketClient = socket;
	}

	public SocketClient getSocketClient() {
		return this.socketClient;
	}
	
	public ClientConsole getConsole() {
		return this.getSocketClient().getClient().getConsole();
	}
}
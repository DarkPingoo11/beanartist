package fr.eseo.gpi.beanartist.controleur.client.services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.DatagramPacket;

import javax.swing.event.EventListenerList;

import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetListener;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;

public class ClientListenService extends ClientService implements Runnable{

	private EventListenerList listeners = new EventListenerList();
	private boolean running;

	public ClientListenService(SocketClient Client) {
		super(Client);
		this.running = false;
		this.listeners = new EventListenerList();
	}		


	public void startService() {
		if(!this.isRunning()) {
			this.getConsole().log("[ListenService] Démarrage du service d'écoute...");
			this.running = true;
			Thread t = new Thread(this);
			t.start();
		}
	}

	public void stopService() {
		this.getConsole().log("[ListenService] Arrêt du service d'écoute...");
		this.running = false;
	}

	public void addNetListener(NetListener listener) {
		this.listeners.add(NetListener.class, listener);
	}

	public void remNetListener(NetListener listener) {
		this.listeners.remove(NetListener.class, listener);
	}

	public NetListener[] getNetListeners() {
		return this.listeners.getListeners(NetListener.class);
	}

	public void notifyPacketCaptured(NetEvent event) {
		for(NetListener nl : this.getNetListeners()) {
			nl.packetCaptured(event);
		}
	}

	@Override
	public void run() {
		this.getConsole().log("[ListenService] Service d'écoute démarré.");
		//On créer un packet buffer, qui recevra chaque packet indépendament
		byte[] buffer = new byte[8192];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);

		while(this.isRunning()){
			packet.setLength(buffer.length);

			try {
				//On attends de recevoir un packet, et on le met dans le buffer
				this.getSocketClient().getSocket().receive(packet); 
			} catch (IOException e) {
				e.printStackTrace();
			}

			ObjectInputStream iStream = null;
			Object received = null;
			try {
				iStream = new ObjectInputStream(new ByteArrayInputStream(packet.getData()));
				received = iStream.readObject();
				iStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}

			if(received instanceof DatasPacket) {
				DatasPacket rPacket = (DatasPacket) received;
				this.notifyPacketCaptured(new NetEvent(rPacket, packet.getAddress(), packet.getPort()));
			}
		}

		this.getConsole().log("[ListenService] Service d'écoute stoppé.");
	}	


	//ACCESSEURS & MUTATEURS
	public boolean isRunning() {
		return this.running;
	}

}

package fr.eseo.gpi.beanartist.controleur.client;

import java.net.SocketException;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.serveur.controleur.ServeurBeAnArtist;




public class NetworkManager {

	private static NetworkManager instance;

	private ClientBeAnArtist client;
	private ServeurBeAnArtist serveur;

	private NetworkManager() {
		this.reCreateClient();
		this.serveur = null;
	}

	public boolean isClientConnected() {
		return this.getClient() != null && this.getClient().isConnected();
	}
	
	public void reCreateClient() {
		try {
			this.client = new ClientBeAnArtist();
		} catch(SocketException e) {
			e.printStackTrace();
			DebugUtil.error(this.getClass(), "[ERREUR] Impossible de créer le client");
		}
	}

	public ClientBeAnArtist getClient() {
		return this.client;
	}
	
	public void createServeur(int port) throws SocketException {
		this.serveur = new ServeurBeAnArtist(port);
		this.serveur.getConsole().setVisible(true);
	}
	
	public void stopServeur() {
		this.getHostedServeur().stopServeur();
		this.serveur = null;
	}
	
	public ServeurBeAnArtist getHostedServeur() {
		return this.serveur;
	}

	public static NetworkManager getInstance() {
		if(instance == null) {
			instance = new NetworkManager();
		}

		return instance;
	}
}

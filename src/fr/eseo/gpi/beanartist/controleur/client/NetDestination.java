package fr.eseo.gpi.beanartist.controleur.client;

import java.net.InetAddress;

public class NetDestination {

	private InetAddress ip;
	private int port;
	
	public NetDestination(InetAddress ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public int getPort() {
		return port;
	}

	public InetAddress getIp() {
		return ip;
	}
	
	public String toString() {
		return "(" + this.getIp() + ":" + this.getPort() + ")";
	}

}

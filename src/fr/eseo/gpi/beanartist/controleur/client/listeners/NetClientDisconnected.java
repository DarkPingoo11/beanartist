package fr.eseo.gpi.beanartist.controleur.client.listeners;

import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class NetClientDisconnected extends NetClientAction{

	public NetClientDisconnected(SocketClient client) {
		super(client);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		super.packetCaptured(e);
		if(!e.isRefused()) {
			DatasPacket dp = e.getPacket();

			if(dp.getType() == PacketType.END_CONNECT) {
				Object message = dp.getMessage();

				if(message instanceof String) {
					NetworkManager nm = NetworkManager.getInstance();
					if(nm.getClient() != null) {
						nm.getClient().resetConnect();
					}
					UIUtil.showSuccessDialog("Déconnexion", (String)message);
				}
			}
		}
	}
}
package fr.eseo.gpi.beanartist.controleur.client.listeners;

import java.util.ArrayList;

import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class NetClientUpdateFormes extends NetClientAction{

	public NetClientUpdateFormes(SocketClient client) {
		super(client);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		super.packetCaptured(e);
		if(!e.isRefused()) {
			DatasPacket dp = e.getPacket();

			if(dp.getType() == PacketType.UPDATE_FORMES) {
				Object forme = dp.getMessage();

				if(forme instanceof ArrayList<?>) {
					@SuppressWarnings("unchecked")
					ArrayList<Forme> formes = (ArrayList<Forme>)forme;
					ArrayList<VueForme> vueFormes = new ArrayList<VueForme>();

					for(Forme f : formes) {
						vueFormes.add(VueForme.getVueFormeCorrespondante(f));
					}

					FenetreBeAnArtist.getInstance().getPanneauDessin().setVueFormes(vueFormes);
					//this.getConsole().log("Mise a jour des formes...");
				}
			}
		}
	}
}
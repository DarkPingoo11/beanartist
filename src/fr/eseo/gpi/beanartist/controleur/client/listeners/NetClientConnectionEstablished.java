package fr.eseo.gpi.beanartist.controleur.client.listeners;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class NetClientConnectionEstablished extends NetClientAction{

	public NetClientConnectionEstablished(SocketClient client) {
		super(client);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		DatasPacket dp = e.getPacket();
		SocketClient client = this.getSocketClient();
		
		if(client.isConnected()) {
			if(dp.getType() == PacketType.CHECK_CONNECT) {
				if(dp.getMessage() instanceof Boolean) {
					boolean status = (boolean) dp.getMessage();
					if(status) {
						//DebugUtil.message(this.getClass(), "Connexion de " +  client.getName() + " validée");
						client.getConnectService().setCheckConnect(true);
					}
				}
			} 
		} else if(dp.getType() == PacketType.START_CONNECT) {
			if(dp.getMessage() instanceof Boolean) {
				boolean status = (boolean) dp.getMessage();
				if(status) {
					client.getConnectService().setConnected(true);
					DebugUtil.message(this.getClass(), "Connexion au serveur reussie !");
				} else {
					DebugUtil.error(this.getClass(), "Connexion au serveur impossible");	
					client.getConnectService().setConnected(false);
				}
			}
		}
	}
}
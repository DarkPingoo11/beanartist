package fr.eseo.gpi.beanartist.controleur.client.listeners;

import java.util.EventListener;

import fr.eseo.gpi.beanartist.modele.formes.Forme;

public interface ClientActionListener extends EventListener{
	
	public void formeCreated(Forme f);
	public void formeDeleted(Forme f);
	public void formeModified(Forme f);

}

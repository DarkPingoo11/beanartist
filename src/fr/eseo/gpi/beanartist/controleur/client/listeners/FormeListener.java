package fr.eseo.gpi.beanartist.controleur.client.listeners;

import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;

public class FormeListener implements ClientActionListener{
	
	public FormeListener() {
	}

	@Override
	public void formeCreated(Forme f) {
		if(this.getManager().isClientConnected()) {
			this.getManager().getClient().getSocketClient().sendPacket(PacketType.ADD_FORME, f);
		}
	}

	@Override
	public void formeDeleted(Forme f) {
		if(this.getManager().isClientConnected()) {
			this.getManager().getClient().getSocketClient().sendPacket(PacketType.REMOVE_FORME, f);
		}
	}
	
	@Override
	public void formeModified(Forme f) {
		if(this.getManager().isClientConnected()) {
			this.getManager().getClient().getSocketClient().sendPacket(PacketType.EDIT_FORME, f);
		}
	}
	
	public NetworkManager getManager() {
		return NetworkManager.getInstance();
	}

}

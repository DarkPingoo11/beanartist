package fr.eseo.gpi.beanartist.controleur.client.listeners;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

@Deprecated
public class NetClientRetirerForme extends NetClientAction{

	public NetClientRetirerForme(SocketClient client) {
		super(client);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		DatasPacket dp = e.getPacket();

		if(dp.getType() == PacketType.REMOVE_FORME) {
			Object forme = dp.getMessage();

			if(forme instanceof Forme) {
				FenetreBeAnArtist.getInstance().getPanneauDessin().retirerForme((Forme)forme);
				DebugUtil.message(this.getClass(), "Retrait d'une vueForme de " + e.getPacket().getFrom());
			} else {
				DebugUtil.error(this.getClass(), "L'objet reçu n'est pas une forme (REMOVE_FORME)!");
			}
		}

	}

}

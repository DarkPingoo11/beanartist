package fr.eseo.gpi.beanartist.controleur.client.listeners;

import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public class NetClientMessageReceived extends NetClientAction{

	public NetClientMessageReceived(SocketClient client) {
		super(client);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		super.packetCaptured(e);
		if(!e.isRefused()) {
			DatasPacket dp = e.getPacket();

			if(dp.getType() == PacketType.MESSAGE) {
				if(dp.getMessage() instanceof String) {
					String message = (String)dp.getMessage();
					this.getConsole().log(message);
					this.getConsole().setUpToDate(false);
					FenetreBeAnArtist.getInstance().getMenu().updateMenu();
				}
			}

		}
	}
}
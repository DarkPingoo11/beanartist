package fr.eseo.gpi.beanartist.controleur.client.listeners;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.serveur.modele.Ping;

public class NetClientPing extends NetClientAction{
	
	public NetClientPing(SocketClient client) {
		super(client);
	}

	@Override
	public void packetCaptured(NetEvent e) {
		DatasPacket dp = e.getPacket();
		
		if(dp.getType() == PacketType.PING && !e.isRefused()) {
			Object obj = dp.getMessage();

			if(obj instanceof Ping) {
				Ping ping = (Ping) obj;

				if(ping.isDone()) {
					this.getConsole().log("[Ping] Requête reçu de " + e.getFromDestination().toString() + " en " + ping.getDuration() + "ms !");
				} else {
					ping.valid();
					this.getSocketClient().sendPacket(PacketType.PING, ping);
				}
			} else {
				DebugUtil.error(this.getClass(), "L'objet reçu n'est pas un Ping!");
			}
		}
	}

}

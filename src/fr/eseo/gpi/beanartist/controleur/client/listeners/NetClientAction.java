package fr.eseo.gpi.beanartist.controleur.client.listeners;

import fr.eseo.gpi.beanartist.controleur.client.NetDestination;
import fr.eseo.gpi.beanartist.controleur.client.socket.SocketClient;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetEvent;
import fr.eseo.gpi.beanartist.serveur.controleur.listeners.NetListener;
import fr.eseo.gpi.beanartist.serveur.modele.DatasPacket;
import fr.eseo.gpi.beanartist.serveur.modele.PacketType;
import fr.eseo.gpi.beanartist.vue.ui.ClientConsole;

public abstract class NetClientAction implements NetListener{

	private SocketClient client;
	public NetClientAction(SocketClient client) {
		this.client = client;
	}
	
	public SocketClient getSocketClient() {
		return this.client;
	}
	
	public ClientConsole getConsole() {
		return this.getSocketClient().getClient().getConsole();
	}
	
	public void packetCaptured(NetEvent event) {
		//On refuse les demande entrantes si elles ne viennent pas du serveur
		DatasPacket packet = event.getPacket();
		if(packet != null) {
			if(packet.getType() != PacketType.START_CONNECT && packet.getType() != PacketType.PING) {
				NetDestination server = this.getSocketClient().getClient().getConnectedServer();
				if(server == null || server.getIp() != event.getFrom() && server.getPort() != event.getPort()) {
					event.refuse();
					this.getConsole().log("Packet entrant refusé ! (" + event.getFrom() + ":" + event.getPort() + ")");
				}
			}
		}
	}
}

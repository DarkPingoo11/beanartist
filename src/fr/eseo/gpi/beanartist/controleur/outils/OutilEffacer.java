package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;

import lib.jiconfont.icons.FontAwesome;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilEffacer extends Outil{

	private Image iconeCurseur;
	public OutilEffacer(PanneauDessin panneau) {
		super(panneau);
		IconFontSwing.register(FontAwesome.getIconFont());
		this.iconeCurseur = IconFontSwing.buildImage(FontAwesome.ERASER, 1024);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Point p = new Point(e.getX(), e.getY());
		VueForme formeToRem = null;
		for(VueForme vf : this.getPanneauDessin().getVueFormes()) {
			Forme f = vf.getForme();
			if(f.contient(p)) {
				formeToRem = vf;
				break;
			}
		}
		if(formeToRem != null) {
			DebugUtil.message(this.getClass(), "Retrait de la forme "+formeToRem.getForme().toString());
			this.retirerVueForme(formeToRem);
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		super.mouseMoved(e);
		boolean contient = false;
		for(VueForme vue : this.getPanneauDessin().getVueFormes()) {
			if(vue.getForme().contient(e.getX(), e.getY())) {
				contient = true;
				break;
			}
		}

		if(contient) {
			this.getPanneauDessin().setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
					this.iconeCurseur, new java.awt.Point(0, 25),"Curseur Effacer"));
		} else {
			this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}
	

}
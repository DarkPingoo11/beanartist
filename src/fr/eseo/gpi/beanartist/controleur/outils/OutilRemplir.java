package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;

import lib.jiconfont.icons.FontAwesome;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilRemplir extends Outil{

	private Image iconeCurseur;

	public OutilRemplir(PanneauDessin panneau) {
		super(panneau);
		IconFontSwing.register(FontAwesome.getIconFont());
		this.iconeCurseur = IconFontSwing.buildImage(FontAwesome.TINT, 80);
	}

	public Color getColor() {
		return this.getPanneauDessin().getCouleurRemplir();
	}

	public void updateColor() {
		this.iconeCurseur = IconFontSwing.buildImage(FontAwesome.TINT, 80, this.getPanneauDessin().getCouleurRemplir());
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		Point p = new Point(e.getX(), e.getY());
		for(VueForme vf : this.getPanneauDessin().getVueFormes()) {
			Forme f = vf.getForme();
			if(f.contient(p)) {
				f.remplir(e.getButton() == MouseEvent.BUTTON1 ? this.getColor() : null);
				if(this.getPanneauDessin().getOutilCourant() != null) {
					this.getPanneauDessin().getOutilCourant().notifyFormeModified(f);
				}
				this.getPanneauDessin().repaint();
				break;
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		super.mouseMoved(e);
		boolean contient = false;
		for(VueForme vue : this.getPanneauDessin().getVueFormes()) {
			if(vue.getForme().contient(e.getX(), e.getY())) {
				contient = true;
				break;
			}
		}

		if(contient) {
			this.getPanneauDessin().setCursor(Toolkit.getDefaultToolkit().createCustomCursor(
					this.iconeCurseur, new java.awt.Point(0, 25),"Curseur Remplir"));
		} else {
			this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}	

}
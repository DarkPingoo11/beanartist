package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueTrace;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilTrace extends OutilForme{

	private List<Point> mouvement;

	public OutilTrace(PanneauDessin panneau) {
		super(panneau);
		this.mouvement = new ArrayList<Point>();
	}

	private List<Point> getMouvement() {
		return this.mouvement;
	}

	protected VueForme creerVueForme() {
		Trace t = new Trace(this.getDebut());
		for(Point p : this.getMouvement()) {
			t.ajouterPoint(p);
		}
		t.setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		DebugUtil.message(this.getClass(), "Creation d'une forme "+t.toString());
		return new VueTrace(t);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.getMouvement().clear();
		super.mousePressed(e);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(!this.isCanceled()) {
			this.setFin(new Point(e.getX(), e.getY()));
			this.ajouterVueForme(this.creerVueForme());
		}
		this.setCancel(false);
		this.getPanneauDessin().repaint();
	}



	@Override
	public void mouseDragged(MouseEvent e) {
		Point p = new Point(e.getX(), e.getY());
		if(this.getMouvement().isEmpty() || MathUtil.distance(p, this.getMouvement().get(this.getMouvement().size()-1)) >= Outil.PRECISION) {
			this.getMouvement().add(p);
		}

		super.mouseDragged(e);
	}

}
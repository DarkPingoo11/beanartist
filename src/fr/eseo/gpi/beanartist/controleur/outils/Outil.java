package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.event.EventListenerList;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.controleur.client.listeners.ClientActionListener;
import fr.eseo.gpi.beanartist.controleur.client.listeners.FormeListener;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public abstract class Outil implements MouseListener, MouseMotionListener, KeyListener{

	public static final int PRECISION = 5;
	
	private EventListenerList listeners = new EventListenerList();
	private PanneauDessin panneauDessin;
	private Point debut, fin;
	
	public Outil(PanneauDessin panneau) {
		this.associer(panneau);
		this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
	}

	public void addClientActionListener(ClientActionListener listener) {
		this.listeners.add(ClientActionListener.class, listener);
	}
	
	public void remClientActionListener(ClientActionListener listener) {
		this.listeners.remove(ClientActionListener.class, listener);
	}
	
	public ClientActionListener[] getClientActionListeners() {
		return this.listeners.getListeners(ClientActionListener.class);
	}
	
	public void notifyFormeCreated(Forme f) {
		for(ClientActionListener a : this.getClientActionListeners()) {
			a.formeCreated(f);
		}
	}
	
	public void notifyFormeDeleted(Forme f) {
		for(ClientActionListener a : this.getClientActionListeners()) {
			a.formeDeleted(f);
		}
	}
	
	public void notifyFormeModified(Forme f) {
		for(ClientActionListener a : this.getClientActionListeners()) {
			a.formeModified(f);
		}
	}
	
	public PanneauDessin getPanneauDessin() {
		return this.panneauDessin;
	}

	public void setPanneauDessin(PanneauDessin panneau) {
		this.panneauDessin = panneau;
	}
	
	public void ajouterVueForme(VueForme vue) {
		this.getPanneauDessin().ajouterVueForme(vue);
		this.notifyFormeCreated(vue.getForme());
	}

	public void retirerVueForme(VueForme vf) {
		this.getPanneauDessin().retirerVueForme(vf);
		this.notifyFormeDeleted(vf.getForme());
	}
	
	public Point getDebut() {
		return this.debut;
	}

	public Point getFin() {
		return this.fin;
	}

	public void setDebut(Point p) {
		this.debut = p;
	}

	public void setFin(Point p) {
		this.fin = p;
	}

	public void associer(PanneauDessin panneau) {
		if(panneau != null) {
			if(panneau.getOutilCourant() != null) {
				panneau.getOutilCourant().liberer();
			}
			this.setPanneauDessin(panneau);
			this.addClientActionListener(new FormeListener());
			this.panneauDessin.setOutilCourant(this);
			this.panneauDessin.addMouseListener(this);
			this.panneauDessin.addMouseMotionListener(this);
			this.panneauDessin.addKeyListener(this);
			this.panneauDessin.setVueTemporaire(null);
			this.panneauDessin.repaint();
			DebugUtil.message(this.getClass(), "Choix de l'outil "+this.getClass().getSimpleName());
		}
	}

	private void liberer() {
		if(this.panneauDessin != null) {
			this.panneauDessin.setOutilCourant(null);
			this.panneauDessin.removeMouseListener(this);
			this.panneauDessin.removeMouseMotionListener(this);
			this.panneauDessin.removeKeyListener(this);
			this.setPanneauDessin(null);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.setDebut(new Point(e.getX(), e.getY()));
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.setFin(new Point(e.getX(), e.getY()));
		this.getPanneauDessin().setVueTemporaire(null);
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		this.setFin(new Point(e.getX(), e.getY()));
	}

	@Override
	public void mouseDragged(MouseEvent e) {}

	@Override
	public void mouseMoved(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {}

	@Override
	public void mouseExited(MouseEvent e) {}
	
	@Override
	public void keyTyped(KeyEvent e) {}
	
	@Override
	public void keyPressed(KeyEvent e) {}
	
	@Override
	public void keyReleased(KeyEvent e) {}
	
	

}

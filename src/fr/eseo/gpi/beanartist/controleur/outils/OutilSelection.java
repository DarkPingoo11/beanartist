package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.Cursor;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionColler;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionCopier;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionCouper;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionDelete;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionEdit;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.spec.Editable;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;
import fr.eseo.gpi.beanartist.vue.ui.menu.MenuRightClick;
import fr.eseo.gpi.beanartist.vue.ui.menu.MenuRightClick.Level;

public class OutilSelection extends Outil{

	private Forme formeSelectionnee;
	private Forme formeCopy;
	private boolean canMove;
	private boolean isPasting;
	private Point decalage;
	private MenuRightClick menuR;

	public OutilSelection(PanneauDessin panneau) {
		super(panneau);
		this.menuR = new MenuRightClick();
	}

	public void selectionnerForme(Forme f) {
		this.formeSelectionnee = f;
		this.canMove = false;
		this.setPasting(false);

		if(f != null) {
			//Redéfinir la couleur par rapport a celle de la figure selectionnee
			this.getPanneauDessin().setCouleurLigneCourante(f.getCouleurLigne());
			DebugUtil.message(this.getClass(), "Forme sélectionnée : "+f.toString());
		}
	}

	public void setCopy(Forme f) {
		this.formeCopy = (Forme) f.clone();
	}

	public Forme getFormeSelectionnee() {
		return this.formeSelectionnee;
	}

	public Forme getFormeCopy() {
		return this.formeCopy;
	}

	public boolean canMove() {
		return this.canMove;
	}

	private void setCanMove(boolean b) {
		this.canMove = b;
	}

	private Point getDecalage() {
		return this.decalage;
	}

	private void setDecalage(Point d) {
		this.decalage = d;
	}

	public MenuRightClick getMenuRightClick() {
		return this.menuR;
	}

	public boolean isPasting() {
		return this.isPasting;
	}

	public void setPasting(boolean b) {
		if(b) {
			if(this.getFormeCopy() == null) {
				this.isPasting = false;
			} else {
				this.isPasting = true;
				this.getPanneauDessin().setVueTemporaire(VueForme.getVueFormeCorrespondante(this.getFormeCopy()));
				this.getPanneauDessin().repaint();
			}
		} else {
			this.isPasting = false;
			this.getPanneauDessin().setVueTemporaire(null);
		}
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		//Action qui ouvre le menu sur le systeme actuel
		if(e.isPopupTrigger() || e.getButton() == MouseEvent.BUTTON3) {
			if(this.getFormeSelectionnee() != null && this.getFormeSelectionnee().contient(e.getX(), e.getY())) {
				if(this.getFormeSelectionnee() instanceof Editable) {
					this.getMenuRightClick().refreshMenu(Level.E3);
				} else {
					this.getMenuRightClick().refreshMenu(Level.E2);
				}
			} else {
				this.getMenuRightClick().refreshMenu(Level.E1);
			}
			this.getMenuRightClick().show(this.getPanneauDessin(), e.getX(), e.getY());
		}
		else if(this.isPasting() && this.getFormeCopy() != null) {
			VueForme vf = VueForme.getNewVueFormeCorrespondante(this.getFormeCopy());
			vf.getForme().setPosition(new Point(e.getX(), e.getY()));
			this.ajouterVueForme(vf);
			this.setPasting(false);
		} else {
			if(e.getClickCount() >= 2 && this.getFormeSelectionnee() != null) {
				new ActionEdit().fire();
				this.notifyFormeModified(this.getFormeSelectionnee());
			} else {
				Point p = new Point(e.getX(), e.getY());
				Forme select = null;
				for(VueForme vf : this.getPanneauDessin().getVueFormes()) {
					Forme f = vf.getForme();
					if(f.contient(p)) {
						select = f;
						this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
					}
				}
				this.selectionnerForme(select);	
			}
		}
		this.getPanneauDessin().repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
		//Autoriser le mouvement
		if(this.getFormeSelectionnee() != null) {
			Forme f = this.getFormeSelectionnee();
			Point click = new Point(e.getX(), e.getY());
			if(f.contient(click)) {
				this.setCanMove(true);
				this.setDecalage(new Point(click.getX() - f.getX(), click.getY() - f.getY()));
			}
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		//Fin du mouvement
		this.setCanMove(false);
		this.setDecalage(null);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		super.mouseDragged(e);
		if(this.getFormeSelectionnee() != null) {
			Forme f = this.getFormeSelectionnee();
			if(this.canMove()) {
				f.setPosition(new Point(e.getX() - this.getDecalage().getX(), e.getY() - this.getDecalage().getY()));
				this.notifyFormeModified(f);
				this.getPanneauDessin().repaint();
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		super.mouseMoved(e);
		if(this.getFormeSelectionnee() != null) {
			if(this.getFormeSelectionnee().contient(e.getX(), e.getY())) {
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			} else {
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			}
		} else {
			this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
		//Redessiner si on copie (pour voir la figure en mouvement) 
		if(this.getFormeCopy() != null) {
			Point pos = new Point(e.getX(), e.getY());
			this.getFormeCopy().setPosition(pos);
		}

		if(this.isPasting) {
			this.getPanneauDessin().repaint();
		}

	}

	@Override
	public void keyPressed(KeyEvent e) {
		//DELETE
		if(e.getKeyCode() == 127) {
			new ActionDelete().fire();
		}
		//CTRL +
		else if(e.isControlDown()) {
			if(e.getKeyCode() == 67) { //COPY
				new ActionCopier().fire();
			} else if(e.getKeyCode() == 88) { //CUT
				new ActionCouper().fire();
			} else if(e.getKeyCode() == 86) { //PASTE
				new ActionColler().fire();
			}
		}

		//ECHAP
		if (e.getKeyCode() == 27) {
			this.selectionnerForme(null);
			this.setCopy(null);
			this.setPasting(false);
			this.getPanneauDessin().repaint();
		}
	}

}
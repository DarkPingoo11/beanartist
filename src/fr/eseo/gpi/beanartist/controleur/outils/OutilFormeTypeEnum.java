package fr.eseo.gpi.beanartist.controleur.outils;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;

public enum OutilFormeTypeEnum {

	RECTANGLE("Rectangle", GoogleMaterialDesignIcons.CROP_16_9),
	ELLIPSE("Ellipse", GoogleMaterialDesignIcons.RADIO_BUTTON_UNCHECKED),
	CARRE("Carre", GoogleMaterialDesignIcons.CROP_DIN),
	CERCLE("Cercle", GoogleMaterialDesignIcons.LENS),
	LIGNE("Ligne", GoogleMaterialDesignIcons.LINEAR_SCALE),
	TRACE("Trace", GoogleMaterialDesignIcons.TIMELINE),
	TEXTE("Texte", GoogleMaterialDesignIcons.FORMAT_COLOR_TEXT);

	private String nom;
	private IconCode icone;
	OutilFormeTypeEnum(String nom) {
		this(nom, null);
	}
	OutilFormeTypeEnum(String nom, IconCode icone) {
		this.nom = nom;
		this.icone = icone;
	}
	
	public IconCode getIcone() {
		return this.icone;
	}

	public String getNom() {
		return this.nom;
	}

	public static OutilFormeTypeEnum getOutilType(String nom) {
		OutilFormeTypeEnum o = null;
		if(nom != null) {
			for(OutilFormeTypeEnum outil : OutilFormeTypeEnum.values()) {
				if(outil.getNom().equalsIgnoreCase(nom)) {
					o = outil;
					break;
				}
			}
		}
		return o;
	}
}

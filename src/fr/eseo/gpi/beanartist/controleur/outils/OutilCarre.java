package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.formes.VueCarre;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilCarre extends OutilForme{

	public OutilCarre(PanneauDessin panneau) {
		super(panneau);
	}

	protected VueForme creerVueForme() {
		VueCarre v = null;
		if(this.isQuickForme()) {
			v = new VueCarre(new Carre(this.getDebut()));
		} else {
			double x1 = this.getDebut().getX();
			double y1 = this.getDebut().getY();
			double x2 = this.getFin().getX();
			double y2 = this.getFin().getY();

			double cote = Math.max(Math.abs(x2-x1), Math.abs(y2-y1));
			if(x1 < x2) {
				if(y1 > y2) {
					v = new VueCarre(new Carre(new Point(x1, y1-cote), cote));
				} else {
					v = new VueCarre(new Carre(new Point(x1, y1), cote));
				}
			} else if(x1 > x2) {
				if(y1 > y2) {
					v = new VueCarre(new Carre(new Point(x1-cote, y1-cote), cote));
				} else {
					v = new VueCarre(new Carre(new Point(x1-cote, y1), cote));
				}
			}
		}
		
		if(v != null) {
			v.getForme().setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
			DebugUtil.message(this.getClass(), "Creation d'une forme "+v.getForme().toString());
		} 
		return v;
	}
}
package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.Cursor;
import java.awt.event.MouseEvent;

import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilRedimension extends OutilSelection{

	private enum Face {
		HAUT,
		BAS,
		GAUCHE,
		HAUT_GAUCHE,
		BAS_GAUCHE,
		DROITE,
		HAUT_DROITE,
		BAS_DROITE;
	}

	private boolean canResize = false;
	private Face face = null; 

	//Permet en plus de redimensionner
	public OutilRedimension(PanneauDessin panneau) {
		super(panneau);
	}

	public boolean canResize() {
		return this.canResize;
	}

	private void setCanResize(boolean b) {
		this.canResize = b;
	}

	private Face getFace() {
		return this.face;
	}

	private void setFace(Face d) {
		this.face = d;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(this.getFace() != null) {
			this.setCanResize(true);
		} else {
			this.setCanResize(false);
			super.mousePressed(e);
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		this.setFace(null);
		this.setCanResize(false);
		super.mouseReleased(e);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(this.getFormeSelectionnee() != null) {
			if(this.canResize()) {
				Forme f = this.getFormeSelectionnee();
				double dx, dy;
				Face face = this.getFace();
				if(face.equals(Face.BAS) || face.equals(Face.BAS_DROITE) || face.equals(Face.BAS_GAUCHE)){
					dy = e.getY() - f.getY();
					dy = dy < PRECISION ? PRECISION : dy;
					f.setHauteur(dy);
				}
				if(face.equals(Face.DROITE) || face.equals(Face.HAUT_DROITE) || face.equals(Face.BAS_DROITE)) {
					dx = e.getX() - f.getX();
					dx = dx < PRECISION ? PRECISION : dx;
					f.setLargeur(dx);
				}
				if(face.equals(Face.GAUCHE) || face.equals(Face.HAUT_GAUCHE) || face.equals(Face.BAS_GAUCHE)) {
					dx = f.getX() + f.getLargeur() - e.getX();
					dx = dx < PRECISION ? PRECISION : dx;
					f.setX(f.getLargeur() - dx + f.getX());
					f.setLargeur(dx);
				}
				if(this.getFace().equals(Face.HAUT) || face.equals(Face.HAUT_GAUCHE) || face.equals(Face.HAUT_DROITE)) {
					dy = f.getY() + f.getHauteur() - e.getY();
					dy = dy < PRECISION ? PRECISION : dy;
					f.setY(f.getHauteur() - dy + f.getY());
					f.setHauteur(dy);
				}
				this.notifyFormeModified(f);
				this.getPanneauDessin().repaint();
			} else {
				super.mouseDragged(e);
			}
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		this.setFace(null);
		if(this.getFormeSelectionnee() != null && (!(this.getFormeSelectionnee() instanceof Ligne))) {
			//TODO - Trouver une idée pour LIGNE
			Point p = new Point(e.getX(), e.getY());
			Point fp = this.getFormeSelectionnee().getPosition();
			double largeur = this.getFormeSelectionnee().getLargeur();
			double hauteur = this.getFormeSelectionnee().getHauteur();

			if(p.getY() >= fp.getY() && p.getY() <= fp.getY() + hauteur) {
				if(p.getX() >= fp.getX() - PRECISION && p.getX() <= fp.getX() + PRECISION) {
					this.setFace(Face.GAUCHE);
				} else if(p.getX() >= fp.getX() + largeur - PRECISION && p.getX() <= fp.getX() + largeur + PRECISION) {
					this.setFace(Face.DROITE);
				}
			}
			if(p.getX() >= fp.getX() && p.getX() <= fp.getX() + largeur) {
				if(p.getY() >= fp.getY() - PRECISION && p.getY() <= fp.getY() + PRECISION) {
					if(this.getFace() != null) {
						if(this.getFace().equals(Face.DROITE)) {
							this.setFace(Face.HAUT_DROITE);
						} else if(this.getFace().equals(Face.GAUCHE)) {
							this.setFace(Face.HAUT_GAUCHE);
						} else {
							this.setFace(Face.HAUT);
						}
					} else {
						this.setFace(Face.HAUT);
					}
				} else if(p.getY() >= fp.getY() + hauteur - PRECISION && p.getY() <= fp.getY() + hauteur + PRECISION) {
					if(this.getFace() != null) {
						if(this.getFace().equals(Face.DROITE)) {
							this.setFace(Face.BAS_DROITE);
						} else if(this.getFace().equals(Face.GAUCHE)) {
							this.setFace(Face.BAS_GAUCHE);
						} else {
							this.setFace(Face.BAS);
						}
					} else {
						this.setFace(Face.BAS);
					}
				}
			}
			this.changeCursor(this.getFace());
		}

		if(this.getFace() == null) {
			super.mouseMoved(e); //On autorise le mouvement sinon
		}
	}

	public void changeCursor(Face f) {
		if(f != null) {
			switch(f) {
			case BAS:
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
				break;
			case BAS_DROITE:
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
				break;
			case BAS_GAUCHE:
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
				break;
			case DROITE:
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
				break;
			case GAUCHE:
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
				break;
			case HAUT:
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
				break;
			case HAUT_DROITE:
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
				break;
			case HAUT_GAUCHE:
				this.getPanneauDessin().setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
				break;
			}
		}
	}
}
package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.Utils.UIUtil;
import fr.eseo.gpi.beanartist.modele.TexteDatas;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Texte;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueRectangle;
import fr.eseo.gpi.beanartist.vue.formes.VueTexte;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilTexte extends OutilForme{

	public OutilTexte(PanneauDessin panneau) {
		super(panneau);
	}
	
	protected VueForme creerVueFormeTemporaire() {
		double x1 = Math.min(this.getDebut().getX(),this.getFin().getX());
		double x2 = Math.max(this.getDebut().getX(),this.getFin().getX());
		double y1 = Math.min(this.getDebut().getY(),this.getFin().getY());
		double y2 = Math.max(this.getDebut().getY(),this.getFin().getY());
		VueForme v = new VueRectangle(new Rectangle(new Point(x1, y1), new Point(x2, y2)));
		v.getForme().setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		return v;
	}

	protected VueForme creerVueForme() {
		VueTexte v = null;
		if(this.isQuickForme()) {
			v = new VueTexte(new Texte(this.getDebut()));
		} else {
			double x1 = this.getDebut().getX();
			double y1 = this.getDebut().getY();
			double x2 = this.getFin().getX();
			double y2 = this.getFin().getY();

			if(x1 < x2) {
				if(y1 > y2) {
					v = new VueTexte(new Texte(new Point(x1, y2), new Point(x2, y1)));
				} else {
					v = new VueTexte(new Texte(new Point(x1, y1), new Point(x2, y2)));
				}
			} else if(x1 > x2) {
				if(y1 > y2) {
					v = new VueTexte(new Texte(new Point(x2, y2), new Point(x1, y1)));
				} else {
					v = new VueTexte(new Texte(new Point(x2, y1), new Point(x1, y2)));
				}
			}
		}
		if(v != null) {
			v.getForme().setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
			TexteDatas td = UIUtil.showTexteInputDialog("Ajout de texte");
			if(td == null) {
				v = null;
			} else {
				Texte txt = ((Texte)v.getForme());
				txt.setTexte(td.getTexte());
				txt.setFont(td.getFont());
				DebugUtil.message(this.getClass(), "Creation d'une zone de texte "+v.getForme().toString());
			}
		} 
		return v;
	}
}
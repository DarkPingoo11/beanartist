package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.formes.VueEllipse;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilEllipse extends OutilForme{

	public OutilEllipse(PanneauDessin panneau) {
		super(panneau);
	}

	protected VueForme creerVueForme() {
		VueEllipse v = null;
		if(this.isQuickForme()) {
			v = new VueEllipse(new Ellipse(this.getDebut()));
		} else {
			double x1 = this.getDebut().getX();
			double y1 = this.getDebut().getY();
			double x2 = this.getFin().getX();
			double y2 = this.getFin().getY();

			if(x1 < x2) {
				if(y1 > y2) {
					v = new VueEllipse(new Ellipse(new Point(x1, y2), new Point(x2, y1)));
				} else {
					v = new VueEllipse(new Ellipse(new Point(x1, y1), new Point(x2, y2)));
				}
			} else if(x1 > x2) {
				if(y1 > y2) {
					v = new VueEllipse(new Ellipse(new Point(x2, y2), new Point(x1, y1)));
				} else {
					v = new VueEllipse(new Ellipse(new Point(x2, y1), new Point(x1, y2)));
				}
			}
		}

		if(v != null) {
			v.getForme().setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
			DebugUtil.message(this.getClass(), "Creation d'une forme "+v.getForme().toString());
		} 
		return v;
	}
}
package fr.eseo.gpi.beanartist.controleur.outils;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public abstract class OutilForme extends Outil{

	public static final double MIN_DISTANCE = 5;

	private boolean cancelled = false;

	public OutilForme(PanneauDessin panneau) {
		super(panneau);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		super.mouseDragged(e);
		if(!this.isCanceled()) {
			Point tmp = this.getFin();
			this.setFin(new Point(e.getX(), e.getY()));
			this.getPanneauDessin().setVueTemporaire(this.creerVueFormeTemporaire());
			this.setFin(tmp);
			this.getPanneauDessin().repaint();
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		super.mouseReleased(e);
		if(MathUtil.distance(this.getDebut(), this.getFin()) > MIN_DISTANCE && !this.isCanceled()) {
			VueForme vf = this.creerVueForme();
			if(vf != null) {
				this.ajouterVueForme(vf);
			}
		}
		this.setCancel(false);
		this.getPanneauDessin().repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		super.mouseClicked(e);
		if(e.getClickCount() == 2) {
			this.setDebut(this.getFin());
			VueForme vf = this.creerVueForme();
			if(vf != null) {
				this.ajouterVueForme(vf);
			}
			this.getPanneauDessin().repaint();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		//DELETE ou ECHAP -> Annulation forme en cours
		if(e.getKeyCode() == 127 || e.getKeyCode() == 27) {
			this.setCancel(true);
		}
	}

	protected abstract VueForme creerVueForme();

	protected VueForme creerVueFormeTemporaire() {
		return this.creerVueForme();
	}

	public boolean isQuickForme() {
		return this.getDebut() == this.getFin();
	}
	
	protected boolean isCanceled() {
		return this.cancelled;
	}
	
	protected void setCancel(boolean b) {
		this.cancelled = b;
		if(b) {
			this.getPanneauDessin().setVueTemporaire(null);
			this.getPanneauDessin().repaint();
		}
	}
}
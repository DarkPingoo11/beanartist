package fr.eseo.gpi.beanartist.controleur.outils;

import fr.eseo.gpi.beanartist.Utils.DebugUtil;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;
import fr.eseo.gpi.beanartist.vue.formes.VueLigne;
import fr.eseo.gpi.beanartist.vue.ui.PanneauDessin;

public class OutilLigne extends OutilForme{

	public OutilLigne(PanneauDessin panneau) {
		super(panneau);
	}

	protected VueForme creerVueForme() {
		VueLigne v = null;
		if(this.isQuickForme()) {
			v = new VueLigne(new Ligne(this.getDebut()));
		} else {
			v = new VueLigne(new Ligne(this.getDebut(), this.getFin()));
		}
		v.getForme().setCouleurLigne(this.getPanneauDessin().getCouleurLigneCourante());
		DebugUtil.message(this.getClass(), "Creation d'une forme "+v.getForme().toString());
		return v;
	}
}
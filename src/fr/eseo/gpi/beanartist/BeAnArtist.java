package fr.eseo.gpi.beanartist;

import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;
/**
 * 
 * @author Tristan LE GACQUE
 *
 */
public class BeAnArtist {

	public static void main(String[] args) {
		System.out.println("Initialization...");
		FenetreBeAnArtist fen = FenetreBeAnArtist.getInstance();
		fen.getPanneauDessin().setOutilCourant(new OutilSelection(fen.getPanneauDessin()));
		fen.afficher();

		System.out.println("Initialization...Done !");
	}

}

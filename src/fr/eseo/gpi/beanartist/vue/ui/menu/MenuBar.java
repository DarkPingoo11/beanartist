package fr.eseo.gpi.beanartist.vue.ui.menu;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEnregistrer;
import fr.eseo.gpi.beanartist.controleur.actions.ActionOuvrir;
import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionAfficherConsoleClient;
import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionAfficherConsoleServeur;
import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionChangerMenuServeur;
import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionConnecter;
import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionDeconnecter;
import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionDemarrerServeur;
import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionStopperServeur;
import fr.eseo.gpi.beanartist.controleur.client.NetDestination;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.vue.ui.components.ButtonRuler;
import fr.eseo.gpi.beanartist.vue.ui.components.JLabelIcon;

public class MenuBar extends JMenuBar{

	private static final long serialVersionUID = 1L;
	private JMenu menuF, menuNet;
	private JMenuItem connecter, deconnecter, demarrer, stopper, afficherConsoleC, afficherConsoleS, notification;
	private JLabel connectStatusOn, connectStatusOff, statusIconOn, statusIconOff;
	
	public MenuBar() {
		super();
		
		this.initComponents();
		this.updateMenu();
	}
	
	private void initComponents() {
		//SEPARATEUR
		JMenuItem separateur = new JMenuItem(" ");
		separateur.setEnabled(false);
		
		//Menu IO
		this.menuF = new JMenu("Fichier");
		JMenuItem ouvrir = new JMenuItem(new ActionOuvrir());
		JMenuItem enregistrer = new JMenuItem(new ActionEnregistrer());
		this.menuF.add(ouvrir);
		this.menuF.add(enregistrer);
		
		//Menu Reseau
		this.menuNet = new JMenu("Serveur");
		this.demarrer = new JMenuItem(new ActionDemarrerServeur());
		this.stopper = new JMenuItem(new ActionStopperServeur());
		this.afficherConsoleC = new JMenuItem(new ActionAfficherConsoleClient());
		this.afficherConsoleS = new JMenuItem(new ActionAfficherConsoleServeur());
		this.connecter = new JMenuItem(new ActionConnecter());
		this.deconnecter = new JMenuItem(new ActionDeconnecter());
		
		//Visuel status
		this.connectStatusOn = new JLabel("Connecté");
		this.connectStatusOff = new JLabel("Non connecté");
		this.statusIconOn = new JLabelIcon(GoogleMaterialDesignIcons.SIGNAL_WIFI_4_BAR, 20, 40);
		this.statusIconOff = new JLabelIcon(GoogleMaterialDesignIcons.SIGNAL_WIFI_OFF, 20, 40);
		this.notification = new JMenuItem(new ActionAfficherConsoleClient());
		
		//---Parametrage
		this.notification.setText("");
		this.notification.setIcon(IconFontSwing.buildIcon(GoogleMaterialDesignIcons.MESSAGE, 16));
		this.notification.setBackground(new Color(255, 255, 0, 200));
		this.notification.setPreferredSize(new Dimension(28, 25));
		this.notification.setMaximumSize(new Dimension(28, 25));
		
		//-----Mise en place
		this.menuNet.add(this.demarrer);
		this.menuNet.add(this.stopper);
		this.menuNet.add(this.afficherConsoleS);
		this.menuNet.add(separateur);
		this.menuNet.add(this.connecter);
		this.menuNet.add(this.deconnecter);
		this.menuNet.add(this.afficherConsoleC);
		
		//-----Ruler
		ButtonRuler br = new ButtonRuler();
		br.add(this.demarrer);
		br.add(this.stopper);
		br.add(this.afficherConsoleS);
		br.add(this.connecter);
		br.add(this.deconnecter);
		br.add(this.afficherConsoleC);
		br.reorganizeIconsGap();
		
		this.add(this.menuF);
		this.add(this.menuNet);
		this.add(Box.createRigidArea(new Dimension(5, 1)));
		this.add(this.notification);
		this.add(Box.createHorizontalGlue());
		this.add(this.connectStatusOn);
		this.add(this.statusIconOn);
		this.add(this.connectStatusOff);
		this.add(this.statusIconOff);
		this.add(Box.createRigidArea(new Dimension(5, 1)));
		this.menuNet.addMenuListener(new ActionChangerMenuServeur());
	}
	
	public void updateMenu() {
		NetworkManager nm = NetworkManager.getInstance();
		if(nm.getHostedServeur() == null) {
			if(!nm.isClientConnected()) {
				this.demarrer.setVisible(true);
			} else {
				this.demarrer.setVisible(false);
			}
			this.stopper.setVisible(false);
			this.afficherConsoleS.setVisible(false);
		} else {
			this.demarrer.setVisible(false);
			this.stopper.setVisible(true);
			this.afficherConsoleS.setVisible(true);
		}
		
		if(nm.isClientConnected()) {
			NetDestination net = nm.getClient().getConnectedServer();
			this.connectStatusOn.setText("Connecté (" + net.getIp() + ":" + net.getPort() + ")");
			this.connectStatusOn.setVisible(true);
			this.statusIconOn.setVisible(true);
			this.connectStatusOff.setVisible(false);
			this.statusIconOff.setVisible(false);
			this.connecter.setVisible(false);
			this.deconnecter.setVisible(true);
			this.setBackground(Color.GREEN);
		} else {
			this.connectStatusOn.setVisible(false);
			this.statusIconOn.setVisible(false);
			this.connectStatusOff.setVisible(true);
			this.statusIconOff.setVisible(true);
			this.connecter.setVisible(true);
			this.deconnecter.setVisible(false);
			this.setBackground(Color.LIGHT_GRAY);
		}
		
		if(!nm.getClient().getConsole().isUpToDate()) {
			this.notification.setVisible(true);
		} else {
			this.notification.setVisible(false);
		}
	}

}

package fr.eseo.gpi.beanartist.vue.ui.menu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionColler;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionCopier;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionCouper;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionDelete;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionEdit;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionEdition;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionInfo;
import fr.eseo.gpi.beanartist.controleur.actions.edition.ActionPropriete;

public class MenuRightClick extends JPopupMenu{

	private static final long serialVersionUID = 1L;

	public enum Level{
		E1(1), E2(2), E3(3);
		
		private int id;
		Level(int i) {
			this.id = i;
		}
		
		public int getId() {
			return this.id;
		}
	}
	
	
	//1 - PASTE
	//2 - CUT - COPY - DEL - PROPERTIES - INFO
	//3 - EDIT
	private HashMap<Integer, List<JMenuItem>> items;

	public MenuRightClick() {
		super();
		this.items = new HashMap<Integer, List<JMenuItem>>();
		this.initComponents();
		this.refreshMenu(1);
	}

	protected void initComponents() {
		JMenuItem space = new JMenuItem(" ");
		space.setEnabled(false);
		this.addItemEdition(new ActionInfo(), 2);
		this.addItemEdition(new ActionEdit(), 3);
		this.addItemEdition(new ActionDelete(), 2);
		this.addItemEdition(space, 2);
		this.addItemEdition(new ActionCouper(), 2);
		this.addItemEdition(new ActionCopier(), 2);
		this.addItemEdition(new ActionColler(), 1);	
		this.addItemEdition(space, 2);
		this.addItemEdition(new ActionPropriete(), 2);
	}

	protected void addItemEdition(ActionEdition a, int id) {
		JMenuItem item = new JMenuItem(a);
		item.setIcon(a.getIcone());
		this.addItemEdition(item, id);
	}
	
	protected void addItemEdition(JMenuItem item, int id) {
		this.add(item);
		List<JMenuItem> list = this.items.get(id);
		if(list == null) {
			list = new ArrayList<JMenuItem>();
			this.items.put(id, list);
		}
		list.add(item);
	}

	public void refreshMenu(Level l) {
		this.refreshMenu(l.getId());
	}
	
	public void refreshMenu(int id) {
		//On cache tout
		for(int ids : this.items.keySet()) {
			for(JMenuItem item : this.items.get(ids)) {
				item.setVisible(ids <= id);
				//System.out.println(item.getActionCommand() +" = "+(ids <= id));
			}
		}
	}
}

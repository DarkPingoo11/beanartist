package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import lib.jiconfont.IconCode;
import lib.jiconfont.icons.FontAwesome;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.Utils.ColorUtil;
import fr.eseo.gpi.beanartist.controleur.actions.ActionChoisirCouleurLigne;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEffacer;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEffacerForme;
import fr.eseo.gpi.beanartist.controleur.actions.ActionEnregistrer;
import fr.eseo.gpi.beanartist.controleur.actions.ActionForme;
import fr.eseo.gpi.beanartist.controleur.actions.ActionOuvrir;
import fr.eseo.gpi.beanartist.controleur.actions.ActionRedimensionner;
import fr.eseo.gpi.beanartist.controleur.actions.ActionRemplir;
import fr.eseo.gpi.beanartist.controleur.options.OptionCheck;
import fr.eseo.gpi.beanartist.controleur.options.OptionTypeEnum;
import fr.eseo.gpi.beanartist.controleur.outils.OutilFormeTypeEnum;
import fr.eseo.gpi.beanartist.vue.ui.components.ButtonRuler;

public class PanneauBarreOutils extends JPanel{

	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT = 150;
	public static final Color COULEUR_FOND_PAR_DEFAUT = Color.LIGHT_GRAY;
	public static final int SEPARATION_PAR_DEFAUT = 20;
	public static final int HAUTEUR_BOUTON_PAR_DEFAUT = 30;

	private JButton couleurB;
	private JToggleButton remplirB;
	private List<OptionCheck> options;

	public PanneauBarreOutils() {
		super();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, PanneauDessin.HAUTEUR_PAR_DEFAUT));
		this.setBackground(COULEUR_FOND_PAR_DEFAUT);
		this.options = new ArrayList<OptionCheck>();

		initOptions();
		initComponents();
		this.setOptionValue(OptionTypeEnum.IGNORER_CONFIRMATIONS, true);
		this.setOptionValue(OptionTypeEnum.ANTI_ALIASHING, false);
	}

	private List<OptionCheck> getOptions() {
		return this.options;
	}

	public boolean getOptionValue(OptionTypeEnum option) {
		for(OptionCheck o : this.getOptions()) {
			if(o.getOptionType().equals(option)) {
				return o.getStatus();
			}
		}
		throw new IllegalArgumentException("L'option voulue n'existe pas");
	}

	public void setOptionValue(OptionTypeEnum option, boolean value) {
		for(OptionCheck o : this.getOptions()) {
			if(o.getOptionType() == option) {
				o.setStatus(value);
				break;
			}
		}
	}
	
	private void initOptions() {
		for(OptionTypeEnum optionType : OptionTypeEnum.values()) {
			OptionCheck oc = new OptionCheck(optionType);
			oc.setStatus(false);
			this.options.add(oc);
		}
	}


	private void initComponents() {
		ButtonGroup groupe = new ButtonGroup();
		ButtonRuler ruler1 = new ButtonRuler();
		ButtonRuler ruler2 = new ButtonRuler();

		ruler1.add(this.addButton(new JButton(new ActionEffacer()), this, GoogleMaterialDesignIcons.CLEAR));
		groupe.add(ruler1.add(this.addButton(new JToggleButton(new ActionRedimensionner()), this, FontAwesome.MOUSE_POINTER, 5)));
		groupe.add(ruler1.add(this.addButton(new JToggleButton(new ActionEffacerForme()), this, FontAwesome.ERASER)));
		this.remplirB = (JToggleButton)ruler1.add(this.addButton(new JToggleButton(new ActionRemplir()), this, FontAwesome.TINT));
		groupe.add(this.remplirB);
		this.add(Box.createRigidArea(new Dimension(this.getWidth(), SEPARATION_PAR_DEFAUT))); //SPACE
		this.add(Box.createRigidArea(new Dimension(this.getWidth(), SEPARATION_PAR_DEFAUT))); //SPACE
		//Ajout des outils
		for(OutilFormeTypeEnum type : OutilFormeTypeEnum.values()) {
			groupe.add(ruler2.add(this.addButton(new JToggleButton(new ActionForme(type)), this, type.getIcone())));
		}

		this.add(Box.createVerticalGlue()); //Glue pour separer verticalement les elements

		//Ces boutons ne seront pas visible, car je ne vuex pas les avoir sur la barre d'outil
		//Je les ajoutes uniquement pour valider l'assignement center.
		this.addButton(new JButton(new ActionEnregistrer()), this).setVisible(false);
		this.addButton(new JButton(new ActionOuvrir()), this).setVisible(false);
		//Fin des boutons inutiles
			
		//Ajout des options
		for(OptionCheck option : this.getOptions()) {
			//Ajout des optionCheck (un JCheckBox est forcement un AbstractButton : Heritage)
			this.addButton((AbstractButton)option.getComposantAssocie(), this, option.getIconeDisabled());
		}

		this.couleurB = new JButton(new ActionChoisirCouleurLigne());
		this.addButton(this.couleurB, this);

		ruler1.reorganizeIconsGap();
		ruler2.reorganizeIconsGap();
		
	}


	private AbstractButton addButton(AbstractButton button, JPanel addTo) {
		return this.addButton(button, addTo, null, 4);
	}

	private AbstractButton addButton(AbstractButton button, JPanel addTo, IconCode icone) {
		return this.addButton(button, addTo, icone, 4);
	}

	private AbstractButton addButton(AbstractButton button, JPanel addTo, IconCode icone, int spaceWithIco) {
		button.setMaximumSize(new Dimension(LARGEUR_PAR_DEFAUT, HAUTEUR_BOUTON_PAR_DEFAUT));
		button.setMinimumSize(new Dimension(LARGEUR_PAR_DEFAUT, HAUTEUR_BOUTON_PAR_DEFAUT));
		button.setHorizontalAlignment(SwingConstants.LEFT);
		if(icone != null) {
			Icon icon = IconFontSwing.buildIcon(icone, 13);
			button.setIcon(icon);
			button.setIconTextGap(spaceWithIco);
		}
		addTo.add(button);	
		return button;
	}

	protected void repaintCouleurBoxCouleur() {
		Color c = this.getFenetreBeAnArtist().getPanneauDessin().getCouleurLigneCourante();
		this.couleurB.setBackground(new Color(c.getRed(), c.getGreen(), c.getBlue()));
		this.couleurB.setForeground(ColorUtil.getCouleurComplementaire(c));
		this.couleurB.repaint();
	}
	
	protected void repaintCouleurBoxRemplir() {
		Color c = this.getFenetreBeAnArtist().getPanneauDessin().getCouleurRemplir();
		this.remplirB.setBackground(new Color(c.getRed(), c.getGreen(), c.getBlue()));
		this.remplirB.repaint();
	}

	public FenetreBeAnArtist getFenetreBeAnArtist() {
		return FenetreBeAnArtist.getInstance();
	}



}

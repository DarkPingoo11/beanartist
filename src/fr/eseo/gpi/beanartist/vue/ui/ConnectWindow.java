package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;

public class ConnectWindow extends JWindow{

	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT = 240;
	public static final int HAUTEUR_PAR_DEFAUT = 100;

	public ConnectWindow() {
		this.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT));
		this.getContentPane().setLayout(new BoxLayout(this.getContentPane(), BoxLayout.PAGE_AXIS));
		this.initComponents();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setAlwaysOnTop(true);
	}

	private void initComponents() {
		JPanel barPan, textPan;
		barPan = new JPanel();
		textPan = new JPanel();

		textPan.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, 100));
		barPan.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, 200));
		JProgressBar bar = new JProgressBar();
		bar.setIndeterminate(true);
		JLabel text = new JLabel("Connexion en cours...");
		text.setFont(new Font("Courrier", Font.BOLD, 16));

		textPan.add(text);
		barPan.add(bar);

		this.add(new JLabel(" "));
		this.add(textPan);
		this.add(barPan);
	}

	@Override
	public void setVisible(boolean b) {
		FenetreBeAnArtist.getInstance().setEnabled(!b);
		if(!b) {
			PanneauDessin pd = FenetreBeAnArtist.getInstance().getPanneauDessin();
			pd.setFocusable(true);
			pd.requestFocus();
			pd.requestFocusInWindow();
		} else {
			//Auto stop au bout de 10s
			new Thread(new Runnable() {
				public void run() {
					try {
						Thread.sleep(10000L);
					} catch (InterruptedException e) {}
					
					if(isVisible()) {
						setVisible(false);
					}
				}
			}).start();
		}
		super.setVisible(b);
	}

}

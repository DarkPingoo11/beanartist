package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import lib.jiconfont.icons.FontAwesome;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.vue.ui.components.JComboBoxFont;
import fr.eseo.gpi.beanartist.vue.ui.menu.MenuBar;

public class FenetreBeAnArtist extends JFrame{

	private static final long serialVersionUID = 1L;
	public static final String TITRE_PAR_DEFAUT = "Fenêtre BeAnArtist";
	
	private static FenetreBeAnArtist instance;
	
	private PanneauDessin panneauDessin;
	private PanneauBarreOutils panneauBarreOutils;
	private MenuBar menu;
	
	
	private FenetreBeAnArtist() {
		super();
		this.initRequirements();
		this.panneauDessin = new PanneauDessin();
		this.panneauBarreOutils = new PanneauBarreOutils();
		
		//Définition des parametres
		this.setTitle(TITRE_PAR_DEFAUT);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Ajout du PanneauDessin
		this.getContentPane().add(this.panneauDessin, BorderLayout.WEST);
		this.getContentPane().add(this.panneauBarreOutils, BorderLayout.EAST);
		this.setResizable(false);
		
		//Ajout du menu
		this.menu = new MenuBar();
		this.add(this.menu, BorderLayout.NORTH);
		this.pack();
		this.setLocationRelativeTo(null);
		
		this.initDelayedComponents();
	}
	
	public PanneauDessin getPanneauDessin() {
		return this.panneauDessin;
	}
	
	public PanneauBarreOutils getPanneauBarreOutils() {
		return this.panneauBarreOutils;
	}
	
	public MenuBar getMenu() {
		return this.menu;
	}
	
	public void afficher() {
		this.setVisible(true);
	}
	
	public void cacher() {
		this.setVisible(false);
	}
	
	private void initRequirements() {
		IconFontSwing.register(FontAwesome.getIconFont()); //Autoriser les icones
		IconFontSwing.register(GoogleMaterialDesignIcons.getIconFont()); //Autoriser les icones
	}
	
	private void initDelayedComponents() {
		Thread t = new Thread(new Runnable(){
			public void run() {
				JComboBoxFont.initFonts();	
			}
		});
		
		t.start();
	}

	//Methodes statiques
	public static FenetreBeAnArtist getInstance() {
		if(instance == null) {
			instance = new FenetreBeAnArtist();
			instance.getPanneauBarreOutils().repaintCouleurBoxCouleur();
		}
		return instance;
	}
	

}

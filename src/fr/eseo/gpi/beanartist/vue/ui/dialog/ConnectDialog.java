package fr.eseo.gpi.beanartist.vue.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import fr.eseo.gpi.beanartist.controleur.actions.dialog.ActionAnnuler;
import fr.eseo.gpi.beanartist.controleur.actions.dialog.ActionValider;
import fr.eseo.gpi.beanartist.modele.formes.Forme;

public class ConnectDialog extends EditDialog{

	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT = 300;
	public static final int HAUTEUR_PAR_DEFAUT = 235;

	private ConnectPanel connectPanel;
	
	private ConnectDialog(String titre) {
		super();
		this.setTitle(titre);
		this.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT));
		this.setResizable(false);
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.pack();
		this.setLocationRelativeTo(null);

		//Valeurs par defaut
		this.setModal(true);
	}

	private void initComponents() {
		//Définition des différents panels
		this.connectPanel = new ConnectPanel(LARGEUR_PAR_DEFAUT);
		JPanel buttonPan = new JPanel();
		
		//-----Dimensions
		buttonPan.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, 35));
		buttonPan.setBackground(Color.LIGHT_GRAY);

		//Définition des boutons
		JButton okButton = new JButton(new ActionValider(this));
		JButton cancelButton = new JButton(new ActionAnnuler(this));

		//Ajout des boutons au panel
		buttonPan.add(okButton);
		buttonPan.add(cancelButton);

		//Tout ajouter au Dialog
		this.getContentPane().add(this.connectPanel);
		this.getContentPane().add(buttonPan, BorderLayout.SOUTH);
	}

	public Forme getForme() {
		return this.getForme();
	}
	
	public ConnectPanel getConnectPanel() {
		return this.connectPanel;
	}
	
	public ConnectDatas showDialog() {
		this.setVisible(true);

		if(this.isSendData()) {
			String ip = this.getConnectPanel().getValueIP();
			int port = this.getConnectPanel().getValuePort();
			String name = this.getConnectPanel().getValueName();
			
			return new ConnectDatas(ip, port, name);
		}
		
		return null;
	}

	public static ConnectDatas showConnectDialog(String titre) {
		ConnectDialog t = new ConnectDialog(titre);
		return t.showDialog();
	}


}

package fr.eseo.gpi.beanartist.vue.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import fr.eseo.gpi.beanartist.controleur.actions.dialog.ActionAnnuler;
import fr.eseo.gpi.beanartist.controleur.actions.dialog.ActionValider;
import fr.eseo.gpi.beanartist.modele.formes.Forme;

public class ProprieteDialog extends EditDialog{

	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT = 300;
	public static final int HAUTEUR_PAR_DEFAUT = 230;

	private ProprietePanel propPanel;
	private Forme forme;
	
	private ProprieteDialog(String titre, Forme f) {
		super();
		this.forme = f;
		this.setTitle(titre);
		this.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT));
		this.setResizable(false);
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.pack();
		this.setLocationRelativeTo(null);

		//Valeurs par defaut
		this.setModal(true);
	}

	private void initComponents() {
		//Définition des différents panels
		this.propPanel = new ProprietePanel(LARGEUR_PAR_DEFAUT, this.forme);
		JPanel buttonPan = new JPanel();
		
		//-----Dimensions
		buttonPan.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, 35));
		buttonPan.setBackground(Color.LIGHT_GRAY);

		//Définition des boutons
		JButton okButton = new JButton(new ActionValider(this));
		JButton cancelButton = new JButton(new ActionAnnuler(this));

		//Ajout des boutons au panel
		buttonPan.add(okButton);
		buttonPan.add(cancelButton);

		//Tout ajouter au Dialog
		this.getContentPane().add(this.propPanel);
		this.getContentPane().add(buttonPan, BorderLayout.SOUTH);
	}

	public Forme getForme() {
		return this.getForme();
	}
	
	public ProprietePanel getProprietePanel() {
		return this.propPanel;
	}
	
	public ProprieteDatas showDialog() {
		this.setVisible(true);
		if(this.isSendData()) {
			int x = this.getProprietePanel().getValueX();
			int y = this.getProprietePanel().getValueY();
			int largeur = this.getProprietePanel().getValueLargeur();
			int hauteur = this.getProprietePanel().getValueHauteur();
			int epaisseur = this.getProprietePanel().getValueEpaisseur();
			Color ligne = this.getProprietePanel().getValueCouleurLigne();
			Color fond = this.getProprietePanel().getValueCouleurFond();
			
			return new ProprieteDatas(x, y, largeur, hauteur, epaisseur, ligne, fond);
		}
		
		return null;
		
	}

	public static ProprieteDatas showProprieteDialog(String titre, Forme f) {
		ProprieteDialog t = new ProprieteDialog(titre, f);
		return t.showDialog();
	}


}

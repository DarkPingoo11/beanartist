package fr.eseo.gpi.beanartist.vue.ui.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.Utils.ColorUtil;
import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.controleur.actions.dialog.propriete.ActionChangerCouleur;
import fr.eseo.gpi.beanartist.controleur.actions.dialog.propriete.ActionChangerCouleur.ComponentType;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.vue.ui.components.JLabelIcon;

public class ProprietePanel extends JPanel{

	private static final long serialVersionUID = 1L;
	private JTextField posXField, posYField, largeurField, hauteurField, epaisseurField;
	private JButton couleurFondB, couleurLigneB;
	private int largeur;
	private Color cFond, cLigne;
	private Forme forme;

	protected ProprietePanel(int largeur, Forme f) {
		this.largeur = largeur;
		this.forme = f;

		this.initComponents();
		this.setDefaultValues();
	}

	private void initComponents() {
		//Déclaration des panneaux intermédiaires
		JPanel positionPan = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel dimensionsPan = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel epaisseurPan = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel couleursPan = new JPanel(new FlowLayout(FlowLayout.LEFT));

		//-----Dimensions
		positionPan.setPreferredSize(new Dimension(this.largeur, 35));
		dimensionsPan.setPreferredSize(new Dimension(this.largeur, 35));
		epaisseurPan.setPreferredSize(new Dimension(this.largeur, 35));
		couleursPan.setPreferredSize(new Dimension(this.largeur, 35));

		//Déclaration des composants de formulaire
		//-----Déclaration des composants
		this.posXField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		this.posYField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		this.largeurField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		this.hauteurField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		this.epaisseurField = new JFormattedTextField(NumberFormat.getIntegerInstance());
		this.couleurFondB = new JButton(new ActionChangerCouleur(this, ComponentType.FOND));
		this.couleurLigneB = new JButton(new ActionChangerCouleur(this, ComponentType.LIGNE));

		//-----Dimensions
		this.posXField.setPreferredSize(new Dimension(50, 25));
		this.posYField.setPreferredSize(new Dimension(50, 25));
		this.largeurField.setPreferredSize(new Dimension(50, 25));
		this.hauteurField.setPreferredSize(new Dimension(50, 25));
		this.epaisseurField.setPreferredSize(new Dimension(25, 25));
		this.couleurFondB.setPreferredSize(new Dimension(75, 25));
		this.couleurLigneB.setPreferredSize(new Dimension(75, 25));
		
		//-----Tooltip
		this.couleurFondB.setToolTipText("Défini la couleur de fond (CTRL + Clic pour réinitialiser)");

		//Déclaration des Label, pour un affichage fancy
		//-----Icones
		JLabelIcon iconePosition = new JLabelIcon(FontAwesome.ANCHOR, 20, 30);
		JLabelIcon iconeDimensions = new JLabelIcon(FontAwesome.ARROWS, 20, 30);
		JLabelIcon iconeEpaisseur = new JLabelIcon(FontAwesome.COMPRESS, 20, 30);
		JLabelIcon iconeCouleurs = new JLabelIcon(FontAwesome.EYEDROPPER, 20, 30);

		//-----Textes
		JLabel texteX = new JLabel("x");
		JLabel texteV = new JLabel(",");
		JLabel texteLxH = new JLabel(" (L x H)");
		JLabel texteXY = new JLabel(" (X , Y)");
		JLabel textePos = new JLabel("Position :");
		JLabel texteDim = new JLabel("Dimensions :");
		JLabel texteEpa = new JLabel("Epaisseur : ");
		JLabel texteCou = new JLabel("Couleurs : ");
		JLabel texteSpace = new JLabel(" ");

		//-----Dimensions
		textePos.setPreferredSize(new Dimension(75, 25));
		texteDim.setPreferredSize(new Dimension(75, 25));
		texteEpa.setPreferredSize(new Dimension(75, 25));
		texteCou.setPreferredSize(new Dimension(75, 25));
		texteX.setPreferredSize(new Dimension(10, 25));
		texteV.setPreferredSize(new Dimension(10, 25));
		texteSpace.setPreferredSize(new Dimension(5, 25));

		//-----Alignement
		texteX.setHorizontalAlignment(JTextField.CENTER);
		texteV.setHorizontalAlignment(JTextField.CENTER);

		//Placement de tous les composants
		//-----Partie Position
		positionPan.add(iconePosition);
		positionPan.add(textePos);
		positionPan.add(this.posXField);
		positionPan.add(texteV);
		positionPan.add(this.posYField);
		positionPan.add(texteXY);

		//-----Partie Dimensions
		dimensionsPan.add(iconeDimensions);
		dimensionsPan.add(texteDim);
		dimensionsPan.add(this.largeurField);
		dimensionsPan.add(texteX);
		dimensionsPan.add(this.hauteurField);
		dimensionsPan.add(texteLxH);

		//-----Partie Epaisseur
		epaisseurPan.add(iconeEpaisseur);
		epaisseurPan.add(texteEpa);
		epaisseurPan.add(this.epaisseurField);

		//-----Partie Couleurs
		couleursPan.add(iconeCouleurs);
		couleursPan.add(texteCou);
		couleursPan.add(this.couleurLigneB);
		couleursPan.add(texteSpace);
		couleursPan.add(this.couleurFondB);

		//Ajout de tout au panel
		this.add(positionPan);
		this.add(dimensionsPan);
		this.add(epaisseurPan);
		this.add(couleursPan);
	}

	private void setDefaultValues() {
		this.posXField.setText(""+MathUtil.arrondis(this.forme.getX()));
		this.posYField.setText(""+MathUtil.arrondis(this.forme.getY()));
		this.largeurField.setText(""+MathUtil.arrondis(this.forme.getLargeur()));
		this.hauteurField.setText(""+MathUtil.arrondis(this.forme.getHauteur()));
		this.epaisseurField.setText(""+MathUtil.arrondis(this.forme.getEpaisseurLigne()));
		this.setValueCouleurFond(this.forme.getCouleurFond());
		this.setValueCouleurLigne(this.forme.getCouleurLigne());
	}

	public int getValueX() {
		if(this.posXField.getText() != null) {
			return Integer.parseInt(this.posXField.getText());
		} 
		return MathUtil.arrondis(forme.getX());
	}

	public int getValueY() {
		if(this.posYField.getText() != null) {
			return Integer.parseInt(this.posYField.getText());
		} 
		return MathUtil.arrondis(forme.getY());
	}

	public int getValueLargeur() {
		if(this.largeurField.getText() != null) {
			return Integer.parseInt(this.largeurField.getText());
		} 
		return MathUtil.arrondis(forme.getLargeur());
	}

	public int getValueHauteur() {
		if(this.hauteurField.getText() != null) {
			return Integer.parseInt(this.hauteurField.getText());
		} 
		return MathUtil.arrondis(forme.getHauteur());
	}

	public int getValueEpaisseur() {
		if(this.epaisseurField.getText() != null) {
			return Integer.parseInt(this.epaisseurField.getText());
		} 
		return MathUtil.arrondis(forme.getEpaisseurLigne());
	}

	public Color getValueCouleurLigne() {
		return this.cLigne;
	}

	public Color getValueCouleurFond() {
		return this.cFond;
	}

	public void setValueCouleurLigne(Color c) {
		this.cLigne = c;
		if(c != null) {
			this.couleurLigneB.setBackground(new Color(c.getRed(), c.getGreen(), c.getBlue()));
			this.couleurLigneB.setForeground(ColorUtil.getCouleurComplementaire(c));
		}
	}

	public void setValueCouleurFond(Color c) {
		this.cFond = c;
		if(c != null) {
			this.couleurFondB.setBackground(new Color(c.getRed(), c.getGreen(), c.getBlue()));
			this.couleurFondB.setForeground(ColorUtil.getCouleurComplementaire(c));
		} else {
			this.couleurFondB.setBackground(null);
			this.couleurFondB.setForeground(Color.BLACK);
		}
	}

}

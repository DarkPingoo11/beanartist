package fr.eseo.gpi.beanartist.vue.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import lib.jiconfont.icons.FontAwesome;
import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.controleur.actions.dialog.ActionAnnuler;
import fr.eseo.gpi.beanartist.controleur.actions.dialog.ActionValider;
import fr.eseo.gpi.beanartist.modele.TexteDatas;
import fr.eseo.gpi.beanartist.vue.ui.components.JComboBoxFont;

public class TexteDialog extends EditDialog{

	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT = 400;
	public static final int HAUTEUR_PAR_DEFAUT = 150;

	private JTextField texte, taille;
	private JComboBox<String> police;

	//TODO - Changer avec un panel (pour dispatcher) et la classe JLabelIcon
	private TexteDialog(String titre, String messageD, int taille, String family) {
		super();
		this.setTitle(titre);
		this.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT));
		this.setResizable(false);
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.initComponents();
		this.pack();
		this.setLocationRelativeTo(null);
	
		//Valeurs par defaut
		this.texte.setText(messageD);
		this.taille.setText(""+taille);
		this.police.setSelectedItem(family);
		this.setModal(true);
	}

	private void initComponents() {

		JPanel topPan = new JPanel();
		JPanel buttonPan = new JPanel();

		buttonPan.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, 35));
		buttonPan.setBackground(Color.LIGHT_GRAY);

		//Inputs
		this.texte = new JTextField();
		this.texte.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT-20-32, 25));

		try {
			this.taille = new JFormattedTextField(new MaskFormatter("##"));
			this.taille.setPreferredSize(new Dimension(30, 25));
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		JLabel icone = new JLabel("");
		icone.setIcon(IconFontSwing.buildIcon(GoogleMaterialDesignIcons.TEXT_FORMAT, 32));
		icone.setPreferredSize(new Dimension(32, 25));

		JLabel icone2 = new JLabel("");
		icone2.setIcon(IconFontSwing.buildIcon(FontAwesome.TEXT_HEIGHT, 20));
		icone2.setPreferredSize(new Dimension(26, 25));

		JLabel icone3 = new JLabel("");
		icone3.setIcon(IconFontSwing.buildIcon(GoogleMaterialDesignIcons.FORMAT_SIZE, 32));
		icone3.setPreferredSize(new Dimension(32, 25));

		JLabel space = new JLabel("");
		space.setPreferredSize(new Dimension(80, 25));

		this.police = new JComboBoxFont(this);

		topPan.add(icone);
		topPan.add(this.texte);

		topPan.add(icone2);
		topPan.add(this.taille);
		topPan.add(space);
		topPan.add(icone3);
		topPan.add(this.police);

		//Boutons
		JButton okButton = new JButton(new ActionValider(this));
		JButton cancelButton = new JButton(new ActionAnnuler(this));

		buttonPan.add(okButton);
		buttonPan.add(cancelButton);

		//DefaultValues
		this.taille.setText("14");

		this.getContentPane().add(topPan);
		this.getContentPane().add(buttonPan, BorderLayout.SOUTH);
	}

	public JComboBox<String> getPoliceBox() {
		return this.police;
	}

	public JTextField getTextField() {
		return this.texte;
	}

	public JTextField getTailleField() {
		return this.taille;
	}

	public String getFontFamilyName() {
		return this.getPoliceBox().getSelectedItem().toString();
	}

	public int getTaille() {
		String t = this.getTailleField().getText();
		if(t == null || t == "") {
			t = "12";
		}
		return Math.max(8, Integer.parseInt(this.getTailleField().getText()));
	}

	public String getTexte() {
		return this.getTextField().getText();
	}

	public TexteDatas showDialog() {
		this.setVisible(true);

		if(this.isSendData()) {
			return new TexteDatas(this.getTexte(), this.getFontFamilyName(), this.getTaille());
		} else {
			return null;
		}
	}

	public static TexteDatas showTexteDialog(String titre, String def, int taille, String family) {
		TexteDialog t = new TexteDialog(titre, def, taille, family);
		return t.showDialog();
	}


}

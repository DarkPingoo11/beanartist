package fr.eseo.gpi.beanartist.vue.ui.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import lib.jiconfont.icons.FontAwesome;
import fr.eseo.gpi.beanartist.controleur.client.NetworkManager;
import fr.eseo.gpi.beanartist.vue.ui.components.JLabelIcon;

public class ConnectPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	private JTextField ipField, portField, nameField;
	private int largeur;

	public ConnectPanel(int largeur) {
		this.largeur = largeur;

		this.initComponents();
		this.setDefaultValues();
	}

	private void initComponents() {
		//Déclaration des panneaux intermédiaires
		JPanel ipPan = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel portPan = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel namePan = new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel warningPan = new JPanel(new FlowLayout(FlowLayout.LEFT));

		//-----Dimensions
		ipPan.setPreferredSize(new Dimension(this.largeur, 35));
		portPan.setPreferredSize(new Dimension(this.largeur, 35));
		namePan.setPreferredSize(new Dimension(this.largeur, 35));
		warningPan.setPreferredSize(new Dimension(this.largeur, 35));

		//Déclaration des composants de formulaire
		//-----Déclaration des composants
		this.ipField = new JFormattedTextField();
		this.portField = new JFormattedTextField();
		this.nameField = new JFormattedTextField();

		//-----Dimensions
		this.ipField.setPreferredSize(new Dimension(100, 25));
		this.portField.setPreferredSize(new Dimension(100, 25));
		this.nameField.setPreferredSize(new Dimension(100, 25));

		//Déclaration des Label, pour un affichage fancy
		//-----Icones
		JLabelIcon iconeAdress = new JLabelIcon(FontAwesome.LOCATION_ARROW, 20, 30);
		JLabelIcon iconePort = new JLabelIcon(FontAwesome.LOCATION_ARROW, 20, 30);
		JLabelIcon iconeName = new JLabelIcon(FontAwesome.USER, 20, 30);

		//-----Textes
		JLabel texteIP = new JLabel("Ip : ");
		JLabel textePORT = new JLabel("Port : ");
		JLabel texteNOM = new JLabel("Nom : ");
		JLabel texteS = new JLabel(" ");
		JLabel texteS1 = new JLabel(" ");
		JLabel texteS2 = new JLabel(" ");
		JLabel texteS3 = new JLabel(" ");
		JLabel texteWarning = new JLabel("<html>Votre dessin sera perdu lors <br />de votre connexion au serveur !</html>");
		texteWarning.setForeground(Color.RED);
		
			
		//-----Dimensions
		texteIP.setPreferredSize(new Dimension(50, 25));
		textePORT.setPreferredSize(new Dimension(50, 25));
		texteNOM.setPreferredSize(new Dimension(50, 25));
		texteS.setPreferredSize(new Dimension(10, 25));
		texteS1.setPreferredSize(new Dimension(10, 25));
		texteS2.setPreferredSize(new Dimension(10, 25));
		texteS3.setPreferredSize(new Dimension(50, 25));

		//-----Alignement
		texteIP.setHorizontalAlignment(JTextField.RIGHT);
		textePORT.setHorizontalAlignment(JTextField.RIGHT);
		texteNOM.setHorizontalAlignment(JTextField.RIGHT);

		//Placement de tous les composants
		//-----Partie DESTINATION
		ipPan.add(iconeAdress);
		ipPan.add(texteIP);
		ipPan.add(texteS);
		ipPan.add(this.ipField);
		
		portPan.add(iconePort);
		portPan.add(textePORT);
		portPan.add(texteS1);
		portPan.add(this.portField);

		//-----Partie NAME
		namePan.add(iconeName);
		namePan.add(texteNOM);
		namePan.add(texteS2);
		namePan.add(this.nameField);
		
		//-----Partie WARNING
		warningPan.add(texteS3);
		warningPan.add(texteWarning);

		//Ajout de tout au panel
		this.add(ipPan);
		this.add(portPan);
		this.add(namePan);
		this.add(warningPan);
	}

	private void setDefaultValues() {
		this.ipField.setText("127.0.0.1");
		this.portField.setText("2345");

		if(NetworkManager.getInstance().getClient() != null) {
			this.nameField.setText(NetworkManager.getInstance().getClient().getName());
		}
	}

	public String getValueIP() {
		if(this.ipField.getText() != null) {
			return (this.ipField.getText());
		} 
		return "127.0.0.1";
	}

	public int getValuePort() {
		if(this.portField.getText() != null) {
			return Integer.parseInt(this.portField.getText());
		} 
		return 2345;
	}

	public String getValueName() {
		if(this.nameField.getText() != null) {
			return this.nameField.getText();
		} else if(NetworkManager.getInstance().getClient() != null) {
			return NetworkManager.getInstance().getClient().getName();
		} else {
			return "";
		}
	}

}

package fr.eseo.gpi.beanartist.vue.ui.dialog;

import javax.swing.JDialog;

public abstract class EditDialog extends JDialog{

	private static final long serialVersionUID = 1L;

	private boolean sendData;
	
	public EditDialog() {
		super();
		this.setSendData(false);
	}
	
	/**
	 * @return the sendData
	 */
	public boolean isSendData() {
		return sendData;
	}
	
	/**
	 * @param sendData the sendData to set
	 */
	public void setSendData(boolean sendData) {
		this.sendData = sendData;
	}
	

}

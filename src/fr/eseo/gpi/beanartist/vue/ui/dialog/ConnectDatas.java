package fr.eseo.gpi.beanartist.vue.ui.dialog;



public class ConnectDatas {

	private String ip, name;
	private int port;
	public ConnectDatas(String ip, int port, String name) {
		this.ip = ip;
		this.port = port;
		this.name = name;
	}
	
	public String getIp() {
		return ip;
	}

	public String getName() {
		return name;
	}

	public int getPort() {
		return port;
	}
}

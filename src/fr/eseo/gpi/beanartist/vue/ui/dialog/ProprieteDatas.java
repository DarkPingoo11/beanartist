package fr.eseo.gpi.beanartist.vue.ui.dialog;

import java.awt.Color;


public class ProprieteDatas {

	private int x, y, largeur, hauteur, epaisseur;
	private Color ligne, fond;
	public ProprieteDatas(int x, int y, int largeur, int hauteur, int epaisseur, Color ligne, Color fond) {
		this.setX(x);
		this.setY(y);
		this.setLargeur(largeur);
		this.setHauteur(hauteur);
		this.setEpaisseur(epaisseur);
		this.setLigne(ligne);
		this.setFond(fond);
	}
	
	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the largeur
	 */
	public int getLargeur() {
		return largeur;
	}

	/**
	 * @param largeur the largeur to set
	 */
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	/**
	 * @return the hauteur
	 */
	public int getHauteur() {
		return hauteur;
	}

	/**
	 * @param hauteur the hauteur to set
	 */
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}

	/**
	 * @return the epaisseur
	 */
	public int getEpaisseur() {
		return epaisseur;
	}

	/**
	 * @param epaisseur the epaisseur to set
	 */
	public void setEpaisseur(int epaisseur) {
		this.epaisseur = epaisseur;
	}

	/**
	 * @return the ligne
	 */
	public Color getLigne() {
		return ligne;
	}

	/**
	 * @param ligne the ligne to set
	 */
	public void setLigne(Color ligne) {
		this.ligne = ligne;
	}

	/**
	 * @return the fond
	 */
	public Color getFond() {
		return fond;
	}

	/**
	 * @param fond the fond to set
	 */
	public void setFond(Color fond) {
		this.fond = fond;
	}

}

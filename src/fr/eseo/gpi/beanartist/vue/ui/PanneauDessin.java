package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.controleur.focus.AlwaysFocusListener;
import fr.eseo.gpi.beanartist.controleur.options.OptionTypeEnum;
import fr.eseo.gpi.beanartist.controleur.outils.Outil;
import fr.eseo.gpi.beanartist.controleur.outils.OutilSelection;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.vue.formes.VueForme;

public class PanneauDessin extends JPanel{

	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT = 600;
	public static final int HAUTEUR_PAR_DEFAUT = 500;
	public static final int PAS_GRILLE_PAR_DEFAUT = 10;
	public static final Color COULEUR_FOND_PAR_DEFAUT = Color.WHITE;
	public static final Color COULEUR_LIGNE_PAR_DEFAUT = new Color(51, 51, 51);
	public static final Color COULEUR_GRILLE_PAR_DEFAUT = Color.LIGHT_GRAY;
	public static final Color COULEUR_SELECTIONNEE_PAR_DEFAUT = Color.GREEN;

	private List<VueForme> vueFormes;
	private VueForme vueTemporaire;
	private Outil outilCourant;
	private Color couleurLigneCourante, couleurRemplir;

	public PanneauDessin(int largeur, int hauteur, Color fond) {
		super();

		this.setPreferredSize(new Dimension(largeur, hauteur));
		this.setBackground(fond);
		this.vueFormes = new ArrayList<VueForme>();
		this.couleurLigneCourante = COULEUR_LIGNE_PAR_DEFAUT;
		this.couleurRemplir = COULEUR_FOND_PAR_DEFAUT;
		this.addFocusListener(new AlwaysFocusListener(this));
	}

	public PanneauDessin() {
		this(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT, COULEUR_FOND_PAR_DEFAUT);
	}

	public List<VueForme> getVueFormes() {
		return this.vueFormes;
	}

	public void ajouterVueForme(VueForme vue) {
		this.vueFormes.add(vue);
		this.repaint();
	}

	public Outil getOutilCourant() {
		return this.outilCourant;
	}

	public void setOutilCourant(Outil o) {
		this.outilCourant = o;
	}

	public VueForme getVueTemporaire() {
		return this.vueTemporaire;
	}

	public void setVueTemporaire(VueForme v) {
		this.vueTemporaire = v;
	}

	public Color getCouleurLigneCourante() {
		return this.couleurLigneCourante;
	}

	public Color getCouleurRemplir() {
		return this.couleurRemplir;
	}

	public void setCouleurLigneCourante(Color c) {
		this.couleurLigneCourante = (c == null ? COULEUR_LIGNE_PAR_DEFAUT : c);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().repaintCouleurBoxCouleur();
	}

	public void setCouleurRemplir(Color c) {
		this.couleurRemplir = (c == null ? COULEUR_FOND_PAR_DEFAUT : c);
		FenetreBeAnArtist.getInstance().getPanneauBarreOutils().repaintCouleurBoxRemplir();
	}

	public synchronized void retirerForme(Forme f) {
		VueForme formeToRem = null;
		for(VueForme vf : this.getVueFormes()) {
			if(vf.getForme().equals(f)) {
				formeToRem = vf;
				break;
			}
		}
		if(formeToRem != null) {
			this.retirerVueForme(formeToRem);
		}
	}

	public synchronized void retirerVueForme(VueForme vf) {
		this.getVueFormes().remove(vf);
		this.repaint();
	}

	public synchronized void setVueFormes(List<VueForme> liste) {
		this.vueFormes.clear();
		this.vueFormes.addAll(liste);
		this.repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D) g.create();
		PanneauBarreOutils pbo = FenetreBeAnArtist.getInstance().getPanneauBarreOutils();

		//Affichage de la grille
		if(pbo.getOptionValue(OptionTypeEnum.AFFICHER_GRILLE)) {
			int xMax = this.getWidth();
			int yMax = this.getHeight();
			g2d.setColor(COULEUR_GRILLE_PAR_DEFAUT);
			for(int i = 0; i < xMax; i+=PAS_GRILLE_PAR_DEFAUT) {
				g2d.drawLine(i, 0, i, yMax);
			}
			for(int i = 0; i < yMax; i+=PAS_GRILLE_PAR_DEFAUT) {
				g2d.drawLine(0, i, xMax, i);
			}
		}

		//Activer l'antialiasing
		if(pbo.getOptionValue(OptionTypeEnum.ANTI_ALIASHING)) {
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		}
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);

		g2d.setStroke(new BasicStroke(1));
		for(VueForme vue : this.getVueFormes()) {
			g2d.setStroke(new BasicStroke(MathUtil.arrondis(vue.getForme().getEpaisseurLigne())));
			vue.affiche(g2d);
			//Affichage du rectagle englobant
			if(pbo.getOptionValue(OptionTypeEnum.AFFICHER_RENGLOBANT)) {
				vue.afficheRectangleContenant(g2d);
			}
			//Affichage d'un rectangle sur la forme selectionnee
			if(this.getOutilCourant() != null) {
				if(this.getOutilCourant() instanceof OutilSelection) {
					Forme select = ((OutilSelection)this.getOutilCourant()).getFormeSelectionnee();
					if(select != null) {
						if(select.equals(vue.getForme())) {
							vue.afficheRectangleContenant(g2d, COULEUR_SELECTIONNEE_PAR_DEFAUT);
						}
					}
				}
			}
		}

		g2d.setStroke(new BasicStroke(1));
		//Affichage de la figure temporaire (en cours de copie / creation)
		if(this.getVueTemporaire() != null) {
			this.getVueTemporaire().affiche(g2d);
		}

		g2d.dispose();
	}

}

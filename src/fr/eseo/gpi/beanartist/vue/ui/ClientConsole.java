package fr.eseo.gpi.beanartist.vue.ui;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JTextField;

import fr.eseo.gpi.beanartist.controleur.actions.reseau.ActionSendCommand;
import fr.eseo.gpi.beanartist.controleur.client.ClientBeAnArtist;
import fr.eseo.gpi.beanartist.serveur.vue.ui.Console;

public class ClientConsole extends JFrame{

	private static final long serialVersionUID = 1L;
	public static final int LARGEUR_PAR_DEFAUT = 600;
	public static final int HAUTEUR_PAR_DEFAUT = 300;
	
	private Console console;
	private ClientBeAnArtist client;
	private boolean upToDate;
	
	public ClientConsole(ClientBeAnArtist client) {
		super(">_Console client : " + client.getName());
		this.client = client;
		this.console = new Console(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT, new ActionSendCommand());
		this.upToDate = true;
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setPreferredSize(new Dimension(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT));
		this.setResizable(false);
		this.setContentPane(this.console);
		this.pack();
	}
	
	private Console getConsolePan() {
		return this.console;
	}
	
	public JTextField getTextField() {
		return this.getConsolePan().getTextField();
	}

	public boolean isUpToDate() {
		return this.upToDate;
	}
	
	/**
	 * Défini si l'utilisateur a vu le dernier message de la console (message utilisateur)
	 * @param b Status
	 */
	public void setUpToDate(boolean b) {
		this.upToDate = b;
		FenetreBeAnArtist.getInstance().getMenu().updateMenu();
	}
	
	public ClientBeAnArtist getClient() {
		return this.client;
	}

	public void log(String msg) {
		this.getConsolePan().log(msg);
	}
	
	public void clear() {
		this.getConsolePan().clear();
	}
}


package fr.eseo.gpi.beanartist.vue.ui.components;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;

import fr.eseo.gpi.beanartist.vue.ui.dialog.TexteDialog;

public class JComboBoxFont extends JComboBox<String> {

	private static final long serialVersionUID = 1L;
	private static String[] fonts = null;

	private TexteDialog texteD;

	public JComboBoxFont(TexteDialog td) {
		super();

		this.texteD = td;
		if(JComboBoxFont.fonts == null) {
			JComboBoxFont.initFonts();
		}

		for(String name : fonts) {
			this.addItem(name);
		}

		this.setRenderer(new FontCellRenderer());
		this.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					getTexteDialog().getPoliceBox().setFont(new Font(getSelectedItem().toString(), Font.PLAIN, 12));
					getTexteDialog().getTextField().setFont(new Font(getSelectedItem().toString(), Font.PLAIN, 12));
				}
			}
		});
	}

	public static void initFonts() {
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		fonts = ge.getAvailableFontFamilyNames();
	}

	public TexteDialog getTexteDialog() {
		return texteD;
	}

	private class FontCellRenderer extends DefaultListCellRenderer {
		private static final long serialVersionUID = 1L;

		@Override
		@SuppressWarnings("rawtypes")
		public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
			JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
			Font font = new Font((String)value, Font.PLAIN, 14);
			label.setFont(font);
			label.setPreferredSize(new Dimension(160, 20));
			return label;
		}
	}
}
package fr.eseo.gpi.beanartist.vue.ui.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.AbstractButton;

public class ButtonRuler {

	public static final int ESPACE_PAR_DEFAUT = 4;
	public static final int ESPACE_SUPPLEMENTAIRE = 6;
	
	private List<AbstractButton> boutons;
	
	public ButtonRuler() {
		this.boutons = new ArrayList<AbstractButton>();
	}
	
	private List<AbstractButton> getBoutons() {
		return this.boutons;
	}
	
	public AbstractButton add(AbstractButton a) {
		this.getBoutons().add(a);
		return a;
	}
	
	public void addAll(Collection<AbstractButton> a) {
		this.getBoutons().addAll(a);
	}
	
	public void reorganizeIconsGap() {
		int maxIcoSize = 0;
		//Récupération du plus grand espace
		for(AbstractButton b : this.getBoutons()) {
			if(b.getIcon() != null) {
				if(b.getIcon().getIconWidth() > maxIcoSize) {
					maxIcoSize = b.getIcon().getIconWidth();
				}
			}
		}
		//Reorganisation
		for(AbstractButton b : this.getBoutons()) {
			if(b.getIcon() != null) {
				int currentWidth = b.getIcon().getIconWidth();
				b.setIconTextGap(maxIcoSize - currentWidth + ESPACE_PAR_DEFAUT + ESPACE_SUPPLEMENTAIRE);
			}
		}
	}
	
	
	
	

}

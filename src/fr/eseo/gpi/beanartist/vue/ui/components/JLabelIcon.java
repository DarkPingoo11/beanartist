package fr.eseo.gpi.beanartist.vue.ui.components;

import java.awt.Dimension;

import javax.swing.JLabel;

import lib.jiconfont.IconCode;
import lib.jiconfont.swing.IconFontSwing;

public class JLabelIcon extends JLabel{

	private static final long serialVersionUID = 1L;

	public JLabelIcon(IconCode icone, int size, int size2) {
		super(" ");
		if(icone != null) {
			this.setIcon(IconFontSwing.buildIcon(icone, size));
			this.setPreferredSize(new Dimension(size2, 25));
			this.setAlignmentY(JLabel.CENTER_ALIGNMENT);
			this.setHorizontalTextPosition(JLabel.LEFT);
		}
	}
	
	
	public JLabelIcon(IconCode icone, int size) {
		this(icone, size, size);
	}
	
	public JLabelIcon(IconCode icone) {
		this(icone, 32, 32);
	}

}

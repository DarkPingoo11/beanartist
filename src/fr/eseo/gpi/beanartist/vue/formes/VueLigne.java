package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;

public class VueLigne extends VueForme{

	public VueLigne(Ligne f) {
		super(f);
	}
	
	@Override
	public void affiche(Graphics2D graphic) {
		Ligne l = (Ligne)this.getForme();
		graphic.setColor(l.getCouleurLigne());
//		graphic.setBackground(l.getCouleurFond());
		
		graphic.drawLine(
				MathUtil.arrondis(l.getP1().getX()), 
				MathUtil.arrondis(l.getP1().getY()), 
				MathUtil.arrondis(l.getP2().getX()), 
				MathUtil.arrondis(l.getP2().getY()));
	}
}

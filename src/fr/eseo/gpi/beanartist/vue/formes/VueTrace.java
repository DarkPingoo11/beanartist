package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.modele.formes.Point;
import fr.eseo.gpi.beanartist.modele.formes.Trace;

public class VueTrace extends VueForme{

	public VueTrace(Trace f) {
		super(f);
	}
	
	@Override
	public void affiche(Graphics2D graphic) {
		Trace t = (Trace) this.getForme();
		
		graphic.setColor(t.getCouleurLigne());
//		graphic.setBackground(t.getCouleurFond());
		
		for(int i = 0; i < t.getPoints().size()-1; i++) {
			Point p1 = t.getPoints().get(i);
			Point p2 = t.getPoints().get(i+1);
			graphic.drawLine(
					MathUtil.arrondis(p1.getX()), 
					MathUtil.arrondis(p1.getY()), 
					MathUtil.arrondis(p2.getX()), 
					MathUtil.arrondis(p2.getY()));
		}
	}
}

package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.FontMetrics;
import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.modele.formes.Texte;

public class VueTexte extends VueForme{

	public VueTexte(Texte f) {
		super(f);
	}

	@Override
	public void affiche(Graphics2D graphic) {
		Texte t = (Texte) this.getForme();
		graphic.setColor(t.getCouleurLigne());
		graphic.setFont(t.getFont());

		//Afficher uniquement le texte visible
		FontMetrics metrics = graphic.getFontMetrics(graphic.getFont());
		String line = "";
		String texte = t.getTexte();
		String[] mots = texte.split(" ");
		int maxH = MathUtil.arrondis(t.getHauteur());
		int maxL = MathUtil.arrondis(t.getLargeur());
		int hauteurCourant = metrics.getHeight();

		for(int i = 0; i < mots.length && hauteurCourant <= maxH; i++) {
			//Si le mot est trop grand
			if(metrics.stringWidth(mots[i]) > maxL) {
				String mot = mots[i];
				line += " ";
				//On ajoute caractère par caractère jusqu'au depassement
				for(int j = 0; j < mot.length() && hauteurCourant <= maxH; j++) {
					if(metrics.stringWidth(line + mot.charAt(j)) < maxL) {
						line += mot.charAt(j);
					} else {
						//On ajoute un tiret, sauf si il reste un seul caractère, dans ce cas on l'ajoute a la place
						line += (j == mot.length()-1) ? mot.charAt(j) : "-";
						//Si c'est la dernière ligne, on ajoute des ..
						if((hauteurCourant + metrics.getHeight()) > maxH) {
							if(!line.endsWith("-") && line.length() > 2) {
								line += line.substring(0, line.length()-1-2) + "..";
							}
						}
						//On dessine le string et on prepare la prochaine ligne
						graphic.drawString(line, MathUtil.arrondis(t.getX()), MathUtil.arrondis(t.getY()) + hauteurCourant );
						line = "" + ((j == mot.length()-1) ? "" : mot.charAt(j));
						hauteurCourant += metrics.getHeight();
					}
				}
			} else {
				if(metrics.stringWidth(line + " " + mots[i]) <= maxL) {
					line += ((i==0) ? "" : " ") + mots[i];
				} else {
					if((hauteurCourant + metrics.getHeight()) > maxH) {
						if(metrics.stringWidth(line + "..") > maxL && line.length() > 2) {
							line = line.substring(0, line.length()-1-2) + "..";
						} else {
							line += "..";
						}
					}
					graphic.drawString(line, MathUtil.arrondis(t.getX()), MathUtil.arrondis(t.getY()) + hauteurCourant );
					line = "" + mots[i];
					hauteurCourant += metrics.getHeight();
				}
			}
		}

		if(!line.isEmpty() && hauteurCourant <= maxH) {
			graphic.drawString(line, MathUtil.arrondis(t.getX()), MathUtil.arrondis(t.getY()) + hauteurCourant);
		}
	}
}

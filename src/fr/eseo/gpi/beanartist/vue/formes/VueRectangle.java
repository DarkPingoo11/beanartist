package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;

public class VueRectangle extends VueForme{

	public VueRectangle(Rectangle f) {
		super(f);
	}
	
	@Override	
	public void affiche(Graphics2D graphic) {
		Forme f = this.getForme();
		
		if(f.getCouleurFond() != null) {
			graphic.setColor(f.getCouleurFond());
			graphic.fillRect(
					MathUtil.arrondis(f.getX()), 
					MathUtil.arrondis(f.getY()), 
					MathUtil.arrondis(f.getLargeur()), 
					MathUtil.arrondis(f.getHauteur()));
		}
		
		graphic.setColor(f.getCouleurLigne());
		graphic.drawRect(
				MathUtil.arrondis(f.getX()), 
				MathUtil.arrondis(f.getY()), 
				MathUtil.arrondis(f.getLargeur()), 
				MathUtil.arrondis(f.getHauteur()));
		
		
	}
}

package fr.eseo.gpi.beanartist.vue.formes;

import java.awt.Color;
import java.awt.Graphics2D;

import fr.eseo.gpi.beanartist.Utils.MathUtil;
import fr.eseo.gpi.beanartist.controleur.options.OptionTypeEnum;
import fr.eseo.gpi.beanartist.controleur.outils.Outil;
import fr.eseo.gpi.beanartist.modele.formes.Carre;
import fr.eseo.gpi.beanartist.modele.formes.Cercle;
import fr.eseo.gpi.beanartist.modele.formes.Ellipse;
import fr.eseo.gpi.beanartist.modele.formes.Forme;
import fr.eseo.gpi.beanartist.modele.formes.Ligne;
import fr.eseo.gpi.beanartist.modele.formes.Rectangle;
import fr.eseo.gpi.beanartist.modele.formes.Texte;
import fr.eseo.gpi.beanartist.modele.formes.Trace;
import fr.eseo.gpi.beanartist.vue.ui.FenetreBeAnArtist;

public abstract class VueForme {

	private static final Color COULEUR_REC_ENGLOBANT_DEFAUT = Color.GRAY;
	
	protected final Forme forme;
	
	public VueForme(Forme f) {
		this.forme = f;
	}
	
	public Forme getForme() {
		return this.forme;
	}
	
	public abstract void affiche(Graphics2D graphic);
	
	public void afficheRectangleContenant(Graphics2D graphic) {
		this.afficheRectangleContenant(graphic, COULEUR_REC_ENGLOBANT_DEFAUT);
	}
	
	public void afficheRectangleContenant(Graphics2D graphic, Color c) {
		int x = MathUtil.arrondis(this.forme.getCadreMinX());
		int y = MathUtil.arrondis(this.forme.getCadreMinY());
		int l = MathUtil.arrondis(this.forme.getCadreMaxX() - this.forme.getCadreMinX());
		int h = MathUtil.arrondis(this.forme.getCadreMaxY() - this.forme.getCadreMinY());
		Color previousColor = graphic.getColor();
		graphic.setColor(c);
		graphic.drawRect(x, y, l, h);
		
		if(FenetreBeAnArtist.getInstance().getPanneauBarreOutils().getOptionValue(OptionTypeEnum.AFFICHER_PRECISION)) {
			graphic.setColor(Color.BLUE);
			graphic.drawRect(x-Outil.PRECISION, y-Outil.PRECISION, l+Outil.PRECISION*2, h+Outil.PRECISION*2);
			graphic.drawRect(x+Outil.PRECISION, y+Outil.PRECISION, l-Outil.PRECISION*2, h-Outil.PRECISION*2);
		}
		
		//On remet la couleur précédente
		graphic.setColor(previousColor);
	}
	
	//STATIQUE
	public static VueForme getNewVueFormeCorrespondante(Forme f) {
		return getVueFormeCorrespondante((Forme)f.clone());
	}
	
	public static VueForme getVueFormeCorrespondante(Forme f) {
		if(f instanceof Carre) {
			return new VueCarre((Carre) f);
		} else if(f instanceof Cercle) {
			return new VueCercle((Cercle) f);
		} else if(f instanceof Rectangle) {
			return new VueRectangle((Rectangle) f);
		} else if(f instanceof Ellipse) {
			return new VueEllipse((Ellipse) f);
		} else if(f instanceof Ligne) {
			return new VueLigne((Ligne) f);
		} else if(f instanceof Trace) {
			Trace t = (Trace)((Trace)f);
			return new VueTrace(t);
		} else if(f instanceof Texte) {
			Texte t = (Texte)((Texte)f);
			return new VueTexte(t);
		} else {
			return null;
		}
	}

}

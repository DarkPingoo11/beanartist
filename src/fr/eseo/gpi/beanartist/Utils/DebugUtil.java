package fr.eseo.gpi.beanartist.Utils;


public class DebugUtil {

	private static final boolean DEBUG = true;
	
	public static void message(Class<?> from, String m) {
		if(DEBUG) {
			System.out.println("[DEBUG-"+from.getSimpleName()+"] "+ m);
		}
	}
	
	public static void error(Class<?> from, String m) {
		if(DEBUG) {
			System.err.println("[DEBUG-"+from.getSimpleName()+"] "+ m);
		}
	}

	
	
}

package fr.eseo.gpi.beanartist.Utils;

import java.awt.Color;

import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import lib.jiconfont.icons.GoogleMaterialDesignIcons;
import lib.jiconfont.swing.IconFontSwing;
import fr.eseo.gpi.beanartist.modele.TexteDatas;
import fr.eseo.gpi.beanartist.vue.ui.dialog.TexteDialog;

public class UIUtil {

	public static Color pickColor(String message, Color cdef) {
		 Color c = JColorChooser.showDialog(null, "Choisissez la couleur de ligne", cdef);
		 return c == null ? cdef : c;
	}
	
	public static String chooseFileSavePath()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Enregistrer le dessin");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.addChoosableFileFilter(new FileNameExtensionFilter("Dessin (*.dessin)", "dessin"));
		if(chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) { 
			return chooser.getSelectedFile().getAbsolutePath()+".dessin";
		}
		return null;
	}
	
	public static String chooseFileOpenPath()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Ouvrir un dessin");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.addChoosableFileFilter(new FileNameExtensionFilter("Dessin (*.dessin)", "dessin"));
		if(chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) { 
			return chooser.getSelectedFile().getAbsolutePath();
		}
		return null;
	}
	
	public static void showErrorDialog(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showSuccessDialog(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void showWarningDialog(String titre, String message) {
		JOptionPane.showMessageDialog(null, message, titre, JOptionPane.WARNING_MESSAGE);
	}
	
	public static boolean askWarningDialog(String titre, String message) {
		return JOptionPane.showConfirmDialog(null, message, titre, JOptionPane.YES_NO_CANCEL_OPTION) == JOptionPane.YES_OPTION;
	}
	
	public static String showSimpleInputDialog(String titre, String message, String defaut) {
		Object r = JOptionPane.showInputDialog(null, message, titre, JOptionPane.INFORMATION_MESSAGE, IconFontSwing.buildIcon(GoogleMaterialDesignIcons.FORMAT_COLOR_TEXT,32), null, defaut);
		if(r instanceof String) {
			return (String)r;
		} else {
			return null;
		}
	}
	
	public static String showSimpleInputDialog(String titre, String message) {
		return showSimpleInputDialog(titre, message, null);
	}
	
	public static TexteDatas showTexteInputDialog(String titre, String defaut, int taille, String font) {
		return TexteDialog.showTexteDialog(titre, defaut, taille, font);
	}
	
	public static TexteDatas showTexteInputDialog(String titre) {
		return showTexteInputDialog(titre, null, 14, "Dialog");
	}
	
	public static int showInputIntDialog(String titre, String message, int def) {
		int entier = 0;
		boolean errorOccured = false;
		do {
			errorOccured = false;
			String s = UIUtil.showSimpleInputDialog(titre, message, ""+def);
			try {
				entier = Integer.parseInt(s);
			} catch(Exception ex) {
				errorOccured = true;
			}
		} while(errorOccured);
		
		return entier;
	}
	
	public static Integer showInputIntCancelDialog(String titre, String message, int def) {
		int entier = 0;
		boolean errorOccured = false;
		boolean isNull = false;
		do {
			errorOccured = false;
			String s = UIUtil.showSimpleInputDialog(titre, message, ""+def);
			if(s == null) {
				isNull = true;
			}
			try {
				entier = Integer.parseInt(s);
			} catch(Exception ex) {
				errorOccured = true;
			}
		} while(errorOccured && !isNull);
		
		return isNull ? null : entier;
	}
}

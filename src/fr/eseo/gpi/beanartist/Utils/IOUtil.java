package fr.eseo.gpi.beanartist.Utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import fr.eseo.gpi.beanartist.modele.formes.Forme;

public class IOUtil {

	public static void serializeAndSave(String path, List<Forme> formes) {
		ObjectOutputStream os = null;
		boolean erreur = false;
		String message = "Les "+formes.size()+" formes ont bien étées enregistrées !";
		
		try {
			FileOutputStream file = new FileOutputStream(path);
			os = new ObjectOutputStream(new BufferedOutputStream(file));
			os.writeObject(formes);
		} catch(IOException ex) {
			erreur = true;
			message = ex.getMessage();
		} finally {
			//On vide le stream
			try {
				if(os != null) {
					os.flush();
					os.close();
				}
			} catch (IOException e) {
				erreur = true;
				message = e.getMessage();
			}
		}
		
		if(erreur) {
			UIUtil.showErrorDialog("Action impossible !", message+"<br />"+"Le fichier n'a pas été sauvegardé");
			try{
				//On efface le fichier créer si il y a eu une erreur
				File f = new File(path);
				f.delete();
			} catch(Exception ex) {}
		} else {
			UIUtil.showSuccessDialog("Enregistrement reussi !", message);
		}
	}
	
	@SuppressWarnings("unchecked")
	public static List<Forme> openAndDeserialize(String path) {
		ObjectInputStream is = null;
		boolean erreur = false;
		String titre = "";
		String message = "";
		List<Forme> formes = null;
		
		try {
			FileInputStream file = new FileInputStream(path);
			is = new ObjectInputStream(new BufferedInputStream(file));
			formes = (List<Forme>)is.readObject();
		} catch(IOException ex) {
			titre = "Erreur de lecture du fichier !";
			message = ex.getMessage();
		} catch(ClassNotFoundException ex2) {
			erreur = true;
			titre = "Erreur de version !";
			message = ex2.getMessage();
		} finally {
			try {
				if(is != null) {
					is.close();
				}
			} catch (IOException ex3) {
				erreur = true;
				titre = "Erreur inattendue !";
				message = ex3.getMessage();
			}
		}
		
		if(erreur) {
			UIUtil.showErrorDialog(titre, message+"<br />"+"L'importation a été abandonnée");
			return null;
		}
		return formes;
	}

}

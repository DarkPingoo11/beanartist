package fr.eseo.gpi.beanartist.Utils;

import java.awt.Color;

public class ColorUtil {

	public static Color getCouleurComplementaire(Color c) {
		if(c != null) {
			int r = c.getRed();
			int g = c.getGreen();
			int b = c.getBlue();
			return new Color(255-r, 255-g, 255-b);
		} else {
			return Color.BLACK;
		}
	}

	public static String htmlColorize(String s, Color c) {
		String baliseIn = "<font color=#" + Integer.toHexString(c.getRed()) 
				+ Integer.toHexString(c.getGreen()) 
				+ Integer.toHexString(c.getBlue()) + ">";
		String baliseOut = "</font>";

		return baliseIn + s + baliseOut;
	}



}

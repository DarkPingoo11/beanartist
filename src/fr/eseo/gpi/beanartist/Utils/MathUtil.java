package fr.eseo.gpi.beanartist.Utils;

import java.text.DecimalFormat;

import fr.eseo.gpi.beanartist.modele.formes.Point;

public class MathUtil {

	public static double distance(Point p1, Point p2) {
		return Math.sqrt(Math.pow(p2.getX() - p1.getX(), 2) + Math.pow(p2.getY() - p1.getY(), 2));
	}
	
	public static int arrondis(double d) {
		return (int)(d+0.5d);
	}
	
	public static boolean isP1VersP2Negatif(Point p1, Point p2) {
		return (p1.getX() > p2.getX() || p1.getY() > p2.getY());
	}
	
	public static String formatDouble(double d) {
		return new DecimalFormat("#.##").format(d);
	}
	
}

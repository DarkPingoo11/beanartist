package lib.jiconfont;

/**
 * Librairie créer par Cadu Andrade 
 * Git : git@github.com:jIconFont/jiconfont-font_awesome.git
 * **/

public abstract interface IconCode {
  public abstract String name();
  
  public abstract char getUnicode();
  
  public abstract String getFontFamily();
}

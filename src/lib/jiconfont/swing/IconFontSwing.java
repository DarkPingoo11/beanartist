package lib.jiconfont.swing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

import lib.jiconfont.IconCode;
import lib.jiconfont.IconFont;

/**
 * Librairie créer par Cadu Andrade 
 * Git : git@github.com:jIconFont/jiconfont-font_awesome.git
 * **/

public final class IconFontSwing
{
	private static List<IconFont> fonts = new ArrayList<IconFont>();

	public static synchronized void register(IconFont iconFont) {
		if (!fonts.contains(iconFont)) {
			fonts.add(iconFont);
		}
	}

	public static final synchronized Font buildFont(String fontFamily) {
		try	{
			for (IconFont iconFont : fonts) {
				if (iconFont.getFontFamily().equals(fontFamily)) {
					return Font.createFont(0, iconFont.getFontInputStream());
				}
			}
		} catch (Exception ex) {
			Logger.getLogger(IconFontSwing.class.getName()).log(Level.SEVERE, "Font load failure", ex);
		}
		Logger.getLogger(IconFontSwing.class.getName()).log(Level.SEVERE, "Font not found: " + fontFamily);

		throw new IllegalArgumentException("Font not found: " + fontFamily);
	}

	public static Image buildImage(IconCode iconCode, float size) {
		return buildImage(iconCode, size, Color.BLACK);
	}

	public static Image buildImage(IconCode iconCode, float size, Color color) {
		Font font = buildFont(iconCode, size);
		String text = Character.toString(iconCode.getUnicode());
		return buildImage(text, font, color);
	}

	public static Icon buildIcon(IconCode iconCode, float size) {
		return buildIcon(iconCode, size, Color.BLACK);
	}

	public static Icon buildIcon(IconCode iconCode, float size, Color color) {
		return new ImageIcon(buildImage(iconCode, size, color));
	}

	private static BufferedImage buildImage(String text, Font font, Color color) {
		JLabel label = new JLabel(text);
		label.setForeground(color);
		label.setFont(font);
		Dimension dim = label.getPreferredSize();
		int width = dim.width + 1;
		int height = dim.height + 1;
		label.setSize(width, height);
		BufferedImage bufImage = new BufferedImage(width, height, 2);

		Graphics2D g2d = bufImage.createGraphics();
		g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);


		g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);


		label.print(g2d);
		g2d.dispose();
		return bufImage;
	}

	private static Font buildFont(IconCode iconCode, float size) {
		Font font = buildFont(iconCode.getFontFamily());
		return font.deriveFont(size);
	}
}


package lib.jiconfont;

import java.io.InputStream;

/**
 * Librairie créer par Cadu Andrade 
 * Git : git@github.com:jIconFont/jiconfont-font_awesome.git
 * **/

public abstract interface IconFont {
  public abstract String getFontFamily();
  
  public abstract InputStream getFontInputStream();
}
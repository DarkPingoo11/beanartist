package lib.jiconfont;

/**
 * Librairie créer par Cadu Andrade 
 * Git : git@github.com:jIconFont/jiconfont-font_awesome.git
 * **/

public class DefaultIconCode implements IconCode {
	private final char unicode;
	private final String fontFamily;

	public DefaultIconCode(String fontFamily, char unicode)	{
		this.fontFamily = fontFamily;
		this.unicode = unicode;
	}

	public String name() {
		return "[" + getUnicode() + "]";
	}

	public char getUnicode() {
		return this.unicode;
	}

	public String getFontFamily() {
		return this.fontFamily;
	}
}